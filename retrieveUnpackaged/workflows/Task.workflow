<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CancelledReason</fullName>
        <field>Reason_Code__c</field>
        <name>CancelledReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_High_Task</fullName>
        <field>IsEscalated_del__c</field>
        <literalValue>1</literalValue>
        <name>Escalate High Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_Medium_Task</fullName>
        <field>IsEscalated_del__c</field>
        <literalValue>1</literalValue>
        <name>Escalate Medium Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Task_Description_Summary_field</fullName>
        <field>Task_Description_Summary__c</field>
        <formula>IF( LEN(Description) &gt; 255,  LEFT(Description, 255), Description)</formula>
        <name>Populate Task Description Summary field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CompletionById</fullName>
        <field>CompletionById__c</field>
        <formula>$User.Id</formula>
        <name>Update CompletionById</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completion_Date</fullName>
        <field>Completion_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Priority_of_the_Task</fullName>
        <description>Update the priority of the task to &quot;High&quot;(Reply Email)</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Update the Priority of the Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <flowActions>
        <fullName>Run_Task_Flow</fullName>
        <flow>Set_Reminder_on_Inbound_Email</flow>
        <flowInputs>
            <name>TaskID</name>
            <value>{!Id}</value>
        </flowInputs>
        <label>Run Task Flow</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <rules>
        <fullName>Clear Cancellation Reason on Status change</fullName>
        <actions>
            <name>CancelledReason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>These rule will fire when the status of the task   is changed from &quot;Cancelled&quot; to any other status the Cancellation code would become automatically blank</description>
        <formula>TEXT( Status ) &lt;&gt; &quot;Cancelled&quot; &amp;&amp;  NOT(ISBLANK( TEXT(Reason_Code__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Escalate High Priority Task</fullName>
        <actions>
            <name>Escalate_High_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Task with High Priority is escalated on the due date</description>
        <formula>AND( ActivityDate &lt;Today(), ISPICKVAL(Priority ,&quot;High&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Escalate Medium Priority Task</fullName>
        <actions>
            <name>Escalate_Medium_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Task with Medium Priority is escalated one day after the due date</description>
        <formula>AND( ActivityDate+1 &lt;Today(), ISPICKVAL(Priority ,&quot;Medium&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Completion Fields on Task Completion</fullName>
        <actions>
            <name>Update_CompletionById</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Task Description Summary field</fullName>
        <actions>
            <name>Populate_Task_Description_Summary_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Captures the first 255 characters on the standard Task Description/Comments field</description>
        <formula>OR( ISNEW(), ISCHANGED( Description ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Run Task Flow on Tasks</fullName>
        <actions>
            <name>Run_Task_Flow</name>
            <type>FlowAction</type>
        </actions>
        <active>false</active>
        <description>Sets the Contact Name and CBR on the task. Also sets the task reminder if it is an inbound email</description>
        <formula>OR(  ISNEW(),  ISCHANGED( LastModifiedDate )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the  Email Reply Task Priority</fullName>
        <actions>
            <name>Update_the_Priority_of_the_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Re:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
