<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Format_Home_Phone</fullName>
        <description>Formats the Home Phone Number to (XXX) XXX-XXXX</description>
        <field>HomePhone</field>
        <formula>&quot;(&quot; &amp; MID( HomePhone , 1, 3) &amp; &quot;) &quot; &amp; MID( HomePhone , 4, 3) &amp; &quot;-&quot; &amp; MID( HomePhone , 7, 4)</formula>
        <name>Format Home Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Format_Mobile_Phone</fullName>
        <field>MobilePhone</field>
        <formula>&quot;(&quot; &amp; MID(  MobilePhone  , 1, 3) &amp; &quot;) &quot; &amp; MID( MobilePhone , 4, 3) &amp; &quot;-&quot; &amp; MID( MobilePhone , 7, 4)</formula>
        <name>Format Mobile Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Format_Other_Phone</fullName>
        <field>OtherPhone</field>
        <formula>&quot;(&quot; &amp; MID( OtherPhone , 1, 3) &amp; &quot;) &quot; &amp; MID( OtherPhone , 4, 3) &amp; &quot;-&quot; &amp; MID( OtherPhone , 7, 4)</formula>
        <name>Format Other Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Format_Work_Phone</fullName>
        <field>Phone</field>
        <formula>&quot;(&quot; &amp; MID(  Phone  , 1, 3) &amp; &quot;) &quot; &amp; MID( Phone , 4, 3) &amp; &quot;-&quot; &amp; MID( Phone , 7, 4)</formula>
        <name>Format Work Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Format Home Phone</fullName>
        <actions>
            <name>Format_Home_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Formats imported Home Phone numbers to (XXX) XXX-XXXX</description>
        <formula>ISCHANGED( HomePhone )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Format Mobile Phone</fullName>
        <actions>
            <name>Format_Mobile_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Formats imported Mobile Phone numbers to (XXX) XXX-XXXX</description>
        <formula>ISCHANGED(  MobilePhone )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Format Other Phone</fullName>
        <actions>
            <name>Format_Other_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Formats imported Other Phone numbers to (XXX) XXX-XXXX</description>
        <formula>ISCHANGED( OtherPhone )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Format Work Phone</fullName>
        <actions>
            <name>Format_Work_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Formats imported Work Phone numbers to (XXX) XXX-XXXX</description>
        <formula>ISCHANGED( Phone )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
