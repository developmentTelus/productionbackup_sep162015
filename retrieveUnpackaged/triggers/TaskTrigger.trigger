/*

    Author : Deepak Malkani.
    Created Date : Feb 10 2015
    Purpose : Used to perfrom all trigger logic based on Task
*/


trigger TaskTrigger on Task (before insert, after insert, before update, after update) {

    //We Adopt a Common Trigger Framework here
    //Initialise all Handlers and Collections here
    TaskTriggerHandler handler = new TaskTriggerHandler();
    Set<ID> newOwnerIdSet = new Set<ID>();
    Set<ID> oldOwnerIdSet = new Set<ID>();

    if(trigger.isBefore){
        if(trigger.isInsert)
        {
			handler.setTaskFields(trigger.new);
        }
        if(trigger.isUpdate && Outcome_StaticVars.canIRun() == true)
        {
            system.debug('---> Inside before update. value of can I run is '+Outcome_StaticVars.canIRun());
            handler.setTaskFields(trigger.new);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
	
	    }
        if(trigger.isUpdate && (test.isRunningTest() || Outcome_StaticVars.canIRun() == true)){
            system.debug('---> Inside after update. value of can I run is '+Outcome_StaticVars.canIRun());
            newOwnerIdSet = handler.getNewOwnerIds(trigger.newMap);
            oldOwnerIdSet = handler.getOldOwnerIds(trigger.oldMap);        
            handler.createTaskHistEnteries(trigger.oldMap, trigger.newMap, newOwnerIdSet, oldOwnerIdSet);
            Outcome_StaticVars.stopTrigger();
        }
    }

    //Clear off all the collections
    newOwnerIdSet.clear();
    oldOwnerIdSet.clear();
}