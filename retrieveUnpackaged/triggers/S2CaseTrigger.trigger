// **************************************************************************************************************
// Name:            S2CaseTrigger
// Description:     Trigger for the Case object
// Description:     All trigger logic for Case object
// ************************Version Updates***********************************************************************
//
// Updated Date     Updated By      Update Comments 
//
// **************************************************************************************************************

/****************************************************************************************************************
// Name : Deepak Malkani
// Modified Date : Feb 26 2015
// Purpose : Add Create Relevant Task flow logic to this trigger, since flow is not scalable.
//***************************************************************************************************************
// Name : Deepak Malkani
// Modified Date : April 02 2015
// Purpose : Replaced the flow trigger - run tasks on flow with apex based trigger to scale the flow logic 

//***************************************************************************************************************
   Name : Deepak Malkani.
   Modified Date : 13 July 2015
   Purpose : Registered RTS Exception Handler to the trigger.
////***************************************************************************************************************
   Name : Praveen Bonalu.
   Modified Date : 01 August 2015
   Purpose : Modified Trigger to create Proactive Renewal and OMS Tasks   
*/
 
trigger S2CaseTrigger on Case (after insert, after update, before delete,before insert, before update) 
{
    //Initailise all Handlers here
    CaseTriggerHandler handler = new CaseTriggerHandler();
    CaseTriggerHandler_Flows caseFlowHandler = new CaseTriggerHandler_Flows();

    

    if(trigger.isBefore){

        if(trigger.isInsert && handler.getETLLoadCS() == false){
            handler.prepareCaseCollections(trigger.new);
        }

        if((trigger.isUpdate && Outcome_StaticVars.canIRun() == true )) 
        {


            if (handler.getETLLoadCS() == false) {

        ////// Updates by regular user

        handler.closeReminderCaseTask(trigger.newMap, trigger.oldMap);

        S2CaseUtil.checkforOpenTask(Trigger.New); // Check for Open Tasks for the cloased Cases

        handler.prepareCaseChangeCollections(trigger.newMap, trigger.oldMap);

      } else {

        ////// Updates by Integration User
        handler.reassignBackToUserFromIntegration(trigger.newMap, trigger.oldMap);
      }
    }
    }

    if(trigger.isAfter){

        if(trigger.isInsert && handler.getETLLoadCS() == false){
            handler.createTasksforCases(trigger.newMap);
        }
        //Always fire this trigger irrespective of Custom Setting Values.
        //TO DO : Aneeq and Haydar : Please uncomment this when RTS Exception goes live.
      /*  if(trigger.isInsert)
        {
          exceptionHandler.createTimersonInserts(trigger.newMap);
        }*/
        if(trigger.isUpdate && Outcome_StaticVars.canIRun() == true){
            
            if (handler.getETLLoadCS() == false) {
                handler.getRPTRenewalCase(trigger.newMap, trigger.oldMap);
                handler.updateTaskOwnerforCaseUpd(trigger.newMap, trigger.oldMap);
                handler.createProactiveRenewalTasks(trigger.newMap, trigger.oldMap);
                handler.createOMSTasks(trigger.newMap, trigger.oldMap);
                Outcome_StaticVars.stopTrigger();
            }
            //always fire updates irrespective of custom setting values
      CaseFlowHandler.createRTSTasks(trigger.newMap, trigger.oldMap);
      //TO DO : Aneeq and Haydar : Please uncomment this when RTS Exception goes live.
    //  handler.checkCaseExceptions(trigger.oldMap, trigger.newMap);
        }
    }
}