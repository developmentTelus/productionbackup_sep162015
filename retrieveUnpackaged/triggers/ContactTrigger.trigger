/*
###########################################################################
# File..................: ContactTrigger
# Version...............: 1.0
# Created by............: TechM 
# Created Date..........: 17-Apr-2015
# Description...........: Trigger for Contact DML operation

# Copyright (c) Tech Mahindra. All Rights Reserved.
#
# Created by the Tech Mahindra. Modification must retain the above copyright #notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Bell Mobility, is hereby forbidden. Any modification to #source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
*/
trigger ContactTrigger on Contact (after insert, before delete, before insert, 
before update) {
        
    //initialized the CaseTriggerHandler only for customSetting for integration user
    //CaseTriggerHandler handler = new CaseTriggerHandler();
    ContactTriggerHandler handler = new ContactTriggerHandler();
    CustomSettingHandler CSHandler = new CustomSettingHandler();
    
    if(trigger.isBefore){
        if(trigger.isInsert && CSHandler.getETLLoadCS() == false){
            
        }
        if((trigger.isUpdate && Outcome_StaticVars.canIRun() == true && CSHandler.getETLLoadCS() == false && CSHandler.getAPILoads() == false)){
            //Method call to disallow dml update if SourceRefId is present
            ContactTriggerHandler.doNotAllowDML(Trigger.New);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert)
        {
            if(CSHandler.getETLLoadCS() == false)
            {

            }
            if(CSHandler.getAPILoads() == true)
            {
                System.debug('---> After insert trigger has fired for contacts');
                ContactTriggerHandler.createAccntContReln(trigger.new);
            }
        }
        if(trigger.isUpdate && CSHandler.getETLLoadCS() == false && Outcome_StaticVars.canIRun() == true){
            
        }
    }
    

}