/*
*	Author : Deepak Malkani.
*	Modified Date : 11/06/2015
*	Purpose : Trigger code modified to throw error message incase user tries to change sendername.
*/

trigger UserTrigger on User (before update, before insert, after update, after insert) {

	/*
	HH: NOTE TO DEVELOPER: As of Jun 2015, this User trigger is only dealing with one issue (that is the myagent@telus.com issu)
	for this reason, it was not built with the full infrastructe and was made simple.

	If more scenarios are required to be handled by the User trigger, then please follow the design used
	in other triggers
	*/
	/*
	* Deepak Malkani : Trigger designed to follow a common trigger pattern
	*/
	//Initialise all Handlers here
	UserTriggerHandler handler = new UserTriggerHandler();
	if(trigger.isBefore){
		
		
		if(trigger.isUpdate && Outcome_StaticVars.canIRun() == true){
			// HH: Fix the myagent@telus.com issue
			handler.validateuserSenderName(trigger.new);
		} 
		if(trigger.isInsert){
			
		}
	}
	if(trigger.isAfter){
		if(trigger.isUpdate && Outcome_StaticVars.canIRun() == true){
			 Outcome_StaticVars.stopTrigger();
		}
		if(trigger.isInsert){
			
		}
	}
	
     
}