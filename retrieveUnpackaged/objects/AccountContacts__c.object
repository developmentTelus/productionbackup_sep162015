<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AccountContactCompKey__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Composite key to capture Account and Contact Relationship separated by |</description>
        <externalId>true</externalId>
        <label>AccountContactCompKey</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>AccountId__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Lookup field for Account objects</inlineHelpText>
        <label>AccountId</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>AccountName</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>AccountRefId__c</fullName>
        <description>Stores External Key value for associated Account.</description>
        <externalId>true</externalId>
        <label>AccountRefId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Summary__c</fullName>
        <description>Compound field to describe the contact&apos;s associated account</description>
        <externalId>false</externalId>
        <formula>TEXT(AccountId__r.Brand__c)  &amp; &quot; - &quot; &amp;  TEXT(AccountId__r.Type) &amp; &quot; - &quot; &amp;  AccountId__r.AccountNumber &amp; &quot; - &quot; &amp;  AccountId__r.Name</formula>
        <inlineHelpText>Brand - Type - Account No. - Account Name</inlineHelpText>
        <label>Account Summary</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Bill_Cycle_Day__c</fullName>
        <externalId>false</externalId>
        <formula>AccountId__r.Bill_Cycle_Day__c</formula>
        <label>Bill Cycle Day</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ContactId__c</fullName>
        <externalId>false</externalId>
        <label>ContactId</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>ContactName</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ContactRefId__c</fullName>
        <description>Captures the External Source System Key of  Contact</description>
        <externalId>true</externalId>
        <label>ContactRefId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Effective_End_Date__c</fullName>
        <description>Effective End Date</description>
        <externalId>false</externalId>
        <label>Effective End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Effective_Start_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Effective Start Date</inlineHelpText>
        <label>Effective Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Relationship__c</fullName>
        <description>Indicates the relationship between the contact individual and the customer</description>
        <externalId>false</externalId>
        <label>Relationship</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <description>The role that the contact individual plays with respect to the Customer</description>
        <externalId>false</externalId>
        <label>Role</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SourceRefIDCount__c</fullName>
        <externalId>false</externalId>
        <formula>LEN( SourceRefID__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SourceRefIDCount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SourceRefIDNo__c</fullName>
        <externalId>false</externalId>
        <formula>VALUE(RIGHT(SourceRefID__c, LEN(SourceRefID__c)-3))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SourceRefIDNo</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SourceRefID__c</fullName>
        <externalId>true</externalId>
        <label>SourceRefID</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Account Contacts</label>
    <listViews>
        <fullName>All</fullName>
        <columns>SourceRefID__c</columns>
        <columns>NAME</columns>
        <columns>AccountId__c</columns>
        <columns>Bill_Cycle_Day__c</columns>
        <columns>ContactId__c</columns>
        <columns>Effective_End_Date__c</columns>
        <columns>Effective_Start_Date__c</columns>
        <columns>Account_Summary__c</columns>
        <columns>Relationship__c</columns>
        <columns>Role__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Junction_Objects_with_Relationships</fullName>
        <columns>NAME</columns>
        <columns>Relationship__c</columns>
        <columns>Role__c</columns>
        <columns>ContactId__c</columns>
        <columns>AccountId__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Relationship__c</field>
            <operation>contains</operation>
            <value>N</value>
        </filters>
        <label>Junction Objects with Relationships</label>
    </listViews>
    <nameField>
        <displayFormat>AC-{000000}</displayFormat>
        <label>AccountContacts Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Contacts</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Make_AccountRefId_Conditional_Required</fullName>
        <active>true</active>
        <description>Make AccountRefId key conditionally required.</description>
        <errorConditionFormula>AND(ISBLANK( AccountRefId__c ) , IF( $Setup.API_Loads__c.Activate_Conditional_Required_fields__c == true, true, false))</errorConditionFormula>
        <errorMessage>AccountRefID is a required external key on Account Contact. Please make sure you populate this field value.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Make_ContactRefId_Conditional_Required</fullName>
        <active>true</active>
        <description>Make ContactRefId key conditionally required.</description>
        <errorConditionFormula>AND(ISBLANK(  ContactRefId__c ) , IF( $Setup.API_Loads__c.Activate_Conditional_Required_fields__c == true, true, false))</errorConditionFormula>
        <errorMessage>ContactRefID is an required external key on Account Contact. Please make sure you populate this field value.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Make_SourceRefId_conditionally_required</fullName>
        <active>false</active>
        <description>SourceRefId is a required field for Accounts. Make sure you provide the Source key while inserting/updating any account records in Outcome.</description>
        <errorConditionFormula>AND(ISBLANK(SourceRefID__c), CONTAINS( $Profile.Name , &apos;API&apos;), IF($Setup.API_Loads__c.Activate_Conditional_Required_fields__c= true, true, false) )</errorConditionFormula>
        <errorMessage>SourceRefId is a required field for Account Contact. Make sure you provide the Source key while inserting/updating any account contact records in Outcome.</errorMessage>
    </validationRules>
</CustomObject>
