/*
	Author : Deepak Malkani.
	Created Date : July 22 2015
	Purpose : Batch job used to to a mass update on Source Ref Id key for Accounts. This is needed to ensure ETL does not create
	duplicate records based on SourceRefId.
*/

global class AccountExternalKeyUpdate_Batch implements Database.Batchable<sObject> {
	
	String query;
	
	global AccountExternalKeyUpdate_Batch() {
		query = 'SELECT id, Name, SourceRefId__c, SourceRefId_Backup__c, Type, Billing_Account_Number__c FROM Account WHERE NOT SourceRefId__c LIKE \'C_%\'';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		List<Account> accntUpdList = new List<Account>();
		//The query will return a large volume of records, which will be chunked by default batch scope
		//This large volume of returned accounts are all Billing Accounts of type FFH or Mobility
		for(Account a : (List<Account>) scope)
		{
			Account acc = new Account(Id = a.Id);
			acc.SourceRefId_Backup__c = a.SourceRefId__c;
			if(a.Type.tolowerCase() == 'ffh')
				acc.SourceRefId__c = 'Enabler_'+a.Billing_Account_Number__c;
			else if (a.Type.toLowerCase() == 'mobility')
				acc.SourceRefId__c = 'KB_'+a.Billing_Account_Number__c;
			accntUpdList.add(acc);
		}
		
		if(!accntUpdList.isEmpty())
			update accntUpdList;
		accntUpdList.clear();	
	}
	
	global void finish(Database.BatchableContext BC) {
		
		//Finish Block will send an email
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
        		          FROM AsyncApexJob 
        		          WHERE Id =: BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'aneeq.hashmi@telus.com','bj.jancovic@telus.com','greg.lebel@telus.com', 'vincent.ng@telus.com', 'shiva.sayahi@telus.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Account Source Ref Id Key Update ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
}