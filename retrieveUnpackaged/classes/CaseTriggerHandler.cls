/*
	Author : Deepak Malkani.
	Created Date : Feb 26 2015
	Purpose : Common Handler class for all Case related Business logic.	
*/
/********************************************************************************************************************
*
*	Modified By : Deepak Malkani.
*	Modified Date : April 14 2015
*	Modifiecation Reason : JIRA CES20-112 : RTS Cases if owner changed to inetgration User, need to set back to agent
*
*********************************************************************************************************************/ 
/********************************************************************************************************************
*	Author : Deepak Malkani.
*	Modified Date : May 11 2015
*	Purpose : Added method for RTS Renewal get Case - JIRA # CES20-299
*********************************************************************************************************************/
public with sharing class CaseTriggerHandler {
	public CaseTriggerHandler() {
		
	}

	/*
		Author : Deepak Malkani.
		Methos used to Bypass Case Trigger Logic for Integration User
	*/

	public Boolean getETLLoadCS(){
		ETLLoads__c CS = ETLLoads__c.getInstance(UserInfo.getUserId());
		return CS.runDataLoads__c;
	}
	/*
		Author : Deepak Malkani.
		Method used to define entry criteria for RTS Exceptions on Case Update
	*/
	//To Do : Aneeq and Haydar : Please uncomment this code when RTS Exception feature needs to be turned on
	
/*	public void checkCaseExceptions(Map<ID, Case> oldCaseMap, Map<ID, Case> newCaseMap){
		CaseExceptionHandler exceptionHandler = new CaseExceptionHandler();
		Map<ID, Case> csMap = new Map<ID, Case>();
		for(Case csObj : newCaseMap.values())
		{
			if(csObj.Source_Type__c != null && csObj.SourceStatus__c != oldCaseMap.get(csObj.id).SourceStatus__c)
				csMap.put(csObj.Id, csObj);
		}
		if(!csMap.isEmpty())
			exceptionHandler.createTimersonUpdates(csMap);
	}*/
	/*
		Author : Deepak Malkani.
		Method used to prepare all collections to update newly created cases
	*/
	public void prepareCaseCollections(List<Case> newCaseList){
		
		Map<ID, Case> csMap = new Map<ID, Case>();
		Set<ID> caseOwnerSet = new Set<ID>();
		Set<ID> caseContactSet = new Set<ID>();
		Set<ID> caseAccntSet = new Set<ID>();
		for(Case csObj : newCaseList)
		{
			String owner = csObj.OwnerId;
			if(csObj.X1st_time_trigger__c == false && owner.substring(0,3) == '005')
			{
				csMap.put(csObj.Id, csObj);
				caseOwnerSet.add(csObj.OwnerId);
				caseContactSet.add(csObj.ContactId);
				caseAccntSet.add(csObj.AccountId);
			}
		}

		updateCases(csMap, caseOwnerSet, caseContactSet, caseAccntSet, 'FOR INSERT');
		
	}

	/*
		Author : Deepak Malkani.
		Method used to prepare all collections for cases which belong to Mobility Queue and are then later on 
		assigned to the agent, using Get Case Button
	*/
	public void prepareCaseChangeCollections(Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap)
	{
		Map<ID, Case> csMap = new Map<ID, Case>();
		Set<ID> caseOwnerSet = new Set<ID>();
		Set<ID> caseContactSet = new Set<ID>();
		Set<ID> caseAccntSet = new Set<ID>();

		for(Case caseObj : newCaseMap.values())
		{
			String oldOwner = oldCaseMap.get(caseObj.Id).OwnerId;
			String newOwner = newCaseMap.get(caseObj.Id).OwnerId;
			if((newCaseMap.get(caseObj.Id).OwnerId != oldCaseMap.get(caseObj.Id).OwnerId) && Helper.isQueue(oldOwner) && Helper.isUser(newOwner))
			{
				csMap.put(caseObj.Id, caseObj);
				caseOwnerSet.add(caseObj.OwnerId);
				caseContactSet.add(caseObj.ContactId);
				caseAccntSet.add(caseObj.AccountId);
			}
		}
		updateCases(csMap, caseOwnerSet, caseContactSet, caseAccntSet, 'FOR UPDATE');
		
		//destroy all collections
		csMap.clear();
		caseOwnerSet.clear();
		caseContactSet.clear();
		caseAccntSet.clear();		

	}



	/**
	* @author       Haydar Hadi
	* @date         
	* @description  Used by trigger. If ETL change the owner of a case to "integration user", then this method will change the owner back to what it was before ETL update.
	* @return       void
	*/
	public void reassignBackToUserFromIntegration(Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap)
	{
		System.assert(Trigger.isUpdate);
		System.assert(Trigger.isBefore);

		Set<Id> integrationUsers = (new Map<Id, User>([SELECT Id FROM User WHERE Name = 'Integration User1'])).keyset();

		if (integrationUsers.isEmpty()) return; // this may happen during test if no integration user is setup

		for(Case newCase : newcaseMap.values())
		{
			Case oldCase = oldCaseMap.get(newCase.Id);

			if (integrationUsers.contains(newCase.OwnerId))

				newCase.OwnerId = oldCase.OwnerId;
		}
	}

	/**
	* @author       Haydar Hadi
	* @date         06-JUN-2015
	* @description  Used by trigger. creates Tasks for Proactive Renewal Cases when ETL uploads data
	* @jira         CES20-238
	* @return       void
	*/
	public void createProactiveRenewalTasks(Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap)
	{
		List<Case> caseList = new List<Case>();
		for(Case newCase : newCaseMap.values())
		{
			if (!Helper.isProactiveRenewal(newCase)) continue;

			Case oldCase = oldCaseMap.get(newCase.Id);

			// case moved from queue to user -- Make sure the task is created only when agents do a get case which happens only when ownership changes from a queue to a user
			Boolean movedFromQueueToUser = 
					Helper.isQueue(oldCase.OwnerId) &&
					Helper.isUser(newCase.OwnerId);	

			// Assumption: if the case source type is "LNR-RENEWAL" and came from queue, the queue must be "MOB_Renewals_Proactive_EN"
			if (movedFromQueueToUser) {
				caseList.add(newCase);
			}
		}

		if(!caseList.isEmpty()) {

			List<Task> tasks = new List<Task>();
	
			for (Case cse : caseList) {
				tasks.add(createTask(cse, Date.today(), 'Renewal Call', 'N/A'));
			}

			insert tasks;			
		}
	}	


	/**
	* @author       Haydar Hadi
	* @date         12-JUN-2015
	* @description  Used by trigger. creates Tasks for OMS Cases when ETL uploads data
	* @jira         CES20-351
	* @return       void
	*/
	public void createOMSTasks(Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap)
	{
		List<Case> caseList = new List<Case>();
		for(Case newCase : newCaseMap.values())
		{
			if (!Helper.isOMS(newCase)) continue;

			Case oldCase = oldCaseMap.get(newCase.Id);

			// case moved from queue to user -- Make sure the task is created only when agents do a get case which happens only when ownership changes from a queue to a user
			Boolean movedFromQueueToUser = 
					Helper.isQueue(oldCase.OwnerId) &&
					Helper.isUser(newCase.OwnerId);	

			// Assumption: if the case source type is "LNR-RENEWAL" and came from queue, the queue must be "MOB_Renewals_Proactive_EN"
			if (movedFromQueueToUser) {
				caseList.add(newCase);
			}
		}

		if(!caseList.isEmpty()) {

			List<Task> tasks = new List<Task>();
	
			for (Case cse : caseList) {
				System.debug('cse.Due_Date__c: ' + cse.Due_Date__c);
				System.debug('cse.Next_Billing_Date1__c: ' + cse.Next_Billing_Date1__c);
				        
				        
				if (cse.Due_Date__c!=null) {
					System.debug('added Due Date');
					tasks.add(createTask(cse, cse.Due_Date__c.addDays(-1),          'Pre-Appointment Review', ''));
					tasks.add(createTask(cse, cse.Due_Date__c.addDays(+2),          'Courtesy Call', ''));
					tasks.add(createTask(cse, cse.Due_Date__c.addDays(-3),          'Order Review', ''));
				}
				if (cse.Next_Billing_Date1__c!=null) {
					System.debug('added Nex Billing Day');
					tasks.add(createTask(cse, cse.Next_Billing_Date1__c.addDays(+5), 'Bill Review', ''));
				}
			}

			insert tasks;			
		}
	}	

	// helper method to create task quickly
	private Task createTask(Case cse, Date dueDate, String subject, String description) {
		Task task = new Task();
		
		task.Description = description;
		task.ActivityDate = dueDate;
		task.Subject = subject;

		task.WhatId = cse.Id;
		task.OwnerId = cse.OwnerId;
		task.Priority = 'Medium';

		if (!String.isEmpty(cse.ContactId)) {
			task.WhoId = cse.Contact.Id;
		}

		return task;
	}


	/*
		Author : Deepak Malkani.
		Method used to perform all Updates on Cases, replacing create Task for Case Flow. We set all fields in Before Update

	*/
	public void updateCases(Map<ID, Case> caseMap, Set<ID> caseOwnerSet, Set<ID> caseContactSet, Set<ID> caseAccntSet, String onDML){
		
		//Initialise all Collections 
		Map<ID, Contact> casecontactMap = new Map<ID, Contact>([SELECT id, LanguagePreference__c 
																FROM Contact
																WHERE id IN : caseContactSet]);
		Map<ID, Account> caseaccntMap = new Map<ID, Account>([SELECT id, Language_Preference__c, Next_Billing_Date__c, Type
																FROM Account
																WHERE id IN : caseAccntSet]);
		List<Case> caseUpList = new List<Case>();
		
		//The logic here is used to replace flow and makes sure cases are updated with LOB, Lang Pref and Next Billing Dates		
		for(Case csObj : caseMap.values())
		{
			if(String.isEmpty(csObj.Language_Preference__c))
			{
					if(!casecontactMap.isEmpty())
					{
						if(casecontactMap.get(csObj.ContactId).LanguagePreference__c != null)
						{
							csObj.Language_Preference__c = casecontactMap.get(csObj.ContactId).LanguagePreference__c;
						}
					}
					else if (!caseaccntMap.isEmpty())
					{
						if(caseaccntMap.get(csObj.AccountId).Language_Preference__c != null)
							csObj.Language_Preference__c = caseaccntMap.get(csObj.AccountId).Language_Preference__c;
					}
					else
					{
						csObj.Language_Preference__c = 'English';
					}
			}
			if(!caseaccntMap.isEmpty())
			{
				if(csObj.Next_Billing_Date1__c == null && caseaccntMap.get(csObj.AccountId).Next_Billing_Date__c != null)
					csObj.Next_Billing_Date1__c = caseaccntMap.get(csObj.AccountId).Next_Billing_Date__c;
				if((csObj.Line_of_Business__c == null || csObj.Line_of_Business__c == '') && caseaccntMap.get(csObj.AccountId).Type != null)
					csObj.Line_of_Business__c = caseaccntMap.get(csObj.AccountId).Type;
			}
		}

		if(onDML == 'FOR UPDATE')
			createTaskforGetCase(caseMap);

		//destroy all collections
		casecontactMap.clear();
		caseaccntMap.clear();
		caseUpList.clear();
	}
	
	/*
		Author : Deepak Malkani.
		This method updates task ownerid, if case ownerid changes -- cant use flows/process builder due to bulkification limitations
		https://success.salesforce.com/ideaView?id=08730000000DhBlAAK&sort=2
	*/
	public void updateTaskOwnerforCaseUpd(Map<ID, Case> newcaseMap, Map<ID, Case> oldCaseMap)
	{
		Set<ID> caseIds = new Set<ID>();
		List<Task> tkobjList = new List<Task>();
		for(Case csObj : newCaseMap.values())
		{
			String Owner = csObj.OwnerId;
			if(
				newcaseMap.get(csObj.Id).OwnerId != oldCaseMap.get(csObj.Id).OwnerId 
				&& csObj.X1st_time_trigger__c 
				&& Helper.isUser(owner)
			)
				caseIds.add(csObj.Id);
		}
		
		if(!caseIds.isEmpty())
		{
			for(Task tks : 
				[
					SELECT id, OwnerId, WhatId 
					FROM Task 
					WHERE WhatId IN : caseIds
				])
			{
				Task tkUpd = new Task (Id = tks.Id);
				tkUpd.OwnerId = newCaseMap.get(tks.WhatId).OwnerId;
				tkobjList.add(tkUpd);
			}							
		}
		
		if(!tkobjList.isEmpty())
			update tkobjList;
		//clear off all collections
		caseIds.clear();
		tkObjList.clear();
	}

	/*
		Author : Deepak Malkani
		This method will be invoked when agent clicks on GetCase button, which creates mandatory tasks for cases on OwnerChange
	*/
	public void createTaskforGetCase(Map<ID, Case> caseMap)
	{
		//Get user preference based for the users
		Map<ID, UserPreference> userPrefMap = new Map<ID, UserPreference>();
		Map<ID, User> userMap = new Map<ID, User>();
		List<Task> tknewList = new List<Task>();
		Set<ID> ownerIds = new Set<ID>();

		//The owner Id would have been changed by new, since this block is called in After Update
		for(Case csObj : caseMap.values())
			ownerIds.add(csObj.OwnerId);

		for(UserPreference userPrfObj : [SELECT Id, UserId, Preference, Value FROM UserPreference WHERE UserId IN : ownerIds])
			userPrefMap.put(userPrfObj.UserId, userPrfObj);

		for(User userObj : [SELECT id, TimeZoneSidKey, UserPreferencesTaskRemindersCheckboxDefault FROM User WHERE id IN : ownerIds])
			userMap.put(userObj.Id, userObj);

		//Create all Mandatory Tasks
		for(Case csObj : caseMap.values())
		{
			system.debug('---> the value of case obj fields are '+csObj.Order_Review__c+','+csObj.Pre_Appointment_Review__c+','+csObj.Outcome_Review__c+','+csObj.Close_Review__c);
			if(!userMap.isEmpty())
			{
				Boolean isRun = true;
				String sourceRefId = csObj.SourceRefID__c;
				//Deepak Malkani : Get Case - default behavior should not work for RTS or RPT1-Renewals
				if(
					Helper.isRTS(csObj) || 
					Helper.isRPT1Renewal(csObj) ||
					Helper.isProactiveRenewal(csObj)
				)
					isRun = false;
				
				if(isRun)
				{
					if(csObj.Order_Review__c)
						tknewList.add(createTask(csObj.Order_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Order Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
					if(csObj.Pre_Appointment_Review__c)
						tknewList.add(createTask(csObj.Pre_Appointment_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Pre-Appointment Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));						
					if(csObj.Outcome_Review__c)
						tknewList.add(createTask(csObj.Outcome_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Courtesy Call', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
					if(csObj.Close_Review__c)
						tknewList.add(createTask(csObj.Close_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Bill Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
					if(csObj.Follow_up__c)
						tknewList.add(createTask(csObj.Follow_up_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Follow Up', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				}		
			}				
		}
		
		if(!tknewList.isEmpty()) {
			System.debug('inserting tasks. size:' + tknewList.size());
			for (Task t : tknewList) {
				System.debug('Task.subject:' + t.Subject);
			}
			insert tknewList;
			
		}
		
		//destroy all collections
		userPrefMap.clear();
		userMap.clear();
		tknewList.clear();
		ownerIds.clear();
	}

	/*
		Author : Deepak Malkani.
		This method is used to create Tasks for Cases, based on scenarios user/agent selects from the case wizard.
	*/

	public void createTasksforCases(Map<ID, Case> newcaseMap)
	{
		//Get user preference based for the users
		Map<ID, UserPreference> userPrefMap = new Map<ID, UserPreference>();
		Map<ID, User> userMap = new Map<ID, User>();
		List<Task> tknewList = new List<Task>();
		Map<ID, Case> caseMap = new Map<ID, Case>();
		Set<ID> ownerIds = new Set<ID>();

		for(Case cs : newCaseMap.values())
		{
			String owner = cs.OwnerId;
			if(cs.X1st_time_trigger__c == false && owner.substring(0,3) == '005')
			{
				caseMap.put(cs.Id, cs);
				ownerIds.add(cs.OwnerId);
			}
		}

		for(UserPreference userPrfObj : [SELECT Id, UserId, Preference, Value FROM UserPreference WHERE UserId IN : ownerIds])
			userPrefMap.put(userPrfObj.UserId, userPrfObj);

		for(User userObj : [SELECT id, TimeZoneSidKey, UserPreferencesTaskRemindersCheckboxDefault FROM User WHERE id IN : ownerIds])
			userMap.put(userObj.Id, userObj);

		//Create Conditional Mandatory Tasks
		for(Case csObj : caseMap.values())
		{	
			Boolean runFlg = true;
			system.debug('---> inside the case task creation block');
			if(
				Helper.isRTS(csObj) || 
				Helper.isRPT1Renewal(csObj) ||
				Helper.isProactiveRenewal(csObj)
			)
				runFlg = false;
			//Deepak Malkani : Made a change in the entry criteria, so that tasks are not created as normal for RTS or RPT1-Renewals, via wizard.
			if(runFlg)
			{
				if((csObj.Type == 'Move my service(s)' || csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Make changes to my existing acct/service') && (csObj.Line_of_Business__c == 'FFH') && (csObj.Order_Review__c))	
					//Create Mandatory Order Review Task
					tknewList.add(createTask(csObj.Order_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Order Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				else if ((csObj.Type == 'Move my service(s)' || csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Make changes to my existing acct/service') && (csObj.Line_of_Business__c == 'FFH') && (csObj.Order_Review__c == false))
					//create Order Review Cancelled Tasks
					tknewList.add(createTask(csObj.Order_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Cancelled', 'Order Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, csObj.Reason_Code__c));		
				else if (csObj.Type == 'Move my service(s)' && csObj.Line_of_Business__c == 'Mobility' && csObj.Order_Review__c)	
					//Mandatory Order Review Task
					tknewList.add(createTask(csObj.Order_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Order Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));				
				else if (csObj.Type == 'Move my service(s)' && csObj.Line_of_Business__c == 'Mobility' && !csObj.Order_Review__c)
					//Mandatory Order Review Cancelled Task
					tknewList.add(createTask(csObj.Order_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Cancelled', 'Order Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, csObj.Reason_Code__c));				
				else if ((csObj.Type == 'Get a better deal') && (csObj.Line_of_Business__c == 'FFH' || csObj.Line_of_Business__c == 'Mobility') && csObj.Order_Review__c)
					//Create Optional Order Review Tasks
					tknewList.add(createTask(csObj.Order_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Order Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				if((csObj.Type == 'Move my service(s)' || csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Get a better deal' || csObj.Type == 'Get my product/services working' || csObj.Type == 'Make changes to my existing acct/service') && (csObj.Line_of_Business__c == 'FFH' || csObj.Line_of_Business__c == 'Mobility') && csObj.Outcome_Review__c)
					//Create Mandatory Courtesy Call Task
					tknewList.add(createTask(csObj.Outcome_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Courtesy Call', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				else if ((csObj.Type == 'Move my service(s)' || csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Get a better deal' || csObj.Type == 'Get my product/services working' || csObj.Type == 'Make changes to my existing acct/service') && (csObj.Line_of_Business__c == 'FFH' || csObj.Line_of_Business__c == 'Mobility') && !csObj.Outcome_Review__c)
					//Create Mandatory Courtesy Call Task
					tknewList.add(createTask(csObj.Outcome_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Cancelled', 'Courtesy Call', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, csObj.Reason_Code__c));
				else if((csObj.Type == 'Learn about a product/service') && (csObj.Line_of_Business__c == 'Mobility' || csObj.Line_of_Business__c == 'FFH') && (csObj.Outcome_Review__c))
					//Create Optional Courtesy Call Task
					tknewList.add(createTask(csObj.Outcome_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Courtesy Call', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				if ((csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Get a better deal' || csObj.Type == 'Move my service(s)' || csObj.Type == 'Get my product/services working' || csObj.Type == 'Make changes to my existing acct/service' || csObj.Type == 'Inquire, dispute or pay my bill' || csObj.Type == 'Cancel my service(s)') && (csObj.Line_of_Business__c == 'Mobility' || csObj.Line_of_Business__c == 'FFH') && (csObj.Close_Review__c))
					//Create Mandatory Bill Review Tasks
					tknewList.add(createTask(csObj.Close_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Bill Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				else if ((csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Get a better deal' || csObj.Type == 'Move my service(s)' || csObj.Type == 'Get my product/services working' || csObj.Type == 'Make changes to my existing acct/service' || csObj.Type == 'Inquire, dispute or pay my bill' || csObj.Type == 'Cancel my service(s)') && (csObj.Line_of_Business__c == 'Mobility' || csObj.Line_of_Business__c == 'FFH') && (!csObj.Close_Review__c))	
					//Create Cancelled Bill Review Tasks
					tknewList.add(createTask(csObj.Close_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Cancelled', 'Bill Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, csObj.Reason_Code__c));
				if((csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Get a better deal' || csObj.Type == 'Move my service(s)' || csObj.Type == 'Get my product/services working' || csObj.Type == 'Make changes to my existing acct/service') && (csObj.Line_of_Business__c == 'FFH') && (csObj.Pre_Appointment_Review__c))
					//create Mandatory Pre-Appointment Review Tasks
					tknewList.add(createTask(csObj.Pre_Appointment_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Pre-Appointment Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));						
				else if ((csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Get a better deal' || csObj.Type == 'Move my service(s)' || csObj.Type == 'Get my product/services working' || csObj.Type == 'Make changes to my existing acct/service') && (csObj.Line_of_Business__c == 'FFH') && (!csObj.Pre_Appointment_Review__c))
					//create Cancelled Pre-Appointment Review Tasks
					tknewList.add(createTask(csObj.Pre_Appointment_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Cancelled', 'Pre-Appointment Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, csObj.Reason_Code__c));
				else if ((csObj.Type == 'Move my service(s)') && csObj.Line_of_Business__c == 'Mobility' && csObj.Pre_Appointment_Review__c)
					//create Mandatory Pre-Appointment Review Tasks -- for Mobility Buisness Line
					tknewList.add(createTask(csObj.Pre_Appointment_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Pre-Appointment Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));						
				else if ((csObj.Type == 'Move my service(s)') && csObj.Line_of_Business__c == 'Mobility' && (!csObj.Pre_Appointment_Review__c))
					//create Cancelled Pre-Appointment Review Tasks -- for Mobility Business Line
					tknewList.add(createTask(csObj.Pre_Appointment_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Cancelled', 'Pre-Appointment Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, csObj.Reason_Code__c));
				else if ((csObj.Type == 'Get a better deal' || csObj.Type == 'Get my product/services working') && (csObj.Line_of_Business__c == 'Mobility') && csObj.Pre_Appointment_Review__c)
					//Create Optional Pre-Appointment Review Tasks
					tknewList.add(createTask(csObj.Pre_Appointment_Review_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Pre-Appointment Review', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				if((csObj.Type == 'Sign up for a new product/service' || csObj.Type == 'Make changes to my existing acct/service' || csObj.Type == 'Inquire, dispute or pay my bill' || csObj.Type == 'Get a better deal' || csObj.Type == 'Get my product/services working' || csObj.Type == 'Learn about a product/service' || csObj.Type == 'Move my service(s)' || csObj.Type == 'Cancel my service(s)' || csObj.Type == 'Get feedback') && (csObj.Line_of_Business__c == 'FFH' || csObj.Line_of_Business__c == 'Mobility') && csObj.Follow_up__c)
					//Create Optional Follow-up Tasks
					tknewList.add(createTask(csObj.Follow_up_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Follow Up', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));
				else if (csObj.Type == 'Get feedback' && csObj.Follow_up__c)
					//Create Optional Follow-up Tasks
					tknewList.add(createTask(csObj.Follow_up_Due_Date__c, csObj.Can_Be_Reached__c, csObj.OwnerId, 'Medium', 'Not Started', 'Follow Up', csObj.Id, csObj.ContactId, userMap.get(csObj.OwnerId).UserPreferencesTaskRemindersCheckboxDefault, null));					
			}
		}
		system.debug('----> created task '+tknewList);
		if(!tknewList.isEmpty()) {
			System.debug('inserting tasks b. size:' + tknewList.size());
			
			for (Task t : tknewList) {
				System.debug('Task.subject:' + t.Subject);
			}
			
			insert tknewList;
		}

		//Destroy all collections
		userPrefMap.clear();
		userMap.clear();
		tknewList.clear();
		caseMap.clear();
		ownerIds.clear();	
	}

	/*
		Author : Deepak Malkani.
		Method used to create single tasks. Used for modularization.
	*/
	public Task createTask(Datetime taskDueDate, String canbeRchd, ID OwnerId, String Priority, String Status, String Sub, ID whatId, ID whoId, Boolean remindSet, String reasonCd)
	{
		Task tkObj = new Task();
		tkObj.ActivityDate = Date.valueOf(taskDueDate);
		tkObj.Can_Be_Reached__c = canbeRchd;
		tkObj.OwnerId = OwnerId;
		tkObj.Priority = Priority;
		tkObj.Status = Status;
		tkObj.Subject = Sub;
		tkObj.WhatId = whatId;
		tkObj.WhoId = whoId;
		tkObj.IsReminderSet = remindSet;
		tkObj.ReminderDateTime = taskDueDate;
		tkObj.Reason_Code__c = reasonCd;
		return tkObj;	
	}	

	/*
		Author : Deepak Malkani.
		Method used to close the Close Case Reminder task if the case has been closed and this is the only open task available.
	*/
	public void closeReminderCaseTask(Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap)
	{
		Set<ID> caseIds = new Set<ID>();
		List<Task> updTasksList = new List<Task>();
		for(Case csObj : newcaseMap.values())
		{
			String owner = csObj.OwnerId;
			if((newcaseMap.get(csObj.Id).Status != oldCaseMap.get(csObj.Id).Status) && newcaseMap.get(csObj.Id).Status == 'Pending Close' && owner.substring(0,3) == '005')
				//Store these cases in a set/collection
				caseIds.add(csObj.Id);
		}
		system.debug('--> the case which has been collected is '+caseIds);
		//Work on the collected case ids and change the task to closed, if the task is a Close Case Reminder task
		if(!caseIds.isEmpty())
		{
			for(Case caseObj : [SELECT id, CaseNumber, Status,OwnerId, (select id, isClosed, subject,status from tasks where isClosed = false)
								FROM Case
								WHERE id IN : caseIds])
			{
				system.debug('----> case task list size is '+caseObj.Tasks.Size());
				if(caseObj.Tasks.Size() == 1)
				{
					system.debug('---> inside the block. case has one open task only and subject is '+caseObj.Tasks[0].Subject);
					
					if(caseObj.Tasks[0].Subject == 'Close Case Reminder' || caseObj.Tasks[0].Subject == 'Fermer le dossier de rappel' || caseObj.Tasks[0].Subject == 'Fermer  le dossier de rappel') 
						//we need to close this task
						for(Task tkObj : caseObj.Tasks)
						{
							Task tk = new Task(Id = tkObj.Id);
							tk.status = 'Completed';
							updTasksList.add(tk);
						}
				}
			}
		}

		//Close the Close Case Reminder automatically, when the case is changed to Open Pending
		if(!updTasksList.isEmpty())
			update updTasksList;

		//Clear off all the collections
		caseIds.clear();
		updTasksList.clear();
	}

	/*
		Author : Deepak Malkani.
		Purpose : Method created to ensure all RPT1- Renewal Tasks are created when agent goes a get Case
	*/
	public void getRPTRenewalCase(Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap){
		
		//Initialise all Collections
		Set<ID> caseIds = new Set<ID>();
		CaseTriggerHandler_Flows flowHandler = new CaseTriggerHandler_Flows();
		
		for(Case csObj : newCaseMap.values())
		{
			if(csObj.Source_Type__c != null && csObj.Source_Type__c.toLowerCase() == Label.RPT_Renewals_Source_Type && Helper.isUser(csObj.OwnerId) && Helper.isQueue(oldCaseMap.get(csObj.Id).OwnerId))
			{
				system.debug('---->Met evaluation criteria for RPT1 Renewals');
				caseIds.add(csObj.Id);
			}
		}
		if(!caseIds.isEmpty())
			//call the flow.
			flowHandler.launchRPT1RenewalsFlow(caseIds);
		//clear off collections
		caseIds.clear();
	}

	//////////////////////////////////////////////////////
	/////////////// HELPERS //////////////////////////////
	//////////////////////////////////////////////////////

    public static Boolean isRTS(Case cse) {
    	if (cse==null) return false;
    	String source = cse.SourceRefID__c;
    	if (source==null) return false;
    	return source.substring(0,3) == 'RTS';
	}	
}