/*
Author : Praveen Kumar Bonalu.
Created Date : MAR 11 2015
Purpose : Purpose of this Controller Extension is to process all Business Logic related to TaskCompletion VF Page.
*/
public class TaskCompletionCrtl_EXT {
    
    public Task tk{get;set;}
    public boolean isLastTaskForCase {get; set;}
    public boolean initialized {get; set;}
    public string taskComments {get; set;}
    
    public TaskCompletionCrtl_EXT (ApexPages.StandardController controller) {
        isLastTaskForCase = False;
        if(ApexPages.currentPage().getParameters().get('id')!=null)
            tk= [select id ,Description,Status,IsClosed, WhatId from task where id = :ApexPages.currentPage().getParameters().get('id')];
        initialized = tk.IsClosed == false;
        
    }
    
    public pagereference Save()
    {
        
        tk.Status='Completed';
        String textBody = tk.Description +'\n'+'\n'+ taskComments;
        if(textBody.length() <= 32000)
          tk.Description = textBody;
        else
          tk.Description = tk.Description;
        update tk;
        
        return redirectPage();
    }    
    public pagereference redirectPage(){
        List <case> casesList = [Select Id, Status, (Select Id, Status from Tasks Where  IsClosed = false ) from Case WHERE Id = :tk.WhatId AND IsClosed =false];
        if(casesList.size()>0){
            isLastTaskForCase = (casesList.get(0).Tasks.size() == 0);
        }
        pagereference pr = (isLastTaskForCase)? new PageReference('/'+tk.WhatId+'/s?retURL=%2F'+tk.WhatId+'&cas7=Pending Close')  : new ApexPages.StandardController(tk).view() ;
        pr.setRedirect(true);
        return pr;
    }       
}