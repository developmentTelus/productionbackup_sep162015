/*
*	Author : Deepak Malkani.
*	Created Date : 22/06/2015
*	Purpose : Test class to test the Event Publicher web service call out.
*/
@isTest(SeeAllData=false)
private class SFDCEventPublisherService_Test
{
	/*
	*	Written By : Deepak Malkani.
	*	Purpose : Unit Test method used to make a call out to Event Publisher service with
	*			  OMS Specific Data
	*/
	@isTest
	static void UnitTest_1()
	{
		List<SFDCEventPublisherService_v1.EventRecord> evtRecList = new List<SFDCEventPublisherService_v1.EventRecord>();

		//Prepare some test data for Event Manager
		SFDCEventPublisherService_v1.EventRecord evtRec = new SFDCEventPublisherService_v1.EventRecord();
		evtRec.sourceName = 'OMS';
		evtRec.sourceEventType = 'OA Submitted';
		evtRec.sourceRefID = '014525663';
		evtRec.customerID = '12586698';
		evtRec.billingAccountNo = 'B_88587898';
		evtRec.sourceEventJSONMessage = 'My JSON Test Message';
		evtRecList.add(evtRec);
		Test.startTest();
		//Make the call out now
		List<SFDCEventPublisherService_v1.EventResponse> response = SFDCEventPublisherService_v1.createEvents(evtRecList);
		Test.stopTest();
		//Verify the mock call out
		system.assertEquals('Event upserted sucessfully', response[0].responseMessage);
	}

	/*
	*	Written By : Deepak Malkani.
	*	Purpose : Unit Test method used to make a call out to Event Publisher service with
	*			  OMS Specific Negative Data. In this case we dont provide sourcerefId
	*/
	@isTest
	static void UnitTest_2()
	{
		List<SFDCEventPublisherService_v1.EventRecord> evtRecList = new List<SFDCEventPublisherService_v1.EventRecord>();

		//Prepare some test data for Event Manager
		SFDCEventPublisherService_v1.EventRecord evtRec = new SFDCEventPublisherService_v1.EventRecord();
		evtRec.sourceName = 'OMS';
		evtRec.sourceEventType = 'OA Submitted';
		evtRec.sourceRefID = '014525663';
		evtRec.customerID = '12586698';
		evtRec.billingAccountNo = 'B_88587898';
		evtRecList.add(evtRec);
		Test.startTest();
		//Make the call out now
		try{
			List<SFDCEventPublisherService_v1.EventResponse> response = SFDCEventPublisherService_v1.createEvents(evtRecList);
		}
		catch(Exception e)
		{
			Boolean expectedEx = e.getMessage().contains('FIELD_CUSTOM_VALIDATION') ? true: false;
			system.assertEquals(true, expectedEx);
		}
		Test.stopTest();
	}

}