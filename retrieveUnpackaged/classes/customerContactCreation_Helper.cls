/*
    Author : Deepak Malkani.
    Created Date : 05/06/2015
    Purpose : Provides helper methods to the WebService class : customerContactCreation.
*/
/*
    Author : Deepak Malkani.
    Modified Date : 23/06/2015
    Purpose : Web Service Helper method modified to map all the address specific information which now
              also includes Military and International Addresses.
*/
public with sharing class customerContactCreation_Helper {

    
    public class customerException extends Exception{}
    
    @TestVisible public static SFDCCustomerAccntContCreationService_v1.custContResponse customerRecCreate(SFDCCustomerAccntContCreationService_v1.customerRec cusRec)
    {
        String customerId = null;
        String addConcat = '';
        List<SFDCCustomerAccntContCreationService_v1.CivicAddress> civicAddList = new List<SFDCCustomerAccntContCreationService_v1.CivicAddress>();
        List<SFDCCustomerAccntContCreationService_v1.RuralRouteAddress> ruralAddList = new List<SFDCCustomerAccntContCreationService_v1.RuralRouteAddress>();
        List<SFDCCustomerAccntContCreationService_v1.PostBoxAddress> poBoxAddList = new List<SFDCCustomerAccntContCreationService_v1.PostBoxAddress>();
        List<SFDCCustomerAccntContCreationService_v1.GenDeliveryAdddress> genDelAddList = new List<SFDCCustomerAccntContCreationService_v1.GenDeliveryAdddress>();
        List<SFDCCustomerAccntContCreationService_v1.USAAddress> usAddressList = new List<SFDCCustomerAccntContCreationService_v1.USAAddress>();
        List<SFDCCustomerAccntContCreationService_v1.IntlAddress> intlAddressList = new List<SFDCCustomerAccntContCreationService_v1.IntlAddress>();
         List<SFDCCustomerAccntContCreationService_v1.MilitaryAddress> milAddressList = new List<SFDCCustomerAccntContCreationService_v1.MilitaryAddress>();
        List<SFDCCustomerAccntContCreationService_v1.custContact> custCntList = new List<SFDCCustomerAccntContCreationService_v1.custContact>();
        Boolean isAddrFound = true;
        Boolean isCustomerSuccess = true;
        String CustAccntId;
        SFDCCustomerAccntContCreationService_v1.custContResponse responseMessage;

        //Establishing a setpoint before the data is even inserted/upserted.
        Savepoint sp = database.setSavepoint();
        //Logic to insert Account of Type Customer.
        if(cusRec.CustId != '' && cusRec.CustId != null)
        {
            system.debug('----> Inside if block, the value of customer id is '+cusRec.CustId);
            customerId = 'C_'+cusRec.CustId;
        }
        else
            customerId = null;
        system.debug('----> customer id is now'+customerId);
        Account newCustAccount = new Account(SourceRefID__c = customerId);
        newCustAccount.Name = cusRec.CustName;
        if(cusRec.CustBillCycleDay  != null)
            newCustAccount.Bill_Cycle_Day__c = cusRec.CustBillCycleDay;
        if(newCustAccount.Bill_Cycle_Code__c != null)
            newCustAccount.Bill_Cycle_Code__c = cusRec.CustBillCycleCode;
        newCustAccount.Type = cusRec.CustType;
        newCustAccount.Language_Preference__c = cusRec.CustLangPref;
        newCustAccount.Phone = cusRec.CustPhone;
        newCustAccount.Description = cusRec.CustDescription;
        newCustAccount.Status__c = cusRec.CustStatus;
        newCustAccount.Type = 'Customer';
        newCustAccount.Brand__c = cusRec.CustBrand;
        if(cusRec.CustAddress != null)
        {
            civicAddList.add(cusRec.CustAddress.civicAddr);
            ruralAddList.add(cusRec.CustAddress.RRAddr);
            poBoxAddList.add(cusRec.CustAddress.POBOXAddr);
            genDelAddList.add(cusRec.CustAddress.GenDelAddr);
            usAddressList.add(cusRec.CustAddress.USAddr);
            intlAddressList.add(cusRec.CustAddress.IntlAddr);
            milAddressList.add(cusRec.CustAddress.MilitaryAddr);
        }
        custCntList = cusRec.CustContacts;
        //Each Customer Account will have a Billing Address. These Addresses can be of multiple formats like 
        //Civic Address, Rural Address, PO Address, General Address, US Address and International Address
        if(civicAddList.Size() > 0 && isAddrFound && cusRec.CustAddress.civicAddr != null)
        {
            if(civicAddList[0].careOf != null)
                addConcat += civicAddList[0].careOf +' ';
            if(civicAddList[0].CivicUnitTypeCd != null)
                addConcat += civicAddList[0].CivicUnitTypeCd + ' ';
            if(civicAddList[0].CivicUnitNum != null)
                addConcat += civicAddList[0].CivicUnitNum + ' ';
            if(civicAddList[0].CivicNumber != null)
                addConcat += civicAddList[0].CivicNumber + ' ';
            if(civicAddList[0].CivicNumberSuffix != null)
                addConcat += civicAddList[0].CivicNumberSuffix + ' ';
            if(civicAddList[0].CivicStreetName != null)
                addConcat += civicAddList[0].CivicStreetName + ' ';
            if(civicAddList[0].CivicStreetCd != null)
                addConcat +=  civicAddList[0].CivicStreetCd;
            if(civicAddList[0].CivicAdditonalInfo != null)
                addConcat += civicAddList[0].CivicAdditonalInfo + ' ';
            
            newCustAccount.BillingStreet = addConcat;
            newCustAccount.BillingCity = civicAddList[0].CivicMunicipalityName;
            newCustAccount.BillingState = civicAddList[0].CivicProvince;
            newCustAccount.BillingPostalCode = civicAddList[0].CivicPostalCd;
            newCustAccount.BillingCountry = civicAddList[0].CivicCountry;
            isAddrFound = false;
        }
        else if(poBoxAddList.Size() > 0 && isAddrFound && cusRec.CustAddress.POBOXAddr != null)
        {
            if(poBoxAddList[0].careOf != null)
                addConcat += poBoxAddList[0].careOf + ' ';
            if(poBoxAddList[0].POBoxNum != null)
                addConcat += poBoxAddList[0].POBoxNum + ' ';
            if(poBoxAddList[0].stationTypeCd != null)
                addConcat += poBoxAddList[0].stationTypeCd + ' ';
            if(poBoxAddList[0].stationAreaCd != null)
                addConcat += poBoxAddList[0].stationAreaCd+ ' ';
            if(poBoxAddList[0].stationQualifier != null)
                addConcat += poBoxAddList[0].stationQualifier + ' ';
            if(poBoxAddList[0].AdditonalInfo != null)
                addConcat += poBoxAddList[0].AdditonalInfo + ' ';
            
            newCustAccount.BillingStreet = addConcat;
            newCustAccount.BillingCity = poBoxAddList[0].MunicipalityName;
            newCustAccount.BillingState = poBoxAddList[0].Province;
            newCustAccount.BillingPostalCode = poBoxAddList[0].PostalCd;
            newCustAccount.BillingCountry = poBoxAddList[0].Country;
            isAddrFound = false;
        }
        else if(ruralAddList.Size() > 0 && isAddrFound && cusRec.CustAddress.RRAddr != null)
        {
            if(ruralAddList[0].careOf != null)
                addConcat += ruralAddList[0].careOf + ' ';
            if(ruralAddList[0].RuralRouteType != null)
                addConcat += ruralAddList[0].RuralRouteType + ' ';
            if(ruralAddList[0].RuralRouteNum != null)
                addConcat += ruralAddList[0].RuralRouteNum + ' ';
            if(ruralAddList[0].stationAreaNum != null)
                addConcat += ruralAddList[0].stationAreaNum + ' ';
            if(ruralAddList[0].stationAreaName != null)
                addConcat += ruralAddList[0].stationAreaName + ' ';
            if(ruralAddList[0].stationTypeCd != null)
                addConcat += ruralAddList[0].stationTypeCd + ' ';
            if(ruralAddList[0].stationQualifier != null)
                addConcat += ruralAddList[0].stationQualifier + ' ';
            if(ruralAddList[0].RuralAdditionalInfo != null)
                addConcat += ruralAddList[0].RuralAdditionalInfo +' ';
            
            newCustAccount.BillingStreet = addConcat;
            newCustAccount.BillingCity = ruralAddList[0].RuralMuncipalityName;
            newCustAccount.BillingState = ruralAddList[0].Province;
            newCustAccount.BillingPostalCode = ruralAddList[0].PostalCd;
            newCustAccount.BillingCountry = ruralAddList[0].Country;
            isAddrFound = false;    
        }
        else if(genDelAddList.size() > 0 && isAddrFound && cusRec.CustAddress.GenDelAddr != null)
        {
            if(genDelAddList[0].careOf != null)
                addConcat += genDelAddList[0].careOf + ' ';
            if(genDelAddList[0].stationAreaName != null)
                addConcat += genDelAddList[0].stationAreaName + ' ';
            if(genDelAddList[0].stationTypeCd != null)
                addConcat += genDelAddList[0].stationTypeCd + ' ';
            if(genDelAddList[0].stationQualifier != null)
                addConcat += genDelAddList[0].stationQualifier + ' ';
            if(genDelAddList[0].genAdditionalInfo != null)
                addConcat += genDelAddList[0].genAdditionalInfo + ' ';
            
            newCustAccount.BillingStreet = addConcat;
            newCustAccount.BillingCity = genDelAddList[0].GenDelMunicipalityName;
            newCustAccount.BillingState = genDelAddList[0].GenDelProvince;
            newCustAccount.BillingPostalCode = genDelAddList[0].GenDelPostalCd;
            newCustAccount.BillingCountry = genDelAddList[0].GenDelCountry;
            isAddrFound = false;
        }
        else if (milAddressList.size() >0 && isAddrFound && cusRec.CustAddress.MilitaryAddr != null)
        {
            if(milAddressList[0].careOf != null)
                addConcat += milAddressList[0].careOf + ' ';
            if(milAddressList[0].hmcsName != null)
                addConcat += milAddressList[0].hmcsName + ' ';
            if(milAddressList[0].fleetMailOfficeName != null)
                addConcat += milAddressList[0].fleetMailOfficeName + ' ';
            if(milAddressList[0].postOfficeBoxNumber != null)
                addConcat += milAddressList[0].postOfficeBoxNumber + ' ';
            if(milAddressList[0].stationAreaName != null)
                addConcat += milAddressList[0].stationAreaName + ' ';
            if(milAddressList[0].stationTypeCode != null)
                addConcat += milAddressList[0].stationTypeCode + ' ';
            if(milAddressList[0].stationQualifier != null)
                addConcat += milAddressList[0].stationQualifier + ' ';
            if(milAddressList[0].additionalAddressInformation != null)
                addConcat += milAddressList[0].additionalAddressInformation + ' ';

            newCustAccount.BillingStreet = addConcat;
            newCustAccount.BillingCity = milAddressList[0].municipalityName;
            newCustAccount.BillingState = milAddressList[0].provinceCode;
            newCustAccount.BillingPostalCode = milAddressList[0].postalCode;
            isAddrFound = false;
        }
        else if(usAddressList.size() >0 && isAddrFound && cusRec.CustAddress.USAddr != null)
        {
            if(usAddressList[0].careOf != null)
                addConcat += usAddressList[0].careOf + ' ';
            if(usAddressList[0].additionalAddressInformation != null)
                addConcat += usAddressList[0].additionalAddressInformation + ' ';

            newCustAccount.BillingStreet = addConcat;
            newCustAccount.BillingCity = usAddressList[0].municipalityName;
            newCustAccount.BillingState = usAddressList[0].stateCode;
            newCustAccount.BillingPostalCode = usAddressList[0].zipCode;
            newCustAccount.BillingCountry = usAddressList[0].countryCode;
            isAddrFound = false;
        }
        else if(intlAddressList.size() >0 && isAddrFound && cusRec.CustAddress.IntlAddr != null)
        {
            if(intlAddressList[0].careOf != null)
                addConcat += intlAddressList[0].careOf + ' ';
            if(intlAddressList[0].additionalAddressInformation != null)
                addConcat += intlAddressList[0].additionalAddressInformation + ' ';

            newCustAccount.BillingStreet = addConcat;
            newCustAccount.BillingState = intlAddressList[0].stateCode;
            newCustAccount.BillingCountry = intlAddressList[0].countryCode;
            isAddrFound = false;
        }

        //Upserting Customer Account
        try
        {
            upsert newCustAccount SourceRefID__c;
            system.debug('---> The value of account is '+newCustAccount +'and SourceRefID__c is '+newCustAccount.SourceRefID__c);
            isCustomerSuccess = true;
            CustAccntId = newCustAccount.Id;
            SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
            response.SfdcId = CustAccntId;
            response.respMessage = 'Upserting Account/Contact was a Success';
            response.respError = null;
            responseMessage = response;
        }
        catch(DMLException DMLex)
        {
            addConcat =  null;
            genDelAddList.clear();
            ruralAddList.clear();
            poBoxAddList.clear();
            civicAddList.clear();
            SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
            response.respMessage = 'Could not Insert/Update Customer Account';
            response.respError = DMLex.getMessage();
            response.SfdcId = null;
            isCustomerSuccess = false;
            responseMessage = response;
            Database.rollback(sp);
        }
        catch (CalloutException COutEx)
        {
            addConcat =  null;
            genDelAddList.clear();
            ruralAddList.clear();
            poBoxAddList.clear();
            civicAddList.clear();
            SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
            response.respMessage = 'Call out Failed';
            response.respError = COutEx.getMessage();
            response.SfdcId = null;
            isCustomerSuccess = false;
            responseMessage = response;
            Database.rollback(sp);
        }
        catch (customerException CustEx)
        {
            addConcat =  null;
            genDelAddList.clear();
            ruralAddList.clear();
            poBoxAddList.clear();
            civicAddList.clear();
            SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
            response.respMessage = 'Could not Insert/Update Customer Account';
            response.respError = CustEx.getMessage();
            response.SfdcId = null;
            isCustomerSuccess = false;
            responseMessage = response;
            Database.rollback(sp);
         
        }
        finally
        {
            addConcat =  null;
            genDelAddList.clear();
            ruralAddList.clear();
            poBoxAddList.clear();
            civicAddList.clear();
        }
        //If the accounts got upserted sucessfully, we check if it has any associated contacts. If it does, we upsert them too.
        if(isCustomerSuccess)
        {
            if(cusRec.CustContacts != null)
                if(!custCntList.isEmpty())
                {
                    Map<Boolean, SFDCCustomerAccntContCreationService_v1.custContResponse> responseMap = createContacts(cusRec, CustAccntId);
                    if(responseMap.containsKey(true))
                    {
                        //Means contact errored out
                        database.rollback(sp);
                        responseMessage = responseMap.get(true);
                    }
                    else
                        responseMessage = responseMap.get(false);
                }        
        }
        return responseMessage;
    }

    /*
    *   Author : Deepak Malkani.
    *   Purpose : Method used to create Customer Account Contacts when the webservice call out occurs.
    */
    @TestVisible private static Map<Boolean, SFDCCustomerAccntContCreationService_v1.custContResponse> createContacts(SFDCCustomerAccntContCreationService_v1.customerRec cusRec, String accountId)
    {
        List<Contact> contList = new List<Contact>();
        Boolean isError = false;
        Map<Boolean , SFDCCustomerAccntContCreationService_v1.custContResponse> RespFlag = new Map<Boolean, SFDCCustomerAccntContCreationService_v1.custContResponse>();
        for(Integer i=0; i< cusRec.CustContacts.Size(); i++)
        {
            if(cusRec.CustContacts[i].contLastName.toLowerCase() != 'none')
            {
            Contact con = new Contact();
            con.FirstName = cusRec.CustContacts[i].contFirstName;
            con.LastName = cusRec.CustContacts[i].contLastName;
            con.Salutation = cusRec.CustContacts[i].contSalutation;
            con.Phone = cusRec.CustContacts[i].contMobPhone;
            con.MobilePhone = cusRec.CustContacts[i].contMobPhone;
            con.OtherPhone = cusRec.CustContacts[i].contOtherPhone;
            con.LanguagePreference__c = cusRec.CustContacts[i].contLanguagePref;
            con.Email_Address__c = cusRec.CustContacts[i].contEmailAddress;
            con.Email = cusRec.CustContacts[i].contEmailAddress;
            if(cusRec.CustContacts[i].contSrcKey != null)
                if(cusRec.CustContacts[i].contSrcKey.srcSystemName != null)
                    if(cusRec.CustContacts[i].contSrcKey.srcSystemName == 'CODS')
                        con.SourceRefID__c = 'CI_'+cusRec.CustContacts[i].contSrcKey.Id;
                    else
                        con.SourceRefID__c = cusRec.CustContacts[i].contSrcKey.srcSystemName+'_'+cusRec.CustContacts[i].contSrcKey.Id;
            con.AccountRefId__c = cusRec.CustId;
            con.Role__c = cusRec.CustContacts[i].contRelnType;           
            con.Effective_Start_Date__c = cusRec.CustContacts[i].contStartDate;
            con.Effective_End_Date__c = cusRec.CustContacts[i].contExpirationDate;
            con.Contact_Association_Key__c = cusRec.CustContacts[i].contAssignmentId;
            con.Contact_Associations__c = cusRec.CustContacts[i].contAssociations;
            contList.add(con);
            }
        }
        if(!contList.isEmpty() || test.isRunningTest())
            try{
                Schema.SObjectField f = Contact.Fields.SourceRefID__c; 
                system.debug('-->Contact List is '+contList);
                database.upsert (contList,f,true);
                SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
                response.SfdcId = accountId;
                response.respMessage = 'Inserting Account/Contact was a Success';
                response.respError = null;
                RespFlag.put(false, response);
            }
            catch(DMLException DMLEx)
            {
                 contList.clear();
                 isError = true;
                 SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
                 response.SfdcId = null;
                 response.respMessage = 'Insert Failed. Could not Insert/Update Customer Contact';
                 response.respError = DMLEx.getMessage();
                 RespFlag.put(true, response);
            }
            catch(CalloutException CallOutEx)
            {
                 contList.clear();
                 isError = true;
                 SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
                 response.SfdcId = null;
                 response.respMessage = 'Call out Failed';
                 response.respError = CallOutEx.getMessage();
                 RespFlag.put(true, response);
            }
            catch(ListException LstEx)
            {
                 contList.clear();
                 isError = true;
                 SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
                 response.SfdcId = null;
                 response.respMessage = 'Insert Failed. Could not Insert/Update Customer Contact';
                 response.respError = LstEx.getMessage();  
                 RespFlag.put(true, response);
            }
            catch(TypeException TypEx)
            {
                 contList.clear();
                 isError = true;
                 SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
                 response.SfdcId = null;
                 response.respMessage = 'Insert Failed. Could not Insert/Update Customer Contact';
                 response.respError = TypEx.getMessage();
                 RespFlag.put(true, response);  
            }
            catch(customerException CustEx)
            {
                 contList.clear();
                 isError = true;
                 SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
                 response.SfdcId = null;
                 response.respMessage = 'Insert Failed. Could not Insert/Update Customer Contact';
                 response.respError = CustEx.getMessage();
                 RespFlag.put(true, response);
            }
            catch(Exception e)
            {
                 contList.clear();
                 isError = true;
                 SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
                 response.SfdcId = null;
                 response.respMessage = 'Insert Failed. Could not Insert/Update Customer Contact';
                 response.respError = e.getMessage();
                 RespFlag.put(true, response);
            }
            finally
            {
                contList.clear();
            }
        return RespFlag;    
    }
}