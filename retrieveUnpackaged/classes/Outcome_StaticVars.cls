/*
	Author : Deepak Malkani.
	Created Date : Feb 10 2015
	Purpose : This class is used to initialise Static variables so that the triggers are only executed once and does not get called out again and again
*/

public with sharing class Outcome_StaticVars {
	
	public static Boolean canRun = true;

	public static boolean canIRun(){
		return canRun;
	}

	public static void stopTrigger(){
		canRun = false;
	}
}