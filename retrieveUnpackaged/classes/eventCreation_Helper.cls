/*
*	Author : Deepak Malkani.
*	Created Date : 11/06/2015
*	Purpose : Helper class to insert Events comming from various source systems.
*/
public with sharing class eventCreation_Helper {
	
	public static List<SFDCEventPublisherService_v1.EventResponse> createEvent(List<SFDCEventPublisherService_v1.EventRecord> eventRecords){
		
		List<EventManager__c> evtMagList = new List<EventManager__c>();
		List<SFDCEventPublisherService_v1.EventResponse> responseLst = new List<SFDCEventPublisherService_v1.EventResponse>();
		SFDCEventPublisherService_v1.EventResponse response = new SFDCEventPublisherService_v1.EventResponse();

		for(Integer i=0; i<eventRecords.Size(); i++)
		{
			EventManager__c evtMageObj = new EventManager__c();
			evtMageObj.SourceName__c = eventRecords[i].sourceName;
			evtMageObj.SourceEventType__c = eventRecords[i].sourceEventType;
			evtMageObj.SourceObjectType__c = eventRecords[i].sourceObjectType;
			evtMageObj.EventTriggeredBy__c = eventRecords[i].eventTriggeredBy;
			evtMageObj.EventSrcSystemTimeStamp__c = eventRecords[i].eventSrcSystemTimeStamp;
			evtMageObj.SourceRefID__c = eventRecords[i].sourceRefID;
			evtMageObj.CustomerId__c = eventRecords[i].customerID;
			evtMageObj.BillingAccountNo__c = eventRecords[i].billingAccountNo;
			evtMageObj.BANSrcSystem__c = eventRecords[i].banSrcSystem;
			evtMageObj.AssetID__c = eventRecords[i].assetID;
			evtMageObj.AssetSrcSystem__c = eventRecords[i].assetSrcSystem;
			evtMageObj.ContactID__c = eventRecords[i].contactID;
			evtMageObj.ContactSrcSystem__c = eventRecords[i].contactSrcSystem;
			evtMageObj.EventCentralOfficeID__c = eventRecords[i].eventCentralOfficeID;
			evtMageObj.EventDealerCode__c = eventRecords[i].eventDealerCode;
			evtMageObj.EventPostalCode__c = eventRecords[i].eventPostalCode;
			evtMageObj.SourceEventJSONMessage__c = eventRecords[i].sourceEventJSONMessage;
			evtMagList.add(evtMageObj);
		}
		if(!evtMagList.isEmpty())
			try{
				//Schema.SObjectField f = EventManager__c.Fields.SourceRefID__c;
				database.insert (evtMagList,true);
				for(Integer j=0; j< evtMagList.size(); j++){
					response.responseMessage = 'Event upserted sucessfully';
					response.responseError = null;
					response.sfdcId = evtMagList[j].Id;
					responseLst.add(response);
				}
			}
			catch(DMLException dmlEx)
			{
				response.responseMessage = dmlEx.getMessage();
				response.responseError = dmlEx.getStackTraceString();
				response.sfdcId = null;
				responseLst.add(response);
			}
			catch(CalloutException callEx)
			{
				response.responseMessage = callEx.getMessage();
				response.responseError = callEx.getStackTraceString();
				response.sfdcId = null;
				responseLst.add(response);
			}
			catch(Exception e)
			{
				response.responseMessage = e.getMessage();
				response.responseError = e.getStackTraceString();
				response.sfdcId = null;
				responseLst.add(response);
			}
			finally
			{
				evtMagList.clear();
			}
		return responseLst;
	}
}