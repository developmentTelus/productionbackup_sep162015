/*
	Author : Deepak Malkani.
	Created Date : June 8 2015
	Purpose : Common Handler class to initiliase all Custom Settings across the org
*/
public with sharing class CustomSettingHandler {
	public CustomSettingHandler() {}

	public Boolean getAPILoads(){
		API_Loads__c CSAPI = API_Loads__c.getInstance(UserInfo.getUserId());
		return CSAPI.Invoke_Contact_Trigger__c;
	}

	public Boolean getETLLoadCS(){
		ETLLoads__c CS = ETLLoads__c.getInstance(UserInfo.getUserId());
		return CS.runDataLoads__c;
	}
}