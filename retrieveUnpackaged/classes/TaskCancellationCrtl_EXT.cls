/*
    Author : Praveen Kumar Bonalu.
    Created Date : Feb 27 2015
    Purpose : Purpose of this Controller Extension is to process all Business Logic related to TaskCancellation VF Page.
*/

public class TaskCancellationCrtl_EXT {
    public Task t;
      
    public TaskCancellationCrtl_EXT(ApexPages.StandardController controller) {
        this.t= (Task)controller.getRecord();      
    }
    
    public pagereference Save()
    {      
        try{
            Task tkObj = new Task(Id = t.Id);
            tkObj.Status='Cancelled';
            tkObj.Reason_Code__c = t.Reason_Code__c;
            update tkObj;
            
        }
        
        Catch(DMLException e){ 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Task_Cancel_Reason));
            return null;
        }
        PageReference pg = new PageReference('/'+t.Id);
        pg.setRedirect(true);
        return pg;       
    }  
    
}