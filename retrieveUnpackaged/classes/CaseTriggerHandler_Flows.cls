/*
	Author : Deepak Malkani.
	Created Date : April 14 2015
	Purpose : This handler class is used to call all flows which perform all Trigger sepecific validations and DML operations
*/
/*
*	Author : Deepak Malkani.
*	Modified Date : May 11 2015
*	Purpose : Added mthod for RTS Renewal Task creation - JIRA # CES20-299
*/
 
public with sharing class CaseTriggerHandler_Flows {
	public CaseTriggerHandler_Flows() {
		
	}

	/*
		Author : Deepak Malkani.
		Purpose : Calls the RPT Renewal Task creation flow, when entry criteria is met.
	*/
	public void launchRPT1RenewalsFlow(Set<ID> caseIds){
		
		//Initialise and set all collections
		List<Case> caseList = [SELECT id, CaseNumber, ContactId, OwnerId, Status, SourceRefId__c, Source_Type__c, Close_Review__c, Close_Review_Due_Date__c, Outcome_Review__c, Outcome_Review_Due_Date__c
								FROM Case
								WHERE id IN : caseIds];
		Map<String, Object> params = new Map<String, Object>();
		List<Task> tskCollList = new List<Task>();
		params.put('sObjCaseColln', caseList);
		Flow.Interview.RPT1_Renewal_Task_Creation_Flow RPTTaskFlow = new Flow.Interview.RPT1_Renewal_Task_Creation_Flow(params);
        RPTTaskFlow.start();
        //clearing off all the lists in the end
        params.clear();
        //tskCollList.clear(); 
	}
	
	
	// creates Tasks for RTS Cases when the agent does a get case on RTS specific Case
	//Deepak Malkani : Commented this code
	public void createRTSTasks(Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap)
	{
		System.debug('starting createRTSTasks');
		
		List<Case> caseList = new List<Case>();
		for(Case newCase : newCaseMap.values())
		{
			if (!isRTS(newCase)) continue;
			if (!Helper.isUser(newCase.OwnerID)) continue;

			Case oldCase = oldCaseMap.get(newCase.Id);

			// case moved from queue to user -- Make sure the task is created only when agents do a get case which happens only when ownership changes from a queue to a user
			Boolean movedFromQueueToUser = 
					Helper.isQueue(oldCase.OwnerId) &&
					Helper.isUser(newCase.OwnerId);

			// status changed -- If the owner is already an Agent and the source status actually changed to Ready for Pickup		
			Boolean sourceStatusChanged = 
					oldCase.SourceStatus__c !=
					newCase.SourceStatus__c;

			if (movedFromQueueToUser || sourceStatusChanged) {
				caseList.add(newCase);
			}
		}

		
		if(!caseList.isEmpty()) {

			List<Task> tasks = new List<Task>();
	
			for (Case cse : caseList) {
				
				Map<String, Object> result = FlowHandler.launch_RTS_Task_Fields(cse); 
	
				System.debug('----- ----- result:' + result);
	
				if (result.get('taskSubject')==null) continue;
	
				Task task = new Task();
				
				task.Description = (String) result.get('taskDescription');
				task.ActivityDate = Date.today().addDays(Integer.valueOf((Decimal) result.get('taskDueDateDelta')));
				task.Subject = (String) result.get('taskSubject');
	
				task.WhatId = cse.Id;
				task.OwnerId = cse.OwnerId;
				task.Priority = 'Medium';
	
				if (!String.isEmpty(cse.ContactId)) {
					task.WhoId = cse.Contact.Id;
				}
	
				tasks.add(task);
			}

			insert tasks;			
		}
	}

	// HH May 7, 2015: class FlowHanlder created for this type of operations. the two flows used below are not used anymore
/*	public void launchRTSTaskFlow(List<Case> csList)
	{
		Map<String, Object> params = new Map<String, Object>();
		List<Task> tskCollList = new List<Task>();
		params.put('sObjCaseColl', csList);
		Flow.Interview.Create_RTS_Tasks RTSTaskFlow = new Flow.Interview.Create_RTS_Tasks(params);
        RTSTaskFlow.start();
        //check outputs
        tskCollList= (List<Task>) RTSTaskFlow.getVariableValue('sObjTaskColl');
        system.debug('---> Task inserted are  '+tskCollList); 
	}
	
	public void launchRTSTaskSourceChangeFlow(List<Case> csList)
	{
        //Haydar Hadi : Modify the flow - Create RTS Task for Source Status Changes.
		Map<String, Object> params = new Map<String, Object>();
		List<Task> tskCollList = new List<Task>();
		params.put('sObjCaseColln', csList);
		Flow.Interview.Create_RTS_Task_on_Source_Status_Change RTSTaskStatusChangeFlow = new Flow.Interview.Create_RTS_Task_on_Source_Status_Change(params);
        RTSTaskStatusChangeFlow.start();
        //check outputs
        tskCollList= (List<Task>) RTSTaskStatusChangeFlow.getVariableValue('sObjTaskColln');
        system.debug('---> Task inserted are  '+tskCollList);
        //clearing off all the lists in the end
        params.clear();
        tskCollList.clear(); 
	}*/


	//////////////////////////////////////////////////////
	/////////////// HELPERS //////////////////////////////
	//////////////////////////////////////////////////////

    public static Boolean isRTS(Case cse) {
    	if (cse==null) return false;
    	String source = cse.SourceRefID__c;
    	if (source==null) return false;
    	return source.substring(0,3) == 'RTS';
	}
	
	private static Date toDate(DateTime dt) {
		return Date.newInstance(dt.year(), dt.month(),dt.day());
	}	
}