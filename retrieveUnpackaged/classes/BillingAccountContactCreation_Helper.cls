/*
*	Author : Deepak Malkani.
*	Created Date : July 8th 2015
*	Purpose : Helper class for SFDCBillingAccountContact creation Web service
*
*	Author : Deepak Malkani.
*	Modified Date : July 19 2015
*	Purpose : Helper class updated to ensure address specific attributes are correctly mapped.
*/
/*
	Author : Aneeq Hashmi
	Modified Date : Aug 2 2015
	Purpose : Added exception handling to Customer Account lookup, added Billing_Account_Number__c = BARecords[i].BASrcSystemKey.Id;
	Bug Fixed : Incorrect nested if-else was causing failure of inserting Billing Accounts
<<<<<<< HEAD
	
	Modified Date : 6th Aug 2014
	Purpose : Fix srcSystemName=130 and replace with KB, and handle situation where Last Name = NULL, Insert with NONE
=======
>>>>>>> branch 'master' of https://bitbucket.org/developmentTelus/telus_release4.0/branch/master
*/

public with sharing class BillingAccountContactCreation_Helper {
	

	public class BillingException extends Exception{}
	/*
	*	Author : Deepak Malkani.
	*	Purpose : Method used to create Billing Accounts for a Customer Account and parent it to the correct Customer Account
	*/
	@TestVisible public static SFDCBillingAccntContCreationService_v1.custContResponse createBillingAccnt(List<SFDCBillingAccntContCreationService_v1.billingAccountRec> BARecords){
		
		//Initialise all variables and collections
		Boolean isAccountSuccess = true;
		Boolean isCustFound = true;
		Account custRec;
		String BillAccntId;
		List<SFDCBillingAccntContCreationService_v1.Address> addList;
		Set<String> accntExternaId = new Set<String>();
		SFDCBillingAccntContCreationService_v1.custContResponse responseMessage = new SFDCBillingAccntContCreationService_v1.custContResponse();
		List<Account> billAccntList = new List<Account>();
		List<Contact> billContList = new List<Contact>();
		Account billingAccount;
		Boolean isError = false;
		
		//Set the Database setpoint, to revert back to incase the transaction needs to be rolled back.
		Savepoint sp = database.setSavepoint();
		//first we check if the Customer Account event exists or not
		for(Integer j=0; j < BARecords.size(); j++)
			accntExternaId.add('C_'+BARecords[j].CustomerId);
		//Search if the Customer Account actually exists or not.
		Map<String, Account> customerAccntMap = new Map<String, Account>();
		for(Account acc : [SELECT id, SourceRefID__c, Name FROM Account WHERE SourceRefID__c IN : accntExternaId AND Type = 'Customer'])
			//customerAccntMap.put(acc.SourceRefID__c, acc);
			//Aneeq Modified
			try{
            customerAccntMap.put(acc.SourceRefID__c, acc);
            }
        catch(System.DmlException exp1){
            if(String.isEmpty(acc.SourceRefID__c)){
            //You need to reject the entire transaction.
			SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
			response.respMessage = 'Cannot Process Billing Accounts as Customer Account is not found.';
			response.respError = 'Cannot Process Billing Accounts as Customer Account is not found.';
			response.respErrorCause = 'Cannot Process Billing Accounts as Customer Account is not found.';
			response.SfdcId = null;
			responseMessage = response;
			isCustFound = false;
            }
            else{
             	database.rollBack(sp);
				isAccountSuccess = false;
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = exp1.getMessage();
				response.respError = exp1.getStackTraceString();
				response.respErrorCause = exp1.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;   
            }
		}

		//What is one of the Billing Accounts dont have a Customer Account Id?
		//In this case, we reject everything. The entire Data is rejected.
		if(customerAccntMap.isEmpty() || customerAccntMap.size() != BARecords.size())
		{
			//You need to reject the entire transaction.
			SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
			response.respMessage = 'Cannot Process Billing Accounts as Customer Account is not found.';
			response.respError = 'Cannot Process Billing Accounts as Customer Account is not found.';
			response.respErrorCause = 'Cannot Process Billing Accounts as Customer Account is not found.';
			response.SfdcId = null;
			responseMessage = response;
			isCustFound = false;
		}
		//If the Customer Account is found and there is data comming from Webservice call out, continue with the code block below.
		if(!BARecords.isEmpty() && isCustFound)
		{
			for(Integer i=0; i < BARecords.size(); i++)
			{
				if(customerAccntMap.containsKey('C_'+BARecords[i].CustomerId))
				{
					billingAccount = new Account();
					billingAccount.Name = BARecords[i].BAName;
					billingAccount.Bill_Cycle_Code__c = BARecords[i].BABillCycleCode;
					billingAccount.Bill_Cycle_Day__c = BARecords[i].BABillCycleDay;
					billingAccount.Language_Preference__c = BARecords[i].BALangPref;
					billingAccount.Phone = BARecords[i].BAPhone;
	            	billingAccount.Status__c = BARecords[i].BAStatus;
	            	billingAccount.Brand__c = BARecords[i].BABrand;
	            	if(BARecords[i].BASrcSystemKey != null)
	            		if(BARecords[i].BASrcSystemKey.srcSystemName.toLowerCase() == 'kb')
							billingAccount.Type = 'Mobility';
						else if (BARecords[i].BASrcSystemKey.srcSystemName.toLowerCase() == 'enabler')
							billingAccount.Type = 'FFH';
					billingAccount.Description = BARecords[i].BADescription;
					if(BARecords[i].BASrcSystemKey != null)
						if(BARecords[i].BASrcSystemKey.Id != null && BARecords[i].BASrcSystemKey.Id != '')
							if(BARecords[i].BASrcSystemKey.srcSystemName != null)
								if(BARecords[i].BASrcSystemKey.srcSystemName.toLowerCase() == 'kb'){
									billingAccount.SourceRefID__c = 'KB_'+BARecords[i].BASrcSystemKey.Id;
									billingAccount.Billing_Account_Number__c = BARecords[i].BASrcSystemKey.Id;
								}
								else if(BARecords[i].BASrcSystemKey.srcSystemName.toLowerCase() == 'enabler'){
									billingAccount.SourceRefID__c = 'Enabler_'+BARecords[i].BASrcSystemKey.Id;
									billingAccount.Billing_Account_Number__c = BARecords[i].BASrcSystemKey.Id;
								}
								
					//Parenting the Billing Account to the correct Customer Account
					Account accntRef = new Account (SourceRefID__c = 'C_'+BARecords[i].CustomerId);
					billingAccount.Parent = accntRef;
					addAddresstoAccnt(BARecords[i], billingAccount);
					billAccntList.add(billingAccount);
					system.debug('--->Billing Account record is '+BARecords[i]);
					system.debug('--->Contact record for the Billing Account is '+BARecords[i].BAContacts);
					if(BARecords[i].BAContacts != null)
					{
						List<SFDCBillingAccntContCreationService_v1.BAContact> BillingContacts;
						BillingContacts = BARecords[i].BAContacts;
						for(Integer p=0; p< BARecords[i].BAContacts.size() ; p++)
						{
							
							Contact BillingConRec = new Contact();
							BillingConRec.FirstName = BillingContacts[p].contFirstName;
            				if(String.isEmpty(BillingContacts[p].contLastName)){
            				BillingConRec.LastName = 'NONE';	
            				}
            				else{
            				BillingConRec.LastName = BillingContacts[p].contLastName;
            				}
            				BillingConRec.Salutation = BillingContacts[p].contSalutation;
            				BillingConRec.Phone = BillingContacts[p].contMobPhone;
            				BillingConRec.MobilePhone = BillingContacts[p].contMobPhone;
            				BillingConRec.OtherPhone = BillingContacts[p].contOtherPhone;
            				BillingConRec.LanguagePreference__c = BillingContacts[p].contLanguagePref;
            				BillingConRec.Email_Address__c = BillingContacts[p].contEmailAddress;
            				BillingConRec.Email = BillingContacts[p].contEmailAddress;
            				if(BillingContacts[p].contSrcKey != null)
            					if(BillingContacts[p].contSrcKey.srcSystemName != null)
            						if(BillingContacts[p].contSrcKey.srcSystemName == 'CODS')
                						BillingConRec.SourceRefID__c = 'CI_'+BillingContacts[p].contSrcKey.Id;
                						else if(BillingContacts[p].contSrcKey.srcSystemName == '130')
                							BillingConRec.SourceRefID__c = 'KB_'+BillingContacts[p].contSrcKey.Id;
            								else
                								BillingConRec.SourceRefID__c = BillingContacts[p].contSrcKey.srcSystemName+'_'+BillingContacts[p].contSrcKey.Id;
            				BillingConRec.AccountRefId__c = BARecords[i].CustomerId;
            				BillingConRec.Role__c = BillingContacts[p].contRelnType;
            				billContList.add(BillingConRec);							
						}
					}
				}
			}
		}
		//Inserting all Billing Accounts
		if(!billAccntList.isEmpty() && isCustFound)
		{	
			Schema.SObjectField f = Account.Fields.SourceRefID__c;	
			try{
				database.upsert (billAccntList,f,true);
			}
			catch(DmlException dmlEx){
				database.rollBack(sp);
				isAccountSuccess = false;
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = dmlEx.getMessage();
				response.respError = dmlEx.getStackTraceString();
				response.respErrorCause = dmlEx.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;
			}
			catch(CalloutException callOutEx){
				database.rollback(sp);
				isAccountSuccess = false;
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = callOutEx.getMessage();
				response.respError = callOutEx.getStackTraceString();
				response.respErrorCause = callOutEx.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;	
			}
			catch(BillingException custEx){
				database.rollback(sp);
				isAccountSuccess = false;
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = custEx.getMessage();
				response.respError = custEx.getStackTraceString();
				response.respErrorCause = custEx.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;
			}
			catch(Exception e){
				database.rollback(sp);
				isAccountSuccess = false;
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = e.getMessage();
				response.respError = e.getStackTraceString();
				response.respErrorCause = e.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;
			}
			if(!isError && billContList.isEmpty())
			{
				isAccountSuccess = true;
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = 'Billing Account successfully upserted';
				response.respError = null;
				response.respErrorCause = null;
				response.SfdcId = billingAccount.Id;
				responseMessage = response;
				isError = false;
			}
		}
		//Inserting all Billing Contacts, but they will be referenced at Customer Level - SFDC Datamodel
		if(!billContList.isEmpty() && isAccountSuccess && isCustFound)
		{
			system.debug('---> Billing contact List is '+billContList);
			try{
				Schema.SObjectField f = Contact.Fields.SourceRefID__c;	
				database.upsert (billContList,f,true);
			}
			catch(DmlException dmlEx)
			{
				Database.Rollback(sp);
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = dmlEx.getMessage();
				response.respError = dmlEx.getStackTraceString();
				response.respErrorCause = dmlEx.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;
			}
			catch(CalloutException callOutEx)
			{
				Database.Rollback(sp);
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = callOutEx.getMessage();
				response.respError = callOutEx.getStackTraceString();
				response.respErrorCause = callOutEx.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;
			}
			catch(BillingException billEx)
			{
				Database.Rollback(sp);
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = billEx.getMessage();
				response.respError = billEx.getStackTraceString();
				response.respErrorCause = billEx.getTypeName();
				response.SfdcId = null;
				responseMessage = response;
				isError = true;
			}
			if(!isError)
			{
				SFDCBillingAccntContCreationService_v1.custContResponse response = new SFDCBillingAccntContCreationService_v1.custContResponse();
				response.respMessage = 'Billing Account and Contact successfully upserted';
				response.respError = null;
				response.respErrorCause = null;
				response.SfdcId = billingAccount.Id;
				responseMessage = response;
				isError = false;
			}
		}
		system.debug('--->response message is '+responseMessage);
		return responseMessage;
	}
	
	/*
	*	Purpose : Method created to add Addresses to the Billing Account, based on what comes from the webservice call out.
	*/
	@TestVisible private static void addAddresstoAccnt(SFDCBillingAccntContCreationService_v1.billingAccountRec BARec, Account BillAcntRec)
	{
		List<SFDCBillingAccntContCreationService_v1.CivicAddress> civicAddressList = new List<SFDCBillingAccntContCreationService_v1.CivicAddress>();
		List<SFDCBillingAccntContCreationService_v1.PostBoxAddress> poBoxAddressList = new List<SFDCBillingAccntContCreationService_v1.PostBoxAddress>();
		List<SFDCBillingAccntContCreationService_v1.RuralRouteAddress> ruralAddressList = new List<SFDCBillingAccntContCreationService_v1.RuralRouteAddress>();
		List<SFDCBillingAccntContCreationService_v1.GenDeliveryAdddress> genDelAddressList = new List<SFDCBillingAccntContCreationService_v1.GenDeliveryAdddress>();
		List<SFDCBillingAccntContCreationService_v1.MilitaryAddress> milAddressList = new List<SFDCBillingAccntContCreationService_v1.MilitaryAddress>();
		List<SFDCBillingAccntContCreationService_v1.USAAddress> usAddressList = new List<SFDCBillingAccntContCreationService_v1.USAAddress>();
		List<SFDCBillingAccntContCreationService_v1.IntlAddress> intlAddressList = new List<SFDCBillingAccntContCreationService_v1.IntlAddress>();
		Boolean isAddrFound = true;
		String addConcat = '';

		if(BARec.BAAddress != null)
		{
			if(BARec.BAAddress.civicAddr != null)
				civicAddressList.add(BARec.BAAddress.civicAddr);
			if(BARec.BAAddress.POBOXAddr != null)
				poBoxAddressList.add(BARec.BAAddress.POBOXAddr);
			if(BARec.BAAddress.RRAddr != null)
				ruralAddressList.add(BARec.BAAddress.RRAddr);
			if(BARec.BAAddress.GenDelAddr != null)
				genDelAddressList.add(BARec.BAAddress.GenDelAddr);
			if(BARec.BAAddress.MilitaryAddr != null)
				milAddressList.add(BARec.BAAddress.MilitaryAddr);
			if(BARec.BAAddress.USAddr != null)
				usAddressList.add(BARec.BAAddress.USAddr);
			if(BARec.BAAddress.IntlAddr != null)
				intlAddressList.add(BARec.BAAddress.IntlAddr);
		}
		if(civicAddressList.Size() > 0 && isAddrFound)
		{
			if(civicAddressList[0].careOf != null)
				addConcat += civicAddressList[0].careOf +' ';
			if(civicAddressList[0].CivicUnitTypeCd != null)
				addConcat += civicAddressList[0].CivicUnitTypeCd + ' ';
			if(civicAddressList[0].CivicUnitNum != null)
				addConcat += civicAddressList[0].CivicUnitNum + ' ';
            if(civicAddressList[0].CivicNumber != null)
                addConcat += civicAddressList[0].CivicNumber + ' ';
            if(civicAddressList[0].CivicNumberSuffix != null)
                addConcat += civicAddressList[0].CivicNumberSuffix + ' ';
            if(civicAddressList[0].CivicAdditonalInfo != null)
                addConcat += civicAddressList[0].CivicAdditonalInfo + ' ';
            if(civicAddressList[0].CivicStreetName != null)
                addConcat += civicAddressList[0].CivicStreetName + ' ';
            if(civicAddressList[0].CivicStreetCd != null)
                addConcat +=  civicAddressList[0].CivicStreetCd;
            
            BillAcntRec.BillingStreet = addConcat;
            BillAcntRec.BillingCity = civicAddressList[0].CivicMunicipalityName;
            BillAcntRec.BillingState = civicAddressList[0].CivicProvince;
            BillAcntRec.BillingPostalCode = civicAddressList[0].CivicPostalCd;
            BillAcntRec.BillingCountry = civicAddressList[0].CivicCountry;
            isAddrFound = false;
		}
		else if(poBoxAddressList.size() > 0 && isAddrFound)
		{
			if(poBoxAddressList[0].careOf != null)
				addConcat += poBoxAddressList[0].careOf + ' ';
			if(poBoxAddressList[0].POBoxNum != null)
                addConcat += poBoxAddressList[0].POBoxNum + ' ';
            if(poBoxAddressList[0].stationTypeCd != null)
            	addConcat += poBoxAddressList[0].stationTypeCd + ' ';
            if(poBoxAddressList[0].stationAreaCd != null)
            	addConcat += poBoxAddressList[0].stationAreaCd + ' ';
            if(poBoxAddressList[0].stationQualifier != null)
            	addConcat += poBoxAddressList[0].stationQualifier + ' ';
            if(poBoxAddressList[0].AdditonalInfo != null)
            	addConcat += poBoxAddressList[0].AdditonalInfo;
            
            BillAcntRec.BillingStreet = addConcat;
            BillAcntRec.BillingCity = poBoxAddressList[0].MunicipalityName;
            BillAcntRec.BillingState = poBoxAddressList[0].Province;
            BillAcntRec.BillingPostalCode = poBoxAddressList[0].PostalCd;
            BillAcntRec.BillingCountry = poBoxAddressList[0].Country;
            isAddrFound = false;
		}
		else if(ruralAddressList.size() > 0 && isAddrFound)
		{
			if(ruralAddressList[0].careOf != null)
				addConcat += ruralAddressList[0].careOf + ' ';
			if(ruralAddressList[0].RuralRouteType != null)
				addConcat += ruralAddressList[0].RuralRouteType + ' ';
			if(ruralAddressList[0].RuralRouteNum != null)
				addConcat += ruralAddressList[0].RuralRouteNum + ' ';
			if(ruralAddressList[0].stationTypeCd != null)
				addConcat += ruralAddressList[0].stationTypeCd + ' ';
			if(ruralAddressList[0].stationAreaNum != null)
				addConcat += ruralAddressList[0].stationAreaNum + ' ';
			if(ruralAddressList[0].stationAreaName != null)
				addConcat += ruralAddressList[0].stationAreaName + ' ';
			if(ruralAddressList[0].stationQualifier != null)
				addConcat += ruralAddressList[0].stationQualifier + ' ';
			if(ruralAddressList[0].RuralAdditionalInfo != null)
                addConcat += ruralAddressList[0].RuralAdditionalInfo +' ';
            
            BillAcntRec.BillingStreet = addConcat;
            BillAcntRec.BillingCity = ruralAddressList[0].RuralMuncipalityName;
            BillAcntRec.BillingState = ruralAddressList[0].Province;
            BillAcntRec.BillingPostalCode = ruralAddressList[0].PostalCd;
            BillAcntRec.BillingCountry = ruralAddressList[0].Country;
            isAddrFound = false;    
		}
		else if(genDelAddressList.size() > 0 && isAddrFound)
		{
			if(genDelAddressList[0].careOf != null)
				addConcat += genDelAddressList[0].careOf + ' ';
			if(genDelAddressList[0].stationTypeCd != null)
				addConcat += genDelAddressList[0].stationTypeCd + ' ';
			if(genDelAddressList[0].stationAreaName != null)
				addConcat += genDelAddressList[0].stationAreaName + ' ';
			if(genDelAddressList[0].stationQualifier != null)
				addConcat += genDelAddressList[0].stationQualifier + ' ';
			if(genDelAddressList[0].genAdditionalInfo != null)
				addConcat += genDelAddressList[0].genAdditionalInfo + ' ';
			
            BillAcntRec.BillingStreet = addConcat;
            BillAcntRec.BillingCity = genDelAddressList[0].GenDelMunicipalityName;
            BillAcntRec.BillingState = genDelAddressList[0].GenDelProvince;
            BillAcntRec.BillingPostalCode = genDelAddressList[0].GenDelPostalCd;
            BillAcntRec.BillingCountry = genDelAddressList[0].GenDelCountry;
            isAddrFound = false;
		}
		else if(milAddressList.size() > 0 && isAddrFound)
		{
			if(milAddressList[0].careOf != null)
				addConcat += milAddressList[0].careOf + ' ';
			if(milAddressList[0].hmcsName != null)
				addConcat += milAddressList[0].hmcsName + ' ';
			if(milAddressList[0].fleetMailOfficeName != null)
				addConcat += milAddressList[0].fleetMailOfficeName + ' ';
			if(milAddressList[0].postOfficeBoxNumber != null)
				addConcat += milAddressList[0].postOfficeBoxNumber + ' ';
			if(milAddressList[0].stationTypeCode != null)
				addConcat += milAddressList[0].stationTypeCode + ' ';
			if(milAddressList[0].stationAreaName != null)
				addConcat += milAddressList[0].stationAreaName + ' ';
			if(milAddressList[0].stationQualifier != null)
				addConcat += milAddressList[0].stationQualifier + ' ';
			if(milAddressList[0].additionalAddressInformation != null)
				addConcat += milAddressList[0].additionalAddressInformation + ' ';

			BillAcntRec.BillingStreet = addConcat;
			BillAcntRec.BillingCity = milAddressList[0].municipalityName;
			BillAcntRec.BillingState = milAddressList[0].provinceCode;
			BillAcntRec.BillingPostalCode = milAddressList[0].postalCode;
			isAddrFound = false;
		}
		else if(usAddressList.size() > 0 && isAddrFound)
		{
			if(usAddressList[0].careOf != null)
				addConcat += usAddressList[0].careOf + ' ';
			if(usAddressList[0].additionalAddressInformation != null)
				addConcat += usAddressList[0].additionalAddressInformation + ' ';

			BillAcntRec.BillingStreet = addConcat;
			BillAcntRec.BillingCity = usAddressList[0].municipalityName;
			BillAcntRec.BillingState = usAddressList[0].stateCode;
			BillAcntRec.BillingPostalCode = usAddressList[0].zipCode;
			BillAcntRec.BillingCountry = usAddressList[0].countryCode;
			isAddrFound = false;
		}
		else if(intlAddressList.size() > 0 && isAddrFound)
		{
			if(intlAddressList[0].careOf != null)
				addConcat += intlAddressList[0].careOf + ' ';
			if(intlAddressList[0].additionalAddressInformation != null)
				addConcat += intlAddressList[0].additionalAddressInformation + ' ';

			BillAcntRec.BillingStreet = addConcat;
			BillAcntRec.BillingState = intlAddressList[0].stateCode;
			BillAcntRec.BillingCountry = intlAddressList[0].countryCode;
			isAddrFound = false;
		}
		//clear off all the lists
		civicAddressList.clear();
		poBoxAddressList.clear();
		ruralAddressList.clear();
		genDelAddressList.clear();
		milAddressList.clear();
		usAddressList.clear();
		intlAddressList.clear();
	}

}