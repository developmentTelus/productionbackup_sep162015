/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class AccountExternalKeyUpdate_Batch_Test {

    static testMethod void UnitTest_1_massupdSrcRefId_MobAccnt() {
        
		List<Account> accntList = new List<Account>();
        UtlityTest testHandler = new UtlityTest();
        accntList = testHandler.createMultiMobAccntRec(200);
        Test.startTest();
        AccountExternalKeyUpdate_Batch batch = new AccountExternalKeyUpdate_Batch();
        database.executeBatch(batch);
        Test.stopTest();
        //Making assertions
        for(Account a : [SELECT id, Billing_Account_Number__c, SourceRefId_Backup__c, SourceRefId__c, Type FROM Account])
        	system.assertEquals('KB_'+a.Billing_Account_Number__c, a.SourceRefId__c);
        accntList.clear();
        accntList = [SELECT SourceRefId_Backup__c FROM Account WHERE SourceRefId_Backup__c != null];
        system.assertEquals(200, accntList.size());
        accntList.clear();
    }
    
    static testMethod void UnitTest_2_massupdSrcRefId_FFHAccnt() {
   
		List<Account> accntList = new List<Account>();
        UtlityTest testHandler = new UtlityTest();
        accntList = testHandler.createMultiWiredAccntRec(200);
        Test.startTest();
        AccountExternalKeyUpdate_Batch batch = new AccountExternalKeyUpdate_Batch();
        database.executeBatch(batch);
        Test.stopTest();
        //Making assertions
        for(Account a : [SELECT id, Billing_Account_Number__c, SourceRefId_Backup__c, SourceRefId__c, Type FROM Account])
        	system.assertEquals('Enabler_'+a.Billing_Account_Number__c, a.SourceRefId__c);
        accntList.clear();
        accntList = [SELECT SourceRefId_Backup__c FROM Account WHERE SourceRefId_Backup__c != null];
        system.assertEquals(200, accntList.size());
        accntList.clear();
    }
}