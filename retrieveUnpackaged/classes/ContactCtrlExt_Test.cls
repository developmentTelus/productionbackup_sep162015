/*
	Author : Deepak Malkani.
	Created Date : April 13 2015
	Purpose : Test class for Visual Force Page Controller Extension on Contact
*/

@isTest(SeeAllData=false)
public class ContactCtrlExt_Test
{
	public static testMethod void UnitTest1()
	{
		//Initialise all Handler Classes
		UtlityTest testHandler = new UtlityTest();
		Contact con = testHandler.createContactRec();
		Boolean flg;
		String contactId;
		//Setting Page Reference
		PageReference pg = Page.ContactInlinePage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(con);
		//Initalise the Controller Extension Class
		ContactCtrlExt contactContExt = new ContactCtrlExt(stdCtrl);
		//Asserting the controller call
		system.assertEquals(con.Id, contactContExt.cntct.Id);
		//Calling all other controller methods
		system.assertEquals(true, contactContExt.getContactFirstTime());
		flg = contactContExt.getContactFirstTime();
		ContactId = (String) con.Id;
		//call the remote function
		ContactCtrlExt.updFirstTime(ContactId, flg);
	}

	public static testMethod void UnitTest2()
	{
		//Initialise all Handler Classes
		UtlityTest testHandler = new UtlityTest();
		Contact con = testHandler.createContactRec();
		con.First_Time_Contact__c = false;
		update con;
		Boolean flg;
		String contactId;
		//Setting Page Reference
		PageReference pg = Page.ContactInlinePage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(con);
		//Initalise the Controller Extension Class
		ContactCtrlExt contactContExt = new ContactCtrlExt(stdCtrl);
		//Asserting the controller call
		system.assertEquals(con.Id, contactContExt.cntct.Id);
		//Calling all other controller methods
		system.assertEquals(false, contactContExt.getContactFirstTime());
		flg = contactContExt.getContactFirstTime();
		ContactId = (String) con.Id;
		//call the remote function
		ContactCtrlExt.updFirstTime(ContactId, flg);
	}
}