/*
    Author : Deepak Malkani.
    Created Date : April 1 2015
    Purpose : A common Contact Controller extension, which will be referenced eventually by VF Pages. As of now its been used by VF Page which 
              fires custom CTI Events on Contact Creation.
*/
global with sharing class ContactCtrlExt {

    public Contact cntct {get; set;}
    
    public ContactCtrlExt(ApexPages.StandardController controller) {
        cntct = (Contact) controller.getRecord();
    }
    
    public Boolean getContactFirstTime()
    {    
        if(cntct.Id != null)
            return [SELECT id, First_Time_Contact__c FROM Contact WHERE id =: cntct.Id].First_Time_Contact__c;
        else
            return false;
    }
    
    @RemoteAction
    global static void updFirstTime(String contactId, Boolean firstTimeFlg)
    {
        List<Contact> contList = new List<Contact>();
        if(contactId != null && firstTimeFlg == true)
        {
            Contact con = new Contact(Id = contactId);
            con.First_Time_Contact__c = false;
            contList.add(con);
        }
        if(!contList.isEmpty())
            update contlist;
    }
}