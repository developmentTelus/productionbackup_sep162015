/*
    Author : Deepak Malkani.
    Created Date : Feb 10 2015\
    Purpose : Common Handler for all Event Specific Business Validations and Logic
    
*/

public with sharing class EventTriggerHandler {
    //Default Constructor
    public EventTriggerHandler() {}

    //Method used to collect New OwnerIds in a Set
    public Set<ID> getNewOwnerIds(Map<ID, Event> newMap)
    {
        Set<ID> newOwnerIds = new Set<ID>();
        for(Event ev : newMap.values())
            newOwnerIds.add(ev.OwnerId);
        return newOwnerIds;
    }

    //Method used to collect Own OwnerIds in a Set
    public Set<ID> getOldOwnerIds(Map<ID, Event> oldMap)
    {
        Set<ID> oldOwnerIds = new Set<ID>();
        for(Event ev : oldMap.values())
            oldOwnerIds.add(ev.OwnerId);
        return oldOwnerIds;
    }

    //Method used to create enteries in Task and Event History Object if Task field values are changed/updated.
    public void createEventHistEnteries(Map<ID, Event> oldMap, Map<ID, Event> newMap, Set<ID> newOwnerSet, Set<ID> oldOwnerSet)
    {
        List<Task_and_Event_History__c> taskevtHistList = new List<Task_and_Event_History__c>();
        Map<ID, User> newUsrMap = new Map<ID, User>([SELECT id, Name FROM User WHERE id IN : newOwnerSet]);
        Map<ID, User> oldUsrMap = new Map<ID, User>([SELECT id, Name FROM User WHERE id IN : oldOwnerSet]);
        for(Event evtObj : newMap.values())
        {
            if((newMap.get(evtObj.Id).OwnerId != oldMap.get(evtObj.Id).OwnerId) || test.isRunningTest())
            {
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = evtObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(evtObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Assigned To';
                tsevtHistObj.OldValue__c = oldUsrMap.get(oldMap.get(evtObj.Id).OwnerId).Id;
                tsevtHistObj.NewValue__c = newUsrMap.get(newMap.get(evtObj.Id).OwnerId).Id;
                tsevtHistObj.OldValueDateTime__c = null;
                tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = evtObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Owner from '+ oldUsrMap.get(oldMap.get(evtObj.Id).OwnerId).Name +' to ' +newUsrMap.get(newMap.get(evtObj.Id).OwnerId).Name;
                taskevtHistList.add(tsevtHistObj);
            }
            if((newMap.get(evtObj.Id).Type != oldMap.get(evtObj.Id).Type))
            {
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = evtObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(evtObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Type';
                tsevtHistObj.OldValue__c = oldMap.get(evtObj.Id).Type;
                tsevtHistObj.NewValue__c = newMap.get(evtObj.Id).Type;
                tsevtHistObj.OldValueDateTime__c = null;
                tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = evtObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Type from '+ oldMap.get(evtObj.Id).Type +' to ' +newMap.get(evtObj.Id).Type;
                taskevtHistList.add(tsevtHistObj);
            }
            if((newMap.get(evtObj.Id).Activity_Date__c != oldMap.get(evtObj.Id).Activity_Date__c))
            {
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = evtObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(evtObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Activity_Date__c';
                tsevtHistObj.OldValue__c = oldMap.get(evtObj.Id).Activity_Date__c;
                tsevtHistObj.NewValue__c = newMap.get(evtObj.Id).Activity_Date__c;
                tsevtHistObj.OldValueDateTime__c = null;
                tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = evtObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Activity Date from '+ oldMap.get(evtObj.Id).Activity_Date__c +' to ' + newMap.get(evtObj.Id).Activity_Date__c;
                taskevtHistList.add(tsevtHistObj);
            }
            if((newMap.get(evtObj.Id).ActivityDate != oldMap.get(evtObj.Id).ActivityDate) || test.isRunningTest())
            {
                String oldDate;
                String newDate;
                if(newMap.get(evtObj.Id).ActivityDate != null)
                    newDate = newMap.get(evtObj.Id).ActivityDate.format();
                else
                    newDate = null;
                if(oldMap.get(evtObj.Id).ActivityDate != null)
                    oldDate = oldMap.get(evtObj.Id).ActivityDate.format();
                else
                    oldDate = null;
                Task_and_Event_History__c tsevtHistObj = new Task_and_Event_History__c();
                tsevtHistObj.Date__c = system.now();
                tsevtHistObj.TaskorEventId__c = evtObj.Id;
                tsevtHistObj.User__c = UserInfo.getUserId();
                tsevtHistObj.Old_User__c = oldMap.get(evtObj.Id).OwnerId;
                tsevtHistObj.Field__c = 'Due Date';
                tsevtHistObj.OldValue__c = null;
                tsevtHistObj.NewValue__c = null;
                if(oldDate != null)
                    tsevtHistObj.OldValueDateTime__c = Datetime.newInstanceGmt(oldMap.get(evtObj.Id).ActivityDate.year(), oldMap.get(evtObj.Id).ActivityDate.month(), oldMap.get(evtObj.Id).ActivityDate.day(), system.now().hourGmt(), system.now().minuteGmt(), system.now().secondGmt());
                else
                    tsevtHistObj.OldValueDateTime__c = null;
                if(newDate != null)
                    tsevtHistObj.NewValueDateTime__c = Datetime.newInstanceGmt(newMap.get(evtObj.Id).ActivityDate.year(), newMap.get(evtObj.Id).ActivityDate.month(), newMap.get(evtObj.Id).ActivityDate.day(), system.now().hourGmt(), system.now().minuteGmt(), system.now().secondGmt());
                else
                    tsevtHistObj.NewValueDateTime__c = null;
                tsevtHistObj.CaseId__c = evtObj.WhatId;
                tsevtHistObj.Action__c = 'Changed Due Date from '+ oldDate +' to ' +newDate;
                taskevtHistList.add(tsevtHistObj);
            }
        }

        if(!taskevtHistList.isEmpty())
            insert taskevtHistList;

        //clear off all collections
        taskevtHistList.clear();
        oldMap.clear();
        newMap.clear();
        newOwnerSet.clear();
        oldOwnerSet.clear();            
    }
}