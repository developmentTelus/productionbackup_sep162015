/*
###########################################################################
# File..................: ContactTriggerHandler_Test.cls
# Version...............: 1.0
# Created by............: TechM 
# Created Date..........: 20-Apr-2015
# Description...........: Test  class for Contact Trigger ContactTriggerHandler.cls

# Copyright (c) Tech Mahindra. All Rights Reserved.
#
# Created by the Tech Mahindra. Modification must retain the above copyright #notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Bell Mobility, is hereby forbidden. Any modification to #source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
*/

@isTest(SeeAllData=false)
public with sharing class ContactTriggerHandler_Test {
	
	/*
	**	Method used to create a single case
	*/
	public static testMethod void ContactUpdateSuccessTest()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		Test.startTest();
		testHandler.setCustomSettings();
		Contact con = testHandler.createContactRecWithSourceRef();
		try{
			update con;
			System.assert(true);
		}catch(Exception e){
			System.assert(false);
		}
				
		
		Test.stopTest();
		
	}
	
	/*
	**	Method used to create a single case
	*/
	public static testMethod void ContactUpdateFailTest()
	{
		//Initialise Handler class
		UtlityTest testHandler = new UtlityTest();
		Test.startTest();
		Contact con = testHandler.createContactRecWithSourceRef();
		try{
			update con;
			System.assert(false);
		}catch(Exception e){
			System.assert(true);
		}
				
		
		Test.stopTest();
		
	}
	
}