/**
 * Author : Deepak Malkani
 * Created Date : June 22 2015
 * Purpose : Inbound Integration Service to be called from Retail and FieldLink apps to find Open Cases
 **/
global class OutcomeCaseFinderService_v1 {

	/*
	*	Written By : Deepak Malkani.
	*	Purpose : Web Service Method used to get Cases based on BAN No
	*/
	webservice static List<Case> getCasesbyBAN(String BillingAccntNo){
		List<Case> csList;
		if(!String.isEmpty(BillingAccntNo))
			csList = [SELECT CaseNumber, Type, Owner.Name, Line_of_Business__c, Related_Order_Ticket__c, Order_Ticket_ID__c, CreatedDate, Contact.Name, Language_Preference__c
						FROM Case
						WHERE Billing_Account_Number__c =: BillingAccntNo AND (Status = 'Open' OR Status = 'Pending Close')
						ORDER BY CreatedDate DESC];
		if(!csList.isEmpty())
			return csList;
		
		else
			return null;
	}

	/*
	*	Written By : Deepak Malkani.
	*	Purpose : Web Service Method used to get Cases based on Customer Account No
	*/
	webservice static List<Case> getCasesbyCAN(String CustmerAccntNo){
		List<Case> csList;
		Set<ID> accntIds = new Set<ID>();
		if(!String.isEmpty(CustmerAccntNo))
			for(Account acc : [SELECT id, SourceRefId__c FROM Account WHERE Parent.SourceRefId__c =: 'C_'+CustmerAccntNo AND Parent.Type = 'Customer'])
			{
				accntIds.add(acc.Id);
			}
		if(!accntIds.isEmpty())
		{
			csList = [SELECT CaseNumber, Type, Owner.Name, Line_of_Business__c, Related_Order_Ticket__c, Order_Ticket_ID__c, CreatedDate, Contact.Name, Language_Preference__c
					FROM Case
					WHERE AccountId IN : accntIds AND (Status = 'Open' OR Status = 'Pending Close')
					ORDER BY CreatedDate DESC];
			if(!csList.isEmpty())
				return csList;
			else
				return null;
		}
		else 
			return null;
	}

	/*
	*	Written By : Deepak Malkani.
	*	Purpose : Web Service Method used to get Cases by Case Number.
	*/
	webservice static List<Case> getCasesbyCaseNo(String CaseNo){
		List<Case> csList = new List<Case>();
		if(!String.isEmpty(CaseNo))
			csList = [SELECT CaseNumber, Type, Owner.Name, Line_of_Business__c, Related_Order_Ticket__c, Order_Ticket_ID__c, CreatedDate, Contact.Name, Language_Preference__c
						FROM Case
						WHERE CaseNumber =: CaseNo AND (Status = 'Open' OR Status = 'Pending Close')
						ORDER BY CreatedDate DESC];
		if(!csList.isEmpty())
			return csList;
		else
			return null;
	}

	/*
	*	Written By : Deepak Malkani.
	*	Purpose : Web Service Method used to get Cases by Related Ticket #
	*/
	webservice static List<Case> getCasesbyRelatedItem(String relatedItemType, String relatedItemId){
		List<Case> csList = new List<Case>();
		if((!String.isEmpty(relatedItemType)) && (!String.isEmpty(relatedItemId)))
			csList = [SELECT CaseNumber, Type, Owner.Name, Line_of_Business__c, Related_Order_Ticket__c, Order_Ticket_ID__c, CreatedDate, Contact.Name, Language_Preference__c
						FROM Case
						WHERE Related_Order_Ticket__c =: relatedItemType AND Order_Ticket_ID__c =: relatedItemId AND (Status = 'Open' OR Status = 'Pending Close')
						ORDER BY CreatedDate DESC];
		if(!csList.isEmpty())
			return csList;
		else
			return null;
	}	
}