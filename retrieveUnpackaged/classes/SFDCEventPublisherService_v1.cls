/*
 *  Author : Deepak Malkani.
 *  Created Date : July 8 2015
 *  Purpose : Custom Web service used to create Event Records inside SFDC, from various src systems.
 */

global class SFDCEventPublisherService_v1 {
	
	
	global class EventRecord{
		webservice String sourceName;
		webservice String sourceEventType;
		webservice String sourceObjectType;
		webservice String eventTriggeredBy;
		webservice Datetime eventSrcSystemTimeStamp;
		webservice String sourceRefID;
		webservice String customerID;
		webservice String billingAccountNo;
		webservice String banSrcSystem;
		webservice String assetID;
		webservice String assetSrcSystem;
		webservice String contactID;
		webservice String contactSrcSystem;
		webservice String eventCentralOfficeID;
		webservice String eventDealerCode;
		webservice String eventPostalCode;
		webservice String sourceEventJSONMessage;
	}

	global class EventResponse{
		webservice String responseMessage;
		webservice String responseError;
		webservice String sfdcId;
	}

	webservice static List<EventResponse> createEvents(List<EventRecord> evtRecords){
		return eventCreation_Helper.createEvent(evtRecords);
	}

}