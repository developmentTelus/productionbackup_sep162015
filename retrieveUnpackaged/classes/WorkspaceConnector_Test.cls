@isTest
private class WorkspaceConnector_Test {
    
    public static testMethod void WorkspaceConnector_Test() {       
        String connectorInfo = WorkspaceConnector.getWorkspaceConnectorInfo();
        System.assert(connectorInfo != null);
    }
    
    public static testMethod void findObjectFromANI_Test() {
        String id;
        //Error leg
        id = WorkspaceConnector.findObjectFromANI('416300555555');
        System.assert(id == 'not found');
        
        //Create Account test data 
        Account acc = new Account(Name = 'testAccount',Phone = '4163005555'); 
        id = WorkspaceConnector.findObjectFromANI('4163005555');
        System.assert(id != null);
        
        insert acc;
        id = WorkspaceConnector.findObjectFromANI('4163005555');
        System.assert(id != null);
        
        //Create Contact test data
        Contact con = new Contact(FirstName = 'John', LastName = 'Doe', Phone = '4163004444', MobilePhone = '4163003333');
        id = WorkspaceConnector.findObjectFromANI('4163004444');
        System.assert(id != null);
        
        insert con;
        id = WorkspaceConnector.findObjectFromANI('4163004444');
        System.assert(id != null);
    }
    
    @isTest(SeeAllData=true)
    static void createActivity_Test() {
        User u = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            String ID = '';
            //Create Contact test data
            Contact con1 = new Contact(FirstName = 'John', LastName = 'Doe', Phone = '4163009999', MobilePhone = '4163009999');
            insert con1;
            Contact newCon1 = [SELECT Id FROM Contact WHERE Phone = '4163009999'];
            ID = newCon1.Id;
            Map<String,String> fieldsMap = new Map<String,String>();
            //create map
            fieldsMap.put('Genesys Call Type','Inbound');
            fieldsMap.put('Call Duration','35');
            fieldsMap.put('objId',ID);
            fieldsMap.put('Account Number','1234');
            fieldsMap.put('Call Duration','00.00.35');
            fieldsMap.put('Calling Line Id','1234567');
            fieldsMap.put('Call Topic','Error Correction');
            fieldsMap.put('Call Type','Daily Business Call');
            fieldsMap.put('Comments','');
            fieldsMap.put('Disposition','Unresolved');
            fieldsMap.put('DNIS','7654321');
            fieldsMap.put('GenesysId','0A123B456');
            fieldsMap.put('last IVR','High Priority');
            fieldsMap.put('Subscriber Phone Number','9876543');
            fieldsMap.put('transfer history','');
            String noIDResult = WorkspaceConnector.createActivity(fieldsMap);
            System.assert(noIDResult != null);
            
            //Create Contact test data
            Contact con = new Contact(FirstName = 'John', LastName = 'Doe', Phone = '4163004444', MobilePhone = '4163003333');
            //con.AccountId = ID;  //account id
            insert con;
            Contact newCon = [SELECT Id FROM Contact WHERE Phone = '4163004444'];
            ID = newCon.Id;
            fieldsMap.put('objId',ID);
            String contactResult = WorkspaceConnector.createActivity(fieldsMap);
            System.assert(contactResult != null);
        }
    }
}