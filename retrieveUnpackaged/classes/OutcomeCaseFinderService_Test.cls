/*
	Author : Deepak Malkani.
	Created Date : 16/07/2015
	Purpose : The class provides test coverage for Outcome Case Finder web service used by FieldLink and other applications
*/

@isTest(SeeAllData=false)
private class OutcomeCaseFinderService_Test
{
	static Account createCustomerAccnt(){
		Account a = new Account(Type = 'Customer', Name = 'Test1', SourceRefId__c = 'C_123456');
		insert a;
		return a;
	}
	static Account createBillAccnt(){
		Account parent = createCustomerAccnt();
		Account acc = new Account(Type = 'FFH', Name = 'TestFFH', SourceRefId__c = '123457',
									ParentId = parent.Id, Billing_Account_Number__c = 'B_1234567');
		insert acc;
		return acc;
	}

	@isTest
	static void UnitTest1_SearchCasesbyBan()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyBAN('B_1234567');
		Test.stopTest();
		system.assertEquals(1, csList.size());
	}

	@isTest
	static void UnitTest2_SearchCasesbyBan_Negative(){
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		system.debug('---> Account inserted is '+billAccnt);
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		cs.AccountId = null;
		update cs;
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyBAN('B_1234567');
		Test.stopTest();
		system.assertEquals(null, csList);
	}

	@isTest
	static void UnitTest3_SearchCasesbyCan()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyCAN('123456');
		Test.stopTest();
		system.assertEquals(1, csList.size());
	}

	@isTest
	static void UnitTest4_SearchCasesbyCan_Negative()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyCAN('C_123456');
		Test.stopTest();
		system.assertEquals(null, csList);
	}

	@isTest
	static void UnitTest5_SearchCasesbyCaseNo()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		String caseNo = [SELECT CaseNumber FROM Case WHERE id =: cs.Id].CaseNumber;
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyCaseNo(caseNo);
		Test.stopTest();
		system.assertEquals(1, csList.size());
	}

	@isTest
	static void UnitTest6_SearchCasesbyCaseNo_Negative()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyCaseNo('');
		Test.stopTest();
		system.assertEquals(null, csList);
	}

	@isTest
	static void UnitTest7_SearchCasesbyRelatedTick()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		cs.Related_Order_Ticket__c = 'OMS';
		cs.Order_Ticket_ID__c = '123456';
		update cs;
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyRelatedItem('OMS', '123456');
		Test.stopTest();
		system.assertEquals(1, csList.size());
	}

	@isTest
	static void UnitTest8_SearchCasesbyRelatedTick_Negative()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		cs.Related_Order_Ticket__c = 'OMS';
		cs.Order_Ticket_ID__c = '123456';
		update cs;
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyRelatedItem('ODD', '123456');
		Test.stopTest();
		system.assertEquals(null, csList);
	}

	@isTest
	static void UnitTest9_SearchCasesbyRelatedTick_Negative2()
	{
		//Prepare Test Data
		Account billAccnt = createBillAccnt();
		UtlityTest handler = new UtlityTest();
		Case cs = handler.createCasewtAcnt(billAccnt);
		cs.Related_Order_Ticket__c = 'OMS';
		cs.Order_Ticket_ID__c = '123456';
		update cs;
		List<Case> csList = new List<Case>();
		Test.startTest();
		//Make Call out to the webservice 
		csList = OutcomeCaseFinderService_v1.getCasesbyRelatedItem('', '');
		Test.stopTest();
		system.assertEquals(null, csList);
	}
}