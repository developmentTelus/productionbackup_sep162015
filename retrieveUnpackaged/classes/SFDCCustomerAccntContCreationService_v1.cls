/*
 * Author : Deepak Malkani.
 * Created Date : April 30 2015
 * Purpose : Inbound web service used to create Customer Account and link it to Contacts.
*/

global class SFDCCustomerAccntContCreationService_v1 {
	
	 /*
     * Global Class describing the structure of Customer Record.
    */
    global class customerRec{
        webservice String CustName;
        webservice String CustStatus;
        webservice String CustType;
        webservice String CustSubType; //new attribute added
        webservice String CustBrand;
        webservice String CustDescription;
        webservice Integer CustBillCycleCode;
        webservice Integer CustBillCycleDay;
        webservice String CustLangPref;
        webservice String CustPhone;
        webservice String CustId;
        webservice Address CustAddress; //new attribute added
        webservice List<custContact> CustContacts;
    }
    /*
     * Global class describing the structure of keys
    */
    global class Keys{
        webservice String Id;
        webservice String srcSystemName;
    }
    /*
     * Global class describing Canadian Addresses
    */
    global class Address{
    	webservice CivicAddress civicAddr;
    	webservice PostBoxAddress POBOXAddr;
    	webservice RuralRouteAddress RRAddr;
    	webservice GenDeliveryAdddress GenDelAddr;
        webservice MilitaryAddress MilitaryAddr;
        webservice USAAddress USAddr;
        webservice IntlAddress IntlAddr;
    }
    /*
     * Global class describing Canadian Civic Address
    */
    global class CivicAddress{
    	webservice String careOf;
        webservice String CivicAdditonalInfo;
        webservice String CivicUnitTypeCd;
        webservice String CivicUnitNum;
        webservice String CivicNumber;
        webservice String CivicNumberSuffix;
        webservice String CivicStreetName;
        webservice String CivicStreetCd;
        webservice String CivicStreetDirectionCd;
        webservice String CivicMunicipalityName;
        webservice String CivicProvince;
        webservice String CivicPostalCd;
        webservice String CivicCountry;
    }
    /*
     * Global class describing Canadian Postal Address
    */
    global class PostBoxAddress{
    	webservice String careOf;
        webservice String stationTypeCd;
        webservice String stationAreaCd;
        webservice String stationQualifier;
        webservice String AdditonalInfo;
        webservice String POBoxNum;
        webservice String MunicipalityName;
        webservice String Province;
        webservice String PostalCd;
        webservice String Country;
    }
    /*
     * Global class describing Rural Route Address
    */
    global class RuralRouteAddress{
    	webservice String careOf;
        webservice String RuralRouteType;
        webservice String RuralRouteNum;
        webservice String stationTypeCd;
        webservice String stationAreaNum;
        webservice String stationAreaName;
        webservice String stationQualifier;
        webservice String RuralAdditionalInfo;
        webservice String RuralMuncipalityName;
        webservice String Province;
        webservice String PostalCd;
        webservice String Country;
    }
    /*
     * Global class describing General Delivery Address
    */
    global class GenDeliveryAdddress{
    	webservice String careOf;
        webservice String stationTypeCd;
        webservice String stationAreaName;
        webservice String stationQualifier;
        webservice String genAdditionalInfo;
        webservice String GenDelMunicipalityName;
        webservice String GenDelProvince;
        webservice String GenDelPostalCd;
        webservice String GenDelCountry;
    }
     /*
    *   Global class describing USA Address
    */
    global class USAAddress{
        webservice String careOf;
        webservice String municipalityName;
        webservice String stateCode;
        webservice String countryCode;
        webservice String zipCode;
        webservice String additionalAddressInformation;
    }
    /*
    *   Global class describing Intl Address
    */
    global class IntlAddress{
        webservice String careOf;
        webservice String stateCode;
        webservice String countryCode;
        webservice String additionalAddressInformation;
    }
     /*
    *   Global class describing Military Address
    */
    global class MilitaryAddress{
        webservice String careOf;
        webservice String hmcsName;
        webservice String fleetMailOfficeName;
        webservice String postOfficeBoxNumber;
        webservice String stationTypeCode;
        webservice String stationAreaName;
        webservice String stationQualifier;
        webservice String municipalityName;
        webservice String provinceCode;
        webservice String postalCode;
        webservice String additionalAddressInformation;
    }
    /*
     * Global class describing the structure of contact Records for the parent Customer Account. 
    */
    global class custContact{
    	webservice String contAssociations;//new attribute added
        webservice String contFirstName;
        webservice String contLastName;
        webservice String contSalutation;
        webservice String contMobPhone;
        webservice String contPhone;
        webservice String contLanguagePref;
        webservice String contOtherPhone;
        webservice String contHomePhone;
        webservice String contEmailAddress;
        webservice Date contExpirationDate;
        webservice Date contStartDate;
        webservice String contAssignmentId;
        webservice String contRelnType; //new attribute added
        webservice Keys contSrcKey; //new attribute added
        
    }
    /*
     *	Global Class encapsulating the response from the Web service Call Out. 
	*/
    global class custContResponse{
        webservice String SfdcId;
        webservice String respMessage;
        webservice String respError;
    }

    /*
     *	Web Service method which will be called to create Customer Accounts and its children Contacts.   
	*/
    webservice static custContResponse createCustandContacts(customerRec cust){
        custContResponse response = customerContactCreation_Helper.customerRecCreate(cust);
        return response;     
    }    
}