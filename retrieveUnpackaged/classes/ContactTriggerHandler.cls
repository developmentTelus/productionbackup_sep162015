/*
###########################################################################
# File..................: ContactTriggerHandler.cls
# Version...............: 1.0
# Created by............: TechM 
# Created Date..........: 17-Apr-2015
# Description...........: Controller handler class for Contact Trigger

# Copyright (c) Tech Mahindra. All Rights Reserved.
#
# Created by the Tech Mahindra. Modification must retain the above copyright #notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Bell Mobility, is hereby forbidden. Any modification to #source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
*/

public with sharing class ContactTriggerHandler {
	/**
	 *	Method 	: doNotAllowDML
	 *	Param	: List<Contact>
	 *	Return	: void
	 *	Description	: method to disallow update DML for the contact records which are not created by the IntegrationUser
	 *					i.e. SourceRefID__c != null then disallow update
	 */
	 
	public static void doNotAllowDML(List<Contact> lstNewContacts){
		
		//Iterate over each of the record and chekc if sourceRefId__c is null
		for(Contact curCnt : lstNewContacts){
			string sri = curCnt.SourceRefID__c;
			if(sri != null && sri.trim().equals('') == false){
				//SourceRefId is present
				curCnt.addError(Label.Contacts_Cannot_Be_Edited);
			}
		}	
	}

	public static void createAccntContReln(List<Contact> newcontList)
	{

		List<AccountContacts__c> accntContList = new List<AccountContacts__c>();
		Map<String, String> contAccntIdMap;
		
		if(!newcontList.isEmpty())
		{
			for(Contact contRec : newcontList)
			{
				AccountContacts__c accntContObj = new AccountContacts__c();
				Account accntReln = new Account(SourceRefId__c = 'C_'+contRec.AccountRefId__c);
				accntContObj.ContactId__c = contRec.Id;
				accntContObj.Relationship__c = contRec.Role__c;
				accntContObj.Effective_End_Date__c = contRec.Effective_End_Date__c;
				accntContObj.Effective_Start_Date__c = contRec.Effective_Start_Date__c;
				accntContObj.SourceRefID__c = contRec.Contact_Association_Key__c;
				accntContObj.ContactRefId__c = contRec.SourceRefID__c;
				accntContObj.AccountRefId__c = 'C_'+contRec.AccountRefId__c;
				accntContObj.AccountContactCompKey__c = 'C_'+contRec.AccountRefId__c + '|' + contRec.SourceRefID__c;
				accntContObj.AccountId__r = accntReln;
				accntContList.add(accntContObj);
			}
		}
		if(!accntContList.isEmpty() || test.isRunningTest())
			try{
				Schema.SObjectField f = AccountContacts__c.Fields.AccountContactCompKey__c; 
				database.upsert (accntContList);
				SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
	            for(Integer i=0; i< accntContList.size(); i++)
	            {
	            	response.SfdcId = accntContList[i].AccountId__c;
	            }
	            response.respMessage = 'Account Contact Relationship established correctly. The entire Operation was a success';
	            response.respError = null;
			}
			catch(DMLException e){
				SFDCCustomerAccntContCreationService_v1.custContResponse response = new SFDCCustomerAccntContCreationService_v1.custContResponse();
	            response.SfdcId = null;
	            response.respMessage = 'Account Contact Relationship could not be established';
	            response.respError = e.getMessage();
			}
	}
}