/**
 * Author : Deepak Malkani
 * Created Date : Feb 17 2015
 * Purpose : Inbound Integration Service to be called from SD Application to get Open Cases related to BAN
 **/
global class SFDCConsumerCaseInquiryService
{
    webservice static List<Case> getOpenCasesForBAN (String BillingAccntNum)
    {
        List<Case> caseList = new List<Case>();
        if(!String.isEmpty(BillingAccntNum))
        {
            caseList = [select CaseNumber, Type, Owner.Name, Owner.Phone, Billing_Account_Number__c 
                        FROM Case 
                        WHERE Billing_Account_Number__c =: BillingAccntNum AND (Status = 'Open' OR Status = 'Pending Close')
                        ORDER BY CreatedDate DESC
                        LIMIT 3];
            return caseList;
        }
        else
            return null;
    }
}