/**
 * Author : Deepak Malkani
 * Created Date : March 13 2015
 * Purpose : This is a test class for open Case Finder and Open Case Resminder Batch classes.
 */
@isTest(SeeAllData=false)
public class openCasefinder_Batch_Test {

    static testMethod void UnitTest1() {
      
       //Initialise handler
      UtlityTest testhandler = new UtlityTest();
      List<Batch_Logger__c> batchLogList;
      List<Task> tkList;
      List<Task> tkListUpd = new List<Task>();

      //Prepare Test Data for testing
      List<Case> openCaseList = testhandler.createopenCaseswtNoTasks(200);
      Test.startTest();
      //run the batch
      String Query = 'SELECT id, CaseNumber, Status, OwnerId FROM Case WHERE Status = \'Open\'';
      database.executeBatch(new openCasefinder_Batch(Query));
      Test.stopTest();
      
      batchLogList = [SELECT id, Batch_Status__c, Case__c FROM Batch_Logger__c WHERE Batch_Status__c = 'Completed'];
      tkList = [SELECT id, Subject FROM Task WHERE WhatId IN : openCaseList AND Subject = 'Close Case Reminder'];
      //Making assetions
      system.assertEquals(200, batchLogList.Size());
      system.assertEquals(200, tkList.Size());
      //lets update the existing tasks and set the reminder flag to false
      for(Integer i=0; i< tkList.Size(); i++)
      {
      	Task tkObj = new Task(Id = tkList[i].Id);
      	tkObj.isReminderSet = false;
      	tkListUpd.add(tkObj);
      }

      if(!tkListUpd.isEmpty())
      	update tkListUpd;
      system.runAs(testhandler.createTestUsr())
      {
      	String Query2 = 'SELECT id, CaseNumber, Status, OwnerId FROM Case WHERE Status = \'Open\'';
      	database.executeBatch(new openCasefinder_Batch(Query2));

      	//Make assertions
      	batchLogList = [SELECT id, Batch_Status__c, Case__c FROM Batch_Logger__c WHERE Batch_Status__c = 'Completed'];
      	system.assertEquals(200, batchLogList.Size());
      }
    }
}