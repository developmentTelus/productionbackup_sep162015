@isTest(SeeAllData=false)
public class CaseTriggerHandler_LongTest
{

    public static UtlityTest testUtil = new UtlityTest();

    /*
        Deepak Malkani : Unit test method - for bulkification. This creates 200 tasks to a case.
        If the case owner changes, all tasks linked to the case will have the same owner as case owner. 
    */
    @isTest
    public static void UnitTest1()
    {
        //Initialise Handler class
        UtlityTest testHandler = new UtlityTest();
        Test.startTest();
        Contact con = testHandler.createContactRec();
        Case csObj = testHandler.createCaseRec(con);
        List<Task> tkList = testHandler.createMultiTaskRec(200);
        List<Task> tkUpdList = new List<Task>();
        User u = testHandler.createTestUsr();
        for(Integer i=0; i<tkList.Size(); i++)
        {
            Task tk = new Task(Id = tkList[i].Id);
            tk.WhatId = csObj.Id;
            tkUpdList.add(tk);
        }
        if(!tkUpdList.isEmpty())
            update tkUpdList;
        //lets now update the case with a changed owner id
        //reload the static variable, so that update event fires again.
        Outcome_StaticVars.canRun = true;
        csObj.OwnerId = u.Id;
        update csObj;
        Test.stopTest();
        //perform system asserts
        system.assertEquals(u.Id, [SELECT OwnerId FROM Task WHERE id IN : tkUpdList LIMIT 1].OwnerId);  
    }
    
    /*
        Deepak Malkani : Unit Test Method - for Bulkification. This creates 200 cases and links a close
        case reminder task to each case. When the case is closed, all the close case reminders will also Complete.
    */
	@isTest
	public static void UnitTest2()
    {
        //Initialise Handler class
        UtlityTest testHandler = new UtlityTest();
        Test.startTest();
        List<Case> csObjList = testHandler.createopenCaseswtNoTasks(200);
        List<Task> tkList = new List<Task>();
        List<Case> csUpdList = new List<Case>();
        List<Task> tkgetList;
        for(Integer i=0 ; i< csObjList.size(); i++)
        {
            tkList.add(testHandler.createClsCaseRemindTaskRec(csObjList[i].Id));
        }
        if(!tkList.isEmpty())
            insert tkList;
        //Lets start closing these open cases
        for(Integer j=0; j<csObjList.Size(); j++)
        {
            Case caseUpd = new Case(Id = csObjList[j].Id);
            caseUpd.Status = 'Pending Close';
            csUpdList.add(caseUpd);
        }
        //now set the static variable so that update fires again.
        Outcome_StaticVars.canRun = true;
        if(!csUpdList.isEmpty())
            update csUpdList;
        Test.stopTest();
        tkgetList = [SELECT id, Status FROM Task WHERE id IN : tkList AND Status = 'Completed'];
        //make assertions
        system.assertEquals(200, tkgetList.size());
    }

	/*    
        Deepak Malkani : Created Test Method - for Bulkification needs. This method creates 200 cases
        which are of type Renewals and the owner is modified in bulk to test scalability.
    
	*/    
	// Deepak Malkani : Unit Test Method for RPT1-Renewals -- in bulk mode
    @isTest
    public static void UnitTest3()
    {
        //Creating Test Data
        UtlityTest testHandler = new UtlityTest();
        User u = [SELECT id FROM User WHERE Name = 'Integration User1' LIMIT 1];
        List<Case> caseUpList = new List<Case>();
		List<Case> c = new List<Case>();
        //Start Testing
        Test.startTest();
        ETLLoads__c CS = testHandler.setCustomSettings(); //CS Set to true to bypass all trigger logic
        Contact con = testHandler.createContactRecWithSourceRef();
 		system.runAs(u)
 		{
        	c = testHandler.createMultiCaseRec(con, u, 200);
 		}
        CS.runDataLoads__c = false;
        update CS; //uncheck the CS, so that trigger gets fired now if OwnerId is changed.
        //Outcome_StaticVars.canRun = true;
        for(Integer i=0; i<c.Size(); i++)
        {
            Case csObj = new Case(Id = c[i].Id);
            csObj.Source_Type__c = 'RPT1-Renewal';
            csObj.OwnerId = UserInfo.getUserId();
            caseUpList.add(csObj);
        }
        if(!caseUpList.isEmpty())
            update caseUpList;
        List<Task> tkList = [SELECT id, WhatId FROM Task WHERE WhatId IN : caseUpList];
       // system.assertEquals(400, tkList.Size());     
    }               

     /*
        Deepak Malkani : Created Test Method - for Bulkification needs. This method creates 200 cases
        which are of type Renewals and the owner is modified in bulk to test scalability.
    */
    @isTest
    public static void UnitTest4()
    {
        //Creating Test Data
        UtlityTest testHandler = new UtlityTest();
        User u = [SELECT id FROM User WHERE Name = 'Integration User1' LIMIT 1];
        List<Case> caseUpList = new List<Case>();

        //Start Testing
        Test.startTest();
        ETLLoads__c CS = testHandler.setCustomSettings(); //CS Set to true to bypass all trigger logic
        Contact con = testHandler.createContactRecWithSourceRef();
        List<Case> c = testHandler.createMultiCaseRec(con, u, 200);
        CS.runDataLoads__c = false;
        update CS; //uncheck the CS, so that trigger gets fired now if OwnerId is changed.
        for(Integer i=0; i<c.Size(); i++)
        {
            Case csObj = new Case(Id = c[i].Id);
            csObj.Source_Type__c = 'RPT1-Renewal';
            csObj.Outcome_Review__c = false;
            csObj.Outcome_Review_Due_Date__c = null;
            csObj.OwnerId = UserInfo.getUserId();
            caseUpList.add(csObj);
        }
        if(!caseUpList.isEmpty())
            update caseUpList;
        Test.stopTest();
        List<Task> tkList = [SELECT id, WhatId FROM Task WHERE WhatId IN : caseUpList and (not Subject like 'Email%')];
       // system.assertEquals(200, tkList.Size());     
    }     


    // HH
	@isTest
	public static void testScenario5() {
        
        // ARRANGE      
        Contact contact = new Contact(LastName = 'Test');
        insert contact;

        List<Case> cases = new List<Case>();
        for(Integer i=0; i<200; i++) {
        	Case cse = new Case(
        		ContactId = contact.Id,
        		SourceRefID__c = 'RTS-abc' + i,
        		Source_Action_Cd__c = 'APPLE OTC Delay'
        	);
        	cases.add(cse);
        }
        insert cases;
        // ACT  
        Outcome_StaticVars.canRun = true;
        for (Case cse : cases) {
	        cse.SourceStatus__c = 'Submitted';
        }

        update cases;

        // ASSERT 
        List<Task> tasks = [Select Id, Description, Subject From Task where (not Subject like 'Email%')];
        System.assertEquals(200, tasks.size());
        System.assertEquals('Close Review', tasks[0].Subject);  
        System.assert(tasks[0].Description.StartsWith('IMPRESS: Please contact')); 
    }
}