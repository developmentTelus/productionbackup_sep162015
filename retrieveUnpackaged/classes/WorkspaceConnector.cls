// **************************************************************************************************************
// Name:            Workspace Connector Class
// Description:     Genesys iWS CTI Adapter (Asset) class to process calls
// Author(s):       Rahul Badal
// ************************Version Updates***********************************************************************
//
// Updated Date     Updated By      Update Comments 
//
// **************************************************************************************************************
global class WorkspaceConnector {
    
    @RemoteAction
    global static String findObjectFromANI(String ANI) {
        system.debug('*** findObjectFromANIfor '+ANI);       
        try {
            List<Account> objects = [SELECT Id, Name, Phone FROM Account WHERE Phone = :ANI];
            if (!objects.isEmpty()) {
                for (Account obj : objects) {
                    system.debug('*** findObjectFromANI account = '+ obj.Id);
                    return obj.Id;
                } 
            }                
            try {
                List<Contact> cobjects = [SELECT Id, Name, Phone FROM Contact WHERE Phone = :ANI OR MobilePhone = :ANI];
                if (!cobjects.isEmpty()) {
                    for (Contact cobj : cobjects) {
                        system.debug('*** findObjectFromANI contact = '+ cobj.Id);
                        return cobj.Id;
                    } 
                }                
                return 'not found'; 
            } catch (QueryException e) {
                return 'not found'; 
            }
        } catch (QueryException e) {
            return 'not found'; 
        }        
    }
    
    @RemoteAction
    global static String getWorkspaceConnectorInfo() {
        system.debug('*** getWorkspaceConnectorInfo: ');
        
        try {          
            User a = [SELECT WorkspacePollPort__c,WorkspaceRequestPort__c FROM User WHERE Id= :UserInfo.getUserId()];
            
            system.debug('*** WorkspacePollPort = ' + a.WorkspacePollPort__c);
            system.debug('*** WorkspaceRequestPort = '+ a.WorkspaceRequestPort__c);
            //return json string id  and full name
            return '{"WorkspacePollPort":"' + a.WorkspacePollPort__c + '","WorkspaceRequestPort":"' + a.WorkspaceRequestPort__c + '"}';
        } catch (QueryException e) {
            return 'not found'; 
        }
        return 'not found';
    }    
    
    @RemoteAction global static String createActivity(Map<String,String> activityMap){
        system.debug('*** createActivity' );
        String result = 'not found';
        
        RecordType activityRecordType;
        try {
            activityRecordType = [SELECT Id from RecordType Where SobjectType = 'Task' and DeveloperName = 'Call_Activity_Record_Type'];
        } catch(QueryException e) {
            system.debug('*** QueryException ' + e);
        }            
        
        String callType = activityMap.get('Genesys Call Type');
        String SF_callType = 'Inbound';
        if (callType == 'Outbound') {
            SF_callType = 'Outbound';
        }
        String tempCallType = '';
        if (callType == 'Inbound') {
            tempCallType = 'Inbound';
        } else {
            tempCallType = 'Outbound'; // Rest all is outbound right now
        }
        
        String subject = tempCallType + ' call ';
        String ActivityDate = activityMap.get('DATE');
        String duration = activityMap.get('Call Duration');
        String hours = duration.substring(0,2);
        String mins  = duration.substring(3,5);
        String secs  = duration.substring(6,8);
        Integer hh = Integer.valueOf(hours);
        Integer mm = Integer.valueOf(mins);
        Integer ss = Integer.valueOf(secs);  
        Integer durationInSecs = ss + (mm*60) + (hh*3600);
        system.debug('*** durationInSecs = '+ durationInSecs );
        
        String objectToUse =  activityMap.get('objId');
        system.debug('*** createActivity for object ' + objectToUse);
        String accountPrefix = Schema.SObjectType.Account.getKeyPrefix();
        String contactPrefix = Schema.SObjectType.Contact.getKeyPrefix();
        
        //String prefix = '';
        //prefix = contactPrefix;
        //prefix = ''; // For S2 activities will be created based on the primary object ID.  
        
        //if (prefix == '') {
            Task t = new Task (
                WhoId = objectToUse,
                Type = 'Call',
                Status = 'Completed',
                Subject = subject,
                CallDurationInSeconds = durationInSecs,
                Call_Duration__c = activityMap.get('Call Duration'),
                Calling_Line_ID__c = activityMap.get('Calling Line Id'),
                Call_Topic__c = activityMap.get('Call Topic'),
                Call_Type__c = activityMap.get('Call Type'),
                CallType = SF_calltype,
                Description = activityMap.get('Comments'),
                Disposition__c = activityMap.get('Disposition'),
                DNIS__c = activityMap.get('DNIS'),
                GenesysId__c = activityMap.get('GenesysId'),
                Last_IVR_Menu__c = activityMap.get('last IVR'),
                TN_Number__c = activityMap.get('Subscriber Phone Number'),
                Transfer_History__c = activityMap.get('transfer history'),
                Activity_Date__c = ActivityDate
            );
            
            if (activityRecordType != null ) {
                t.RecordTypeId = activityRecordType.Id;
            } else {
                system.debug('*** No activityRecordType');
            }              
            try {                        
                insert t;
            } catch (QueryException qe) {
                return 'not found'; 
            }
            system.debug('*** Task id = '  +t.Id);
            result = t.Id;
            return result;               
        //}
        //return result;
    }   
}