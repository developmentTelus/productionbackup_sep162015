@isTest(SeeAllData=false)
public class FlowHandlerTest {

	@isTest
	public static void testScenario01() {

		// ARRANGE
		Case cse = new Case(SOURCESTATUS__c  = 'Submitted', Source_Action_CD__c = 'Repair and Return');

		// ACT
		Map<String, Object> result = FlowHandler.launch_RTS_Task_Fields(cse);

		// ASSERT
		System.assertEquals('Courtesy Call', result.get('taskSubject'));
		System.assert(((String) result.get('taskDescription')).startsWith('MUST CALL'));
	}

	@isTest
	public static void testScenario02() {

		// ARRANGE

		// ACT
		for (Integer i =0 ; i<200; i++) {
			Case cse = new Case(SOURCESTATUS__c  = 'Submitted', Source_Action_CD__c = null);
			Map<String, Object> result = FlowHandler.launch_RTS_Task_Fields(cse);
		}

		// ASSERT
		System.assert(true);
	}

	@isTest
	public static void testScenario03() {

		List<Scenario> scenarios = new List<Scenario> {
			new Scenario('Received in service Vendor',		'Repair and Return',	'Received at Vendor',		'Proactive customer', 0),
			new Scenario('Vendor waiting for confirmation', 'Repair and Return',	'Get Customer Approval',	'MUST CALL CUSTOMER - 1st', 0),
			new Scenario('Service Vendor ship to store',	'Repair and Return',	'Device Shipped to Store',	'Proactive customer communication :Please prepare', 0),
			new Scenario('Service Vendor ship to store',	'Apple OTC Delay',	'Device Shipped to Store',	'Proactive customer communication :Please prepare', 0),
			new Scenario('Ready for pickup',				'Repair and Return',	'Ready for Pickup',			'MUST CALL CUSTOMER - 1st', 0),
			new Scenario('Ready for pickup',				'Apple OTC Delay',	'Ready for Pickup',			'MUST CALL CUSTOMER - 1st', 0),
			new Scenario('Pending',							'Apple OTC Delay',	'Courtesy Call',			'MUST CALL CUSTOMER - Repair', 0),
			new Scenario('Submitted',						'Apple OTC immediate',	'Close Review',				'IMPRESS: Please contact customer for direct feedback.', 2),
			new Scenario('Submitted',						'DOA',					'Close Review',				'IMPRESS: Please contact customer for direct feedback.', 2),
			new Scenario('Submitted',						'Apple OTC Delay',	'Close Review',				'IMPRESS: Please contact customer for direct feedback.', 2),
			new Scenario('Closed',							'Repair and Return',	'Close Review',				'IMPRESS: Please contact customer for direct feedback.', 2),

			//new Scenario('Service Vendor ship to store',	'Apple OTC immediate',	null, null, null),
			//new Scenario('Service Vendor ship to store',	'DOA',					null, null, null),
			new Scenario('Vendor waiting for confirmation', 'Apple OTC Delay',	null, null, null),
			new Scenario('Ready for pickup',				'Apple OTC immediate',	null, null, null),
			new Scenario('Ready for pickup',				'DOA',					null, null, null),
			new Scenario('Closed',							'Apple OTC Delay',	null, null, null),
			new Scenario('Pending',							'DOA',					null, null, null)

		};

		for (Scenario scenario : scenarios) {
			// ARRANGE
			Case cse = new Case(SOURCESTATUS__c  = scenario.status, Source_Action_CD__c = scenario.trans);

			// ACT
			Map<String, Object> result = FlowHandler.launch_RTS_Task_Fields(cse);

			// ASSERT
			String msg = '' + scenario + ' RESUT: ' +result;

			if (scenario.subject!=null) {
				System.assertEquals(scenario.subject, (String) result.get('taskSubject'), 'Subject is wrong in: ' + msg);
				System.assert(((String) result.get('taskDescription')).startsWith(scenario.description), 'Description is wrong in:' + msg);
				System.assertEquals(scenario.days, (Decimal) result.get('taskDueDateDelta'), 'Delta is wrong in:' + msg);
			} else {
				System.assertEquals(null, result.get('taskSubject'), 'Subject is wrong in: ' + msg);
			}
		}
 	}
    
    @isTest
    public static void testScenario03a() {
        
        // ARRANGE
	    UtlityTest util = new UtlityTest();
        User integrationUser = util.createTestUsr('Integration Profile', 'Integration', 'User1');
        ETLLoads__c customSetting = new ETLLoads__c(SetupOwnerId = integrationUser.Id, runDataLoads__c = true);
        insert customSetting;
	
        Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, SourceRefID__c = 'RTS-abc', SourceStatus__c = 'Closed',  Source_Action_Cd__c = 'Repair and Return', OwnerId = UserInfo.getUserId());
		insert cse;

		// ACT
	
        System.debug('--0-- start testing now-');
        System.debug('--0-- start testing now-');
        System.debug('--0-- start testing now-');
		
        Outcome_StaticVars.canRun = true;

		System.runAs(integrationUser) {
			cse.SourceStatus__c = 'Submitted';
			cse.OwnerId = integrationUser.Id;
			update cse;
		}

		// ASSERT
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
		System.assertEquals(1, tasks.size());
		System.assertEquals('Courtesy Call', tasks[0].Subject);
		System.assert(tasks[0].Description.StartsWith('MUST CALL CUSTOMER - Repair'));
    }


    @isTest
    public static void testScenario03b() {
        
        // ARRANGE

        UtlityTest util = new UtlityTest();
        User integrationUser = util.createTestUsr('Integration Profile', 'Integration', 'User1');
        ETLLoads__c customSetting = new ETLLoads__c(SetupOwnerId = integrationUser.Id, runDataLoads__c = true);
        insert customSetting;

        QueueSObject q;
        Group g;
		System.runAs(integrationUser) {
            g = new Group(Type='Queue', Name='Test Qeueue');
            insert g;
            q = new QueueSObject(SobjectType='Case', QueueId=g.Id);
            insert q;       
        }

 	
        Contact contact = new Contact(LastName = 'Test');
		insert contact;
		Case cse = new Case(ContactId = contact.Id, SourceRefID__c = 'RTS-abc', SourceStatus__c = 'Closed',  Source_Action_Cd__c = 'Repair and Return', OwnerId = g.Id);
		insert cse;

		// ACT
	
        System.debug('--0-- start testing now-');
        System.debug('--0-- start testing now-');
        System.debug('--0-- start testing now-');
		
        Outcome_StaticVars.canRun = true;

		System.runAs(integrationUser) {
			cse.SourceStatus__c = 'Submitted';
			cse.OwnerId = integrationUser.Id;
			update cse;
		}

		// ASSERT
		Case cse2 = [Select OwnerId from Case where Id = :cse.ID];
		List<Task> tasks = [Select Id, Description, Subject From Task where not Subject like 'Email%'];
		System.assertEquals(0, tasks.size());
        System.assertEquals(g.Id, cse2.OwnerId);
    }
    
    
	////////////////////////////////////////////////
	//// HELPERS
	////////////////////////////////////////////////

	private class Scenario {
		private String status;
		private String trans;
		private String subject;
		private String description;
		private Decimal days;
		public Scenario(String s, String t, String sb, String dsc, Decimal d) {
			status = s;
			trans = t;
			subject = sb;
			description = dsc;
			days = d;
		}

		//public override String toString() {
		//	return String.format('{0} - {1} - {2} - {3} - {4}', new List<String> {status, trans, subject, description, days});
		//}
	}

	private static Case cs(String rtsStatus, Contact contact) {
		return new Case(
			SOURCESTATUS__c  = rtsStatus,
			ContactId = contact.Id,
			Due_Date__c = Date.today() + 2,
			OwnerID = UserInfo.getUserId(),
			Source_Action_CD__c = null
		);
	}

	private static Date toDate(DateTime dt) {
		return Date.newInstance(dt.year(), dt.month(),dt.day());
	}

}