/*
 *  Author : Deepak Malkani.
 *  Created Date : July 8 2015
 *  Purpose : Custom Web service used to create Billing Accounts and associated contacts against a Customer Account.
 */
global class SFDCBillingAccntContCreationService_v1
{
    /*
     * Global Class describing the structure of Customer Record.
    */
    global class billingAccountRec{
        webservice String BAName;
        webservice String BAStatus;
        webservice String BAType;
        webservice String BASubType; //new attribute added
        webservice String BABrand;
        webservice String BADescription;
        webservice Integer BABillCycleCode;
        webservice Integer BABillCycleDay;
        webservice String BALangPref;
        webservice String BAPhone;
        webservice String CustomerId;
        webservice Address BAAddress; //new attribute added
        webservice keys BASrcSystemKey;
        webservice List<BAContact> BAContacts;
    }
     /*
     * Global class describing Canadian Addresses
    */
    global class Address{
        webservice CivicAddress civicAddr;
        webservice PostBoxAddress POBOXAddr;
        webservice RuralRouteAddress RRAddr;
        webservice GenDeliveryAdddress GenDelAddr;
        webservice MilitaryAddress MilitaryAddr;
        webservice USAAddress USAddr;
        webservice IntlAddress IntlAddr;
    }
    /*
     * Global class describing Canadian Civic Address
    */
    global class CivicAddress{
        webservice String careOf;
        webservice String CivicAdditonalInfo;
        webservice String CivicUnitTypeCd;
        webservice String CivicUnitNum;
        webservice String CivicNumber;
        webservice String CivicNumberSuffix;
        webservice String CivicStreetName;
        webservice String CivicStreetCd;
        webservice String CivicStreetDirectionCd;
        webservice String CivicMunicipalityName;
        webservice String CivicProvince;
        webservice String CivicPostalCd;
        webservice String CivicCountry;
    }
    /*
     * Global class describing Canadian Postal Address
    */
    global class PostBoxAddress{
        webservice String careOf;
        webservice String stationTypeCd;
        webservice String stationAreaCd;
        webservice String stationQualifier;
        webservice String AdditonalInfo;
        webservice String POBoxNum;
        webservice String MunicipalityName;
        webservice String Province;
        webservice String PostalCd;
        webservice String Country;
        
    }
    /*
     * Global class describing Rural Route Address
    */
    global class RuralRouteAddress{
        webservice String careOf;
        webservice String RuralRouteType;
        webservice String RuralRouteNum;
        webservice String stationTypeCd;
        webservice String stationAreaNum;
        webservice String stationAreaName;
        webservice String stationQualifier;
        webservice String RuralAdditionalInfo;
        webservice String RuralMuncipalityName;
        webservice String Province;
        webservice String PostalCd;
        webservice String Country;
    }
    /*
     * Global class describing General Delivery Address
    */
    global class GenDeliveryAdddress{
        webservice String careOf;
        webservice String stationTypeCd;
        webservice String stationAreaName;
        webservice String stationQualifier;
        webservice String genAdditionalInfo;
        webservice String GenDelMunicipalityName;
        webservice String GenDelProvince;
        webservice String GenDelPostalCd;
        webservice String GenDelCountry;
    }
    /*
    *   Global class describing Military Address
    */
    global class MilitaryAddress{
        webservice String careOf;
        webservice String hmcsName;
        webservice String fleetMailOfficeName;
        webservice String postOfficeBoxNumber;
        webservice String stationTypeCode;
        webservice String stationAreaName;
        webservice String stationQualifier;
        webservice String municipalityName;
        webservice String provinceCode;
        webservice String postalCode;
        webservice String additionalAddressInformation;
    }
    /*
    *   Global class describing USA Address
    */
    global class USAAddress{
        webservice String careOf;
        webservice String municipalityName;
        webservice String stateCode;
        webservice String countryCode;
        webservice String zipCode;
        webservice String additionalAddressInformation;
    }
    /*
    *   Global class describing Intl Address
    */
    global class IntlAddress{
        webservice String careOf;
        webservice String stateCode;
        webservice String countryCode;
        webservice String additionalAddressInformation;
    }
     /*
     * Global class describing the structure of contact Records for the parent Billing Account. 
    */
    global class BAContact{
        webservice String contAssociations;//new attribute added
        webservice String contFirstName;
        webservice String contLastName;
        webservice String contSalutation;
        webservice String contMobPhone;
        webservice String contPhone;
        webservice String contLanguagePref;
        webservice String contOtherPhone;
        webservice String contHomePhone;
        webservice String contEmailAddress;
        webservice String contExpirationDate;
        webservice String contStartDate;
        webservice String contAssignmentId;
        webservice String contRelnType; //new attribute added
        webservice Keys contSrcKey; //new attribute added
        
    }
     /*
     * Global class describing the structure of keys
    */
    global class Keys{
        webservice String Id;
        webservice String srcSystemName;
    }
    /*
     *  Global Class encapsulating the response from the Web service Call Out. 
    */
    global class custContResponse{
        webservice String SfdcId;
        webservice String respMessage;
        webservice String respError;
        webservice String respErrorCause;
    }

    webservice static custContResponse createBillingAccntswtContacts(List<billingAccountRec> billingAccnts){
        custContResponse response = BillingAccountContactCreation_Helper.createBillingAccnt(billingAccnts);
        return response;
    }
}