@isTest(SeeAllData=false)
private class CustomerHierarchyCtrl_Test {
  static testMethod void CHC_Test() {
    Id customerRT = (ID)Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    //Create Customer Account
    Account newCA = new Account( Name = 'testCustAccount', Phone = '2263505678', Type = 'Customer', BRAND__C = 'TELUS', SOURCEREFID__C = 'C_222222', LANGUAGE_PREFERENCE__C = 'English', Bill_Cycle_Day__c  = 12 , Status__c ='Active' ,  BillingStreet = '100 Consilium Pl', BillingCity= 'Toronto', BillingState= 'ON', BillingCountry= 'Canada',BillingPostalCode= 'M1H 3E3'  ); 
    newCA.RecordTypeId = customerRT;
    insert newCA;

    //Create Billing Accounts
    Account newBAFFH = new Account(Bill_Cycle_Day__c = 12,Status__c= 'Open', Billing_Account_Number__c = '1000001', Name = 'testBillingAccountFFH', Phone = '2263555555', Type = 'FFH', BRAND__C = 'TELUS', SOURCEREFID__C = 'B_333333', LANGUAGE_PREFERENCE__C = 'English',  BillingStreet = '200 Consilium Pl', BillingCity= 'Toronto', BillingState= 'ON', BillingCountry= 'Canada',BillingPostalCode= 'M1H 3E3' );
    newBAFFH.ParentId = newCA.Id;
    insert newBAFFH;

    Account newBAMob = new Account(Bill_Cycle_Day__c = 12, Status__c= 'Open',Billing_Account_Number__c = '1000002', Name = 'testBillingAccountMob', Phone = '2263666666', Type = 'Mobility', BRAND__C = 'TELUS', SOURCEREFID__C = 'B_444444', LANGUAGE_PREFERENCE__C = 'English',  BillingStreet = '300 Consilium Pl', BillingCity= 'Toronto', BillingState= 'ON', BillingCountry= 'Canada',BillingPostalCode= 'M1H 3E3' );
    newBAMob.ParentId = newCA.Id;
    insert newBAMob;

    //Create Contacts
    Contact newCont1 = new Contact(FirstName = 'John', LastName = 'Doer', HomePhone='6398527896' , Phone = '6398527896', MobilePhone = '6398527896' , OtherPhone='6398527896', Email = 'john.doer@somewhere.com',  Email_Address__c = 'test@community.com');
    insert newCont1; 

    Contact newCont2 = new Contact(FirstName = 'John2', LastName = 'Doer2', HomePhone='6398527555' , Phone = '6398527555', MobilePhone = '6398527555' , OtherPhone='6398527555', Email = 'john.doer2@somewhere.com',  Email_Address__c = 'test2@community.com');
    insert newCont2;

    //Create AccountContact records.
    AccountContacts__c newActCont1 = new AccountContacts__c(AccountId__c = newCA.Id, ContactId__c = newCont1.Id , SOURCEREFID__C = 'AC_123456', Role__c ='Primary', Relationship__c='Subscriber' );
    insert newActCont1;

    AccountContacts__c newActCont2 = new AccountContacts__c(AccountId__c = newCA.Id, ContactId__c = newCont2.Id, SOURCEREFID__C = 'AC_963258', Role__c ='Primary', Relationship__c='Subscriber' );
    insert newActCont2;

    //Create Cases
    Case newCase1 = new Case(AccountID = newBAFFH.Id, ContactId = newCont2.Id, Origin = 'Email', Priority = 'High', Status = 'Open', Type = 'Sign up for a new product/service', Reason_Code__c = 'Test1',
                            Line_of_Business__c = 'FFH', Subject = 'Test 1', Close_Review__c = true, Bill_Cycle__c = 14, Close_Review_Due_Date__c = system.now(), 
                            Can_Be_Reached__c = '2224443434', Outcome_Review__c = true, Follow_up__c = false, Order_Review__c = false, Pre_Appointment_Review__c = false, Order_Ticket_ID__c = '123456A');
    insert newCase1;

     Case newCase2 = new Case(ParentId=newCase1.id ,AccountID = newBAFFH.Id, ContactId = newCont2.Id, Origin = 'Email', Priority = 'High', Status = 'Open', Type = 'Sign up for a new product/service', Reason_Code__c = 'Test1',
                            Line_of_Business__c = 'FFH', Subject = 'Test 1', Close_Review__c = true, Bill_Cycle__c = 14, Close_Review_Due_Date__c = system.now(), 
                            Can_Be_Reached__c = '2224443434', Outcome_Review__c = true, Follow_up__c = false, Order_Review__c = false, Pre_Appointment_Review__c = false, Order_Ticket_ID__c='123456B');
    insert newCase2;

    Case newCase2a = [Select Id, OwnerId, CaseNumber from Case where Id = :newCase2.Id];


    
    // Instanttiate Controller
    CustomerHierarchyCtrl controller = new CustomerHierarchyCtrl();
    PageReference pageRef = Page.cpCustomer;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('ban', '1000001');
    controller = new CustomerHierarchyCtrl();

    ApexPages.currentPage().getParameters().put('caseId', newCase2.id);
    ApexPages.currentPage().getParameters().put('taskOwnerId', newCase2a.OwnerId);

    controller.getShowFeed();
    controller.getShowSearch();
    controller.getCenterPanelWidth();
    controller.getShowRightPanel();
    controller.getCustomerHierarchy();

    controller.getCaseById();
    controller.addCaseComment.CommentBody = '';  
    controller.saveCaseComment();
    controller.addCaseComment.CommentBody = 'test';  
    controller.saveCaseComment();
    controller.getCommentsByCaseId();
    controller.getCaseComUserJSON();

    

    controller.getTasksByCaseId();
    controller.getTaskJSON();
    controller.editTaskLink();

    controller.aeTask = new Task();
    controller.aeTask.Description ='';
    controller.aeTask.Priority  ='';
    controller.aeTask.ActivityDate  = system.today() - 2;
    controller.saveTask();

    controller.aeTask = new Task();
    controller.aeTask.Description ='';
    controller.aeTask.Priority  =null;
    controller.aeTask.ActivityDate  = null;
    controller.saveTask();


    controller.aeTask.Description ='test';
    controller.aeTask.Priority  ='Low';
    controller.aeTask.ActivityDate  = system.today();
    controller.saveTask();


    ApexPages.currentPage().getParameters().put('caseId', '');
    ApexPages.currentPage().getParameters().put('search', '');
    ApexPages.currentPage().getParameters().put('ban', '');
    ApexPages.currentPage().getParameters().put('can', '222222');
    controller = new CustomerHierarchyCtrl();


    ApexPages.currentPage().getParameters().put('ban', '');
    ApexPages.currentPage().getParameters().put('can', '');
    ApexPages.currentPage().getParameters().put('casenum', newCase2a.CaseNumber);
    controller = new CustomerHierarchyCtrl();

    ApexPages.currentPage().getParameters().put('ban', '');
    ApexPages.currentPage().getParameters().put('can', '');
    ApexPages.currentPage().getParameters().put('casenum', '');
    ApexPages.currentPage().getParameters().put('tid', '123456A');
    controller = new CustomerHierarchyCtrl();

    ApexPages.currentPage().getParameters().put('caseId', '');
    ApexPages.currentPage().getParameters().put('ban', '');
    ApexPages.currentPage().getParameters().put('can', '');
    ApexPages.currentPage().getParameters().put('casenum', '');
    ApexPages.currentPage().getParameters().put('tid', '');
    ApexPages.currentPage().getParameters().put('phonenum', '6398527555');
    controller = new CustomerHierarchyCtrl();
    controller.getTotalSearchCases();

    ApexPages.currentPage().getParameters().put('caseId', '');
    ApexPages.currentPage().getParameters().put('ban', '');
    ApexPages.currentPage().getParameters().put('can', '');
    ApexPages.currentPage().getParameters().put('casenum', '');
    ApexPages.currentPage().getParameters().put('tid', '');
    ApexPages.currentPage().getParameters().put('phonenum', '');
    controller = new CustomerHierarchyCtrl();
    controller.getsrcMsgString();


    ApexPages.currentPage().getParameters().put('search', 'true');
    controller.caseNumber = newCase2a.CaseNumber;
    controller.srchCase();

    controller.billingAccntNo = '1000002';
    controller.srchCase();
    controller.billingAccntNo = '';
    controller.phoneNumber = '6398527555';
    controller.srchCase();

    controller.billingAccntNo = '';
    controller.phoneNumber = '';    
    controller.ticketOrderId = '123456B';
    controller.srchCase();
    
    controller.billingAccntNo = '';
    controller.phoneNumber = '';    
    controller.ticketOrderId = '';
    controller.caseNumber = newCase2a.CaseNumber;
    controller.srchCase();

  

  }  
}