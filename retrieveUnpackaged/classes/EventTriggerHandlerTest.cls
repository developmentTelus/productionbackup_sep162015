/*
	Author : Deepak Malkani.
	Created Date : Feb 12 2015
	Purpose : Provides Unit Testing for all Business Logic covered under TaskTriggerHandler class
*/

@isTest(SeeAllData=false)
public class EventTriggerHandlerTest
{
	//Test method to update a single Activity Record
	public static testMethod void UnitTest1()
	{
		//Initialise handlers
		UtlityTest testHandler = new UtlityTest();
		List<Task_and_Event_History__c> tskevtHistList = new List<Task_and_Event_History__c>();
		Event evtObj;
		Event evtObjUpd;
		User u;
		//Start Testing
		Test.startTest();
		u = testHandler.createTestUsr();
		system.runAs(u)
		{
			evtObj = testHandler.createEventRec();
		}
		system.runAs(new User(Id = UserInfo.getUserId()))
		{
			evtObjUpd = [SELECT id, Type, ActivityDate, Activity_Date__c, OwnerId FROM Event LIMIT 1];
			evtObjUpd.Type = 'Email';
			evtObjUpd.Activity_Date__c = String.valueOf(system.today()+10);
			evtObjUpd.ActivityDate = system.today() + 10;
			evtObjUpd.OwnerId = u.Id;
			update evtObjUpd;
			//Make Assertions
			tskevtHistList = [SELECT id, Action__c FROM Task_and_Event_History__c];
			system.assertEquals(4, tskevtHistList.size());
		}
		Test.stopTest();
	}
	//Test method used for Bulkifying, it updates 200 Activity Records.
	public static testMethod void UnitTest2()
	{
		//Initialise handlers
		UtlityTest testHandler = new UtlityTest();
		List<Task_and_Event_History__c> tskevtHistList = new List<Task_and_Event_History__c>();
		List<Event> evtList = new List<Event>();
		List<Event> evtUpdList = new List<Event>();

		//Start Testing
		Test.startTest();
		evtList = testHandler.createMultiEvtRec(200);
		for(Integer j=0; j< evtList.size(); j++)
		{
			Event evtObj = new Event(Id = evtList[j].Id);
			evtObj.Type = 'Email';
			evtUpdList.add(evtObj);
		}
		if(!evtUpdList.isEmpty())
			update evtUpdList;
		Test.stopTest();
		//Make Assertions
		tskevtHistList = [SELECT id, Action__c FROM Task_and_Event_History__c];
		system.assertEquals(600, tskevtHistList.size());
	}
}