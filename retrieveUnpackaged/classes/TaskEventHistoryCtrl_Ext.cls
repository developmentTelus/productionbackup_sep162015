/*
    Author : Deepak Malkani.
    Created Date : Feb 15 2015
    Purpose : The purpose of this VF Page controller is to process Business Logic related to Task and Event History
*/

public with sharing class TaskEventHistoryCtrl_Ext {

    public Task tskObj {get; set;}
    public transient List<Task_and_Event_History__c> tskEventHistoryList {get; set;}
    
    public TaskEventHistoryCtrl_Ext(ApexPages.StandardController controller) {
        tskObj = (Task) controller.getRecord();
    }
    
    public List<Task_and_Event_History__c> getTaskHistory()
    {
        tskEventHistoryList = [SELECT id, Action__c, Date__c, TaskorEventId__c, User__c FROM Task_and_Event_History__c WHERE TaskorEventId__c = : tskObj.Id ORDER BY Date__c DESC];
        return tskEventHistoryList;
    }
    
    
}