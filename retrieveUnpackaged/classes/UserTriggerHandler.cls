/*
*	Author : Deepak Malkani.
*	Created Date : 11/06/2015
*	Purpose : Common Handler class used for all Business Logic Processing for User Object
*/
public with sharing class UserTriggerHandler {
	//Default Constructor for Handler Class
	public void UserTriggerHander(){}
	
	public void validateuserSenderName(List<User> usrList){
		
		for(User users : usrList)
		{
			if(users.SenderName != null || users.SenderName != '')
				users.SenderName = null;
		}
		
	}
}