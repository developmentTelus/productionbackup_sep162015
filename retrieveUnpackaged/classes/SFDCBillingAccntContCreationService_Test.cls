/*
	Author : Deepak Malkani.
	Modified by : Aneeq Hashmi
	Created Date : 22/06/2015
	Purpose : Test class to test the Billing Account and Contact web service call out.
*/
@isTest(SeeAllData=false)
private class SFDCBillingAccntContCreationService_Test
{
	static SFDCBillingAccntContCreationService_v1.billingAccountRec prepareBillingAccntData()
	{
		SFDCBillingAccntContCreationService_v1.billingAccountRec baRec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		baRec.BAName = 'Test Billing Account 1';
		baRec.BAStatus = 'Active';
		baRec.BABrand = 'Telus';
		baRec.BABillCycleCode = 24;
		baRec.BABillCycleDay = 12;
		baRec.BALangPref = 'EN';
		baRec.CustomerId = '123456';
		return baRec;
	}
	static Account prepareCustomerAccnt()
	{
		Account custAccnt = new Account();
		custAccnt.SourceRefID__c = 'C_123456';
		custAccnt.Type = 'Customer';
		custAccnt.Name = 'Test Customer Accnt';
		custAccnt.Brand__c = 'Telus';
		insert custAccnt;
		return custAccnt;
	}
	static Account prepareGenericAccnt()
	{
		Account custAccnt = new Account();
		custAccnt.SourceRefID__c = 'B_123456';
		custAccnt.Type = 'FFH';
		custAccnt.Name = 'Test FFH Accnt';
		custAccnt.Brand__c = 'Telus';
		insert custAccnt;
		return custAccnt;
	}
	//Test Method used to make Web Service call out : Billing Account and Contact Creation
	@isTest
	static void UnitTest_1_noCustomerAccnt_for_BillAccnt()
	{
		//Create Billing Account Data but it should not have a Customer Account.
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		baRecList.add(SFDCBillingAccntContCreationService_Test.prepareBillingAccntData());
		Test.startTest();
		//Make Call out to the webservice 
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		//performing assertions after making call out
		system.assertEquals(true, response.respMessage.contains('Customer Account is not found'));
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_2_BillingAccntwtCustAccnt_Errors(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		prepareCustomerAccnt();
		baRecList.add(SFDCBillingAccntContCreationService_Test.prepareBillingAccntData());
		Test.startTest();
		//Make Call out to the webservice 
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		//performing assertions after making call out
		system.assertEquals(true, response.respMessage.contains('SourceRefID__c not specified'));
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_3_BillingAccntwtCustAccnt_NoErrors(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'KB';
		BARec.BASrcSystemKey = BAKeys;
		baRecList.add(BARec);
		Test.startTest();
		//Make Call out to the webservice 
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		//performing assertions after making call out
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'KB_55656566'].id, response.SfdcId);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_4_BillingAccntwtCivicAddr(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.CivicAddress CivAddress = new SFDCBillingAccntContCreationService_v1.CivicAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		CivAddress.CivicCountry = 'Canada';
		CivAddress.CivicMunicipalityName = 'VAN';
		CivAddress.CivicNumber = '123';
		CivAddress.CivicNumberSuffix = 'A';
		CivAddress.CivicPostalCd = 'LLL999L';
		CivAddress.CivicStreetCd = 'Ave';
		CivAddress.CivicAdditonalInfo = 'Near Dundas Corner';
		addr.civicAddr = CivAddress;
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		baRecList.add(BARec);
		Test.startTest();
		//Make Call out to the webservice 
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		//performing assertions after making call out
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals('Canada', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('123 A Near Dundas Corner Ave', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_5_BillingAccntwtPOAddr(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.PostBoxAddress POBoxAddress = new SFDCBillingAccntContCreationService_v1.PostBoxAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		POBoxAddress.careOf = 'Michael';
		POBoxAddress.Country = 'Canada';
		POBoxAddress.MunicipalityName = 'van';
		POBoxAddress.POBoxNum = '1234';
		POBoxAddress.PostalCd = 'L9L9L9L9';
		POBoxAddress.Province = 'Ontario';
		POBoxAddress.stationAreaCd = '005';
		addr.POBOXAddr = POBoxAddress;
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		baRecList.add(BARec);
		Test.startTest();
		//Make Call out to the webservice 
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		//performing assertions after making call out
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals('Canada', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('Michael 1234 005', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_6_BillingAccntwtRRAddr(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.RuralRouteAddress RRAddress = new SFDCBillingAccntContCreationService_v1.RuralRouteAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		RRAddress.careOf = 'Michael';
		RRAddress.RuralRouteNum = '123';
		RRAddress.RuralRouteType = 'RR';
		RRAddress.stationAreaName = 'Station Area 1';
		RRAddress.stationQualifier = 'Stn';
		RRAddress.Country = 'Canada';
		RRAddress.RuralMuncipalityName = 'van';
		RRAddress.PostalCd = 'L9L9L9L9';
		RRAddress.Province = 'Ontario';
		addr.RRAddr = RRAddress;
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		baRecList.add(BARec);
		Test.startTest();
		//Make Call out to the webservice 
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		//performing assertions after making call out
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals('Canada', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('Michael RR 123 Station Area 1 Stn', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_7_BillingAccntwtGenDelAddr(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.GenDeliveryAdddress GenDelAddress = new SFDCBillingAccntContCreationService_v1.GenDeliveryAdddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		GenDelAddress.careOf = 'Michael';
		GenDelAddress.stationAreaName = 'Station Area A';
		GenDelAddress.stationTypeCd = 'Stn';
		GenDelAddress.GenDelCountry = 'Canada';
		GenDelAddress.GenDelMunicipalityName = 'van';
		GenDelAddress.GenDelPostalCd = 'L9L9L9L9';
		GenDelAddress.GenDelProvince = 'Ontario';
		addr.GenDelAddr = GenDelAddress;
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		baRecList.add(BARec);
		Test.startTest();
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals('Canada', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('Michael Stn Station Area A', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_8_BillingAccntwtMilAddr(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.MilitaryAddress milAddress = new SFDCBillingAccntContCreationService_v1.MilitaryAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		milAddress.careOf = 'Mike';
		milAddress.hmcsName = 'Base Camp A';
		milAddress.fleetMailOfficeName = 'Office A';
		milAddress.stationAreaName = 'Station Area A';
		milAddress.stationQualifier = 'Qual';
		milAddress.stationTypeCode = 'Area A';
		milAddress.additionalAddressInformation = 'Next to Base Camp';
		milAddress.municipalityName = 'VAN';
		milAddress.provinceCode = 'ON';
		milAddress.postalCode = 'L9L9L9L';

		addr.MilitaryAddr = milAddress;
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		baRecList.add(BARec);
		Test.startTest();
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals(null, [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('Mike Base Camp A Office A Area A Station Area A Qual Next to Base Camp', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_9_BillingAccntwtUSAddr(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.USAAddress usaAddress = new SFDCBillingAccntContCreationService_v1.USAAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		usaAddress.careOf = 'Mike';
		usaAddress.countryCode = 'Can';
		usaAddress.municipalityName = 'VAN';
		usaAddress.stateCode = 'ON';
		usaAddress.zipCode = 'L9L9L9L';
		usaAddress.additionalAddressInformation = 'next to cover dr';
		addr.USAddr = usaAddress;
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		baRecList.add(BARec);
		Test.startTest();
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals('Can', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('Mike next to cover dr', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_10_BillingAccntwtIntlAddr(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.IntlAddress intlAddress = new SFDCBillingAccntContCreationService_v1.IntlAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		IntlAddress.careOf = 'Mike';
		IntlAddress.countryCode = 'Can';
		IntlAddress.stateCode = 'ON';
		IntlAddress.additionalAddressInformation = 'next to cover dr';
		addr.IntlAddr = IntlAddress;
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		baRecList.add(BARec);
		Test.startTest();
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals('Can', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('Mike next to cover dr', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
	}

	//Test method will create a valid customer account and then send billing account
	@isTest static void UnitTest_11_BillingAccntwtContact_NoError(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.IntlAddress intlAddress = new SFDCBillingAccntContCreationService_v1.IntlAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		SFDCBillingAccntContCreationService_v1.BAContact BAcont = new SFDCBillingAccntContCreationService_v1.BAContact();
		SFDCBillingAccntContCreationService_v1.Keys BCKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		List<SFDCBillingAccntContCreationService_v1.BAContact> BAcontList = new List<SFDCBillingAccntContCreationService_v1.BAContact>();
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		IntlAddress.careOf = 'Mike';
		IntlAddress.countryCode = 'Can';
		IntlAddress.stateCode = 'ON';
		IntlAddress.additionalAddressInformation = 'next to cover dr';
		addr.IntlAddr = IntlAddress;
		BAcont.contFirstName = 'Test1';
		//BAcont.contLastName = 'Test2';
		BAcont.contLastName = '';
		BAcont.contEmailAddress = 'test@test.com';
		BCKeys.Id = '223457';
		//BCKeys.srcSystemName = 'Enabler';
		BCKeys.srcSystemName = '130';
		BAcont.contSrcKey = BCKeys;
		BAcontList.add(BAcont);
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		BARec.BAContacts = BAcontList;
		baRecList.add(BARec);
		Test.startTest();
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals('Billing Account and Contact successfully upserted', response.respMessage);
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].id, response.SfdcId);
		system.assertEquals('Can', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingCountry);
		system.assertEquals('Mike next to cover dr', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'Enabler_55656566'].BillingStreet);
		//system.assertEquals('Enabler_223457', [SELECT id, SourceRefID__c, AccountRefId__c FROM Contact WHERE AccountRefId__c ='123456'].SourceRefId__c);
		system.assertEquals('KB_223457', [SELECT id, SourceRefID__c, AccountRefId__c FROM Contact WHERE AccountRefId__c ='123456'].SourceRefId__c);
	    system.assertEquals('NONE', [SELECT id, LastName FROM Contact WHERE AccountRefId__c ='123456'].LastName);
	}

	//Test method will create a valid customer account and then send billing account
	/*@isTest static void UnitTest_12_BillingAccntwtContact_Errors(){		
		List<SFDCBillingAccntContCreationService_v1.billingAccountRec> baRecList = new List<SFDCBillingAccntContCreationService_v1.billingAccountRec>();
		SFDCBillingAccntContCreationService_v1.billingAccountRec BARec = new SFDCBillingAccntContCreationService_v1.billingAccountRec();
		SFDCBillingAccntContCreationService_v1.IntlAddress intlAddress = new SFDCBillingAccntContCreationService_v1.IntlAddress();
		SFDCBillingAccntContCreationService_v1.Keys BAKeys = new SFDCBillingAccntContCreationService_v1.Keys();
		SFDCBillingAccntContCreationService_v1.Address addr = new SFDCBillingAccntContCreationService_v1.Address();
		SFDCBillingAccntContCreationService_v1.BAContact BAcont = new SFDCBillingAccntContCreationService_v1.BAContact();
		List<SFDCBillingAccntContCreationService_v1.BAContact> BAcontList = new List<SFDCBillingAccntContCreationService_v1.BAContact>();
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		prepareCustomerAccnt();
		BARec = SFDCBillingAccntContCreationService_Test.prepareBillingAccntData();
		IntlAddress.careOf = 'Mike';
		IntlAddress.countryCode = 'Can';
		IntlAddress.stateCode = 'ON';
		IntlAddress.additionalAddressInformation = 'next to cover dr';
		addr.IntlAddr = IntlAddress;
		BAcont.contFirstName = 'Test1';
		BAcont.contLastName = 'Test2';
		BAcont.contEmailAddress = 'test@test.com';
		BAcontList.add(BAcont);
		BAKeys.Id = '55656566';
		BAKeys.srcSystemName = 'Enabler';
		BARec.BASrcSystemKey = BAKeys;
		BARec.BAAddress = addr;
		BARec.BAContacts = BAcontList;
		baRecList.add(BARec);
		Test.startTest();
		SFDCBillingAccntContCreationService_v1.custContResponse response = SFDCBillingAccntContCreationService_v1.createBillingAccntswtContacts(baRecList);
		Test.stopTest();
		system.assertEquals(true, response.respMessage.contains('SourceRefID__c not specified'));
	}*/

}