/*
	Author : Deepak Malkani.
	Created Date : Feb 11 2015
	Purpose : A common Utility class/handler which helps creating test data for all test classes in the SFDC Org.
	MODIFIED : 31 Jul 2015
	Aneeq Hashmi Added createMultiAccntCnt(Integer i) method to create multiple AccountContacts__c objects
*/
@isTest(SeeAllData=false)
public with sharing class UtlityTest {

	public UtlityTest() {
	}

    //Aneeq added
    public List<Contact> createMultiContactSrcSysName(Integer i){
   
   List<Contact> contRefIDList = new List<Contact>(); 	
   
    for(Integer n=0; n<i; n++)
       {
            Contact c1 = new Contact(
            			FirstName = 'Test1'+n,
						LastName = 'LastTest1',
						LanguagePreference__c = 'English',
						Preferred_Contact_Method__c = 'Email',
						SourceRefID__c='130_123456'+n);
        				contRefIDList.add(c1);
       }  	
    	if(!contRefIDList.isEmpty())
			insert contRefIDList;
		return contRefIDList;	 
    }
    
    
    //Aneeq Added
    public List<AccountContacts__c> createMultiAccntCnt(Integer i){		
    List<Account> accntLst = new List<Account>();
	List<Contact> contList = new List<Contact>();
	
    List<AccountContacts__c> accntCntList = new List<AccountContacts__c>();
       
       for(Integer j=0; j<i; j++)
		{
			Account a = new Account(
						Name = 'TestAccnt1'+j,
						Type = 'Mobility',
						Rating = 'Warm',
						Bill_Cycle_Day__c = 12,
						Language_Preference__c = 'English',
						Billing_Account_Number__c = '1234567'+j,
						SourceRefId__c = 'B_1234567'+j,
						SourceRefId_Backup__c = null);
						accntLst.add(a);
        }
       insert accntLst;
       
       for(Integer k=0; k<i; k++)
       {
            Contact c = new Contact(
            			FirstName = 'Test1'+k,
						LastName = 'LastTest1',
						LanguagePreference__c = 'English',
						Preferred_Contact_Method__c = 'Email',
						SourceRefID__c='ref1234567'+k);
        				contList.add(c);
       }
       insert contList; 	
            
       for(Integer m=0; m<i; m++){
       AccountContacts__c d = new AccountContacts__c(
            			AccountId__c = accntLst.get(m).Id,
                		ContactId__c = contList.get(m).Id,
           				AccountId__r = accntLst.get(m),
           				ContactId__r = contList.get(m),
                		SourceRefId__c = 'CA_1234'+m);
           				accntCntList.add(d);
       }
       
		if(!accntCntList.isEmpty())
			insert accntCntList;
		return accntCntList;
	}
    
    
	// create a single Task Record
	public Task createTaskRec()
	{
		Task tkObj = new Task(
			Subject = 'Courtesy Call',
			OwnerId = UserInfo.getUserId(),
			Status = 'Not Started',
			ActivityDate = system.today(),
			Priority = 'Medium',
			Activity_Date__c = String.valueOf(system.today())
		);

		insert tkObj;
		return tkObj;
	}

	// create Multiple Task Records
	public List<Task> createMultiTaskRec(Integer i)
	{
		List<Task> tskList = new List<Task>();
		for(Integer j=0; j<i;j++)
		{
			tskList.add(
				new Task(
					Subject = 'Courtesy Call',
					OwnerId = UserInfo.getUserId(),
					Status = 'Not Started',
					ActivityDate = system.today(),
					Priority = 'Medium',
					Activity_Date__c = String.valueOf(system.today())
				)
			);
		}

		if(!tskList.isEmpty())
			insert tskList;

		return tskList;
	}

	// create a close case reminder task
	public Task createClsCaseRemindTaskRec(ID ObjectId)
	{
		return new Task(
			Subject = 'Close Case Reminder',
			OwnerId = UserInfo.getUserId(),
			Status = 'Not Started',
			ActivityDate = system.today(),
			Priority = 'Low',
			Activity_Date__c = String.valueOf(system.today()),
			WhatId = ObjectId
		);
	}


	// create a Single Test User
	public User createTestUsr()
	{
		return createTestUsr('System Administrator', 'TestFirst', 'TestLast');
	}


	// create a Single Test User
	public User createTestUsr(String profileName, String first, String last)
	{
		Profile p = [
			SELECT
				id, Name
			FROM
				Profile
			WHERE
				Name =: profileName
			LIMIT 1
		];

		User u = new User(
			CompanyName = 'TestComp',
			FirstName = first,
			LastName = last,
			LocaleSidKey = 'en_CA',
			UserName = 'testfirst.last@test.org',
			TimeZoneSidKey = 'America/Indiana/Indianapolis',
			Email = 'testfrt.last@test.org',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			alias = 'test5',
			ProfileId = p.Id
		);

		insert u;
		return u;
	}

	// create a Single Event Record
	public Event createEventRec()
	{
		Event evtObj = new Event(
			Subject = 'Pre-Appointment Review',
			OwnerId = UserInfo.getUserId(),
			DurationInMinutes = 10,
			ActivityDate = null,
			ActivityDateTime = system.now(),
			Type = 'Other',
			Activity_Date__c = String.valueOf(system.today())
		);

		insert evtObj;
		return evtObj;
	}

	// create Multiple Event Records
	public List<Event> createMultiEvtRec(Integer i)
	{
		List<Event> evtList = new List<Event>();

		for(Integer j=0; j< i; j++)
		{
			evtList.add(
				new Event(
					Subject = 'Pre-Appointment Review',
					OwnerId = UserInfo.getUserId(),
					ActivityDateTime = system.now(),
					DurationInMinutes = 10,
					Type = 'Other',
					Activity_Date__c = String.valueOf(system.today())
				)
			);
		}

		if(!evtList.isEmpty())
			insert evtList;

		return evtList;
	}

	public Contact createContactRec()
	{
		Contact c = new Contact(
			FirstName = 'Test1',
			LastName = 'LastTest1',
			LanguagePreference__c = 'English',
			Preferred_Contact_Method__c = 'Email'
		);

		insert c;
		return c;
	}

	public Contact createContactRecWithSourceRef()
	{
		Contact c = new Contact(
			FirstName = 'Test1',
			LastName = 'LastTest1',
			LanguagePreference__c = 'English',
			Preferred_Contact_Method__c = 'Email',
			SourceRefID__c='ref123456789'
		);

		insert c;
		return c;
	}

	public Case createCaseRec(Contact c)
	{

		System.debug('starting createCaseRec');

		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = false,
			Order_Review__c = false,
			Pre_Appointment_Review__c = false,
			SourceRefID__c = 'xxxxxxxxxxxxxxx',
			Source_Type__c = null
		);

		insert cs;

		System.debug('after inserting in createCaseRec');

		return cs;
	}
	//Deepak Malkani : Method used to create contact with a defined owner
	public Case createCaseRec(Contact c, User u)
	{
		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(), 
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Outcome_Review_Due_Date__c = system.now(),
			Follow_up__c = false,
			Order_Review__c = false,
			Pre_Appointment_Review__c = false,
			SourceRefID__c = 'xxxxxxxxxxxxxxx',
			Source_Type__c = null,
			OwnerId = u.Id
		);

		insert cs;
		return cs;
	}

	//Deepak Malkani : Method used to create contact with a defined owner
	public List<Case> createMultiCaseRec(Contact c, User u, Integer j)
	{
		List<Case> caseList = new List<Case>();
		for(Integer i=0; i<j ;i++)
		{
			Case cs = new Case(
				ContactId = c.Id,
				Origin = 'Email',
				Priority = 'High',
				Status = 'Open',
				Type = 'Sign up for a new product/service',
				Reason_Code__c = 'Test1',
				Line_of_Business__c = 'FFH',
				Subject = 'Test 1'+i,
				Bill_Cycle__c = 14,
				Close_Review__c = true,
				Close_Review_Due_Date__c = system.now(), 
				Can_Be_Reached__c = '2224443434',
				Outcome_Review__c = true,
				Outcome_Review_Due_Date__c = system.now(),
				Follow_up__c = false,
				Order_Review__c = false,
				Pre_Appointment_Review__c = false,
				SourceRefID__c = 'xxxxxxxxxxxxxxx'+i,
				Source_Type__c = null,
				OwnerId = u.Id
			);
			caseList.add(cs);
		}
		if(!caseList.isEmpty())
			insert caseList;
		return caseList;
	}

	public List<Case> createopenCaseswtNoTasks(Integer i)
	{
		List<Case> caseInsList = new List<Case>();
		for(Integer j=0; j< i; j++)
		{
			caseInsList.add(
				new Case(
					Origin = 'Email',
					Priority = 'High',
					Status = 'Open',
					Type = 'Sign up for a new product/service',
					Reason_Code__c = 'Test1',
					Line_of_Business__c = 'FFH',
					Subject = 'Test '+j,
					Close_Review__c = false,
					Bill_Cycle__c = 14,
					Close_Review_Due_Date__c = system.now(),
					Can_Be_Reached__c = '2224443434',
					Outcome_Review__c = false,
					Follow_up__c = false,
					Order_Review__c = false,
					Pre_Appointment_Review__c = false
				)
			);
		}

		if(!caseInsList.isEmpty()) insert caseInsList;

		return caseInsList;
	}

	public Case createCaseRecwtOrderReview(Contact c)
	{
		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = false,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = false,
			Source_Type__c = null,
			SourceRefID__c = 'xxxxxxxxxxxxxxx'
		);
		insert cs;
		return cs;
	}

	public Case createCasewithGrp(Contact c)
	{
		Case cs = new Case(
			ContactId = c.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = 'FFH',
			Subject = 'Test 1',
			Close_Review__c = true,
			Bill_Cycle__c = 14,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = false,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = false,
			OwnerId = this.mobilityENGroup().Id
		);

		insert cs;
		return cs;
	}

	public Account createAccntRec()
	{
		Account a = new Account(
			Name = 'TestAccnt1',
			Type = 'Mobility',
			Rating = 'Warm',
			Bill_Cycle_Day__c = 12,
			Language_Preference__c = 'English'
		);
		insert a;
		return a;
	}
	
	public List<Account> createMultiMobAccntRec(Integer i){
		List<Account> accntLst = new List<Account>();
		for(Integer j=0; j<i; j++)
		{
			Account a = new Account(
						Name = 'TestAccnt1'+j,
						Type = 'Mobility',
						Rating = 'Warm',
						Bill_Cycle_Day__c = 12,
						Language_Preference__c = 'English',
						Billing_Account_Number__c = '1234567'+j,
						SourceRefId__c = 'B_1234567'+j,
						SourceRefId_Backup__c = null);
			accntLst.add(a);
		}
		if(!accntLst.isEmpty())
			insert accntLst;
		return accntLst;
	}

	public List<Account> createMultiWiredAccntRec(Integer i){
		List<Account> accntLst = new List<Account>();
		for(Integer j=0; j<i; j++)
		{
			Account a = new Account(
						Name = 'TestAccnt1'+j,
						Type = 'FFH',
						Rating = 'Warm',
						Bill_Cycle_Day__c = 12,
						Language_Preference__c = 'English',
						Billing_Account_Number__c = '3342311'+j,
						SourceRefId__c = 'B_3342311'+j);
			accntLst.add(a);
		}
		if(!accntLst.isEmpty())
			insert accntLst;
		return accntLst;
	}
	
	public Case createCasewtAcnt(Account a)
	{
		Case cs = new Case(
			AccountId = a.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = '',
			Subject = 'Test 1',
			Close_Review__c = true,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = true,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = false,
			Language_Preference__c = ''
		);

		insert cs;
		return cs;
	}

	public Case createCasewtAcntAllTasks(Account a)
	{
		Case cs = new Case(
			AccountId = a.Id,
			Origin = 'Email',
			Priority = 'High',
			Status = 'Open',
			Type = 'Sign up for a new product/service',
			Reason_Code__c = 'Test1',
			Line_of_Business__c = '',
			Subject = 'Test 1',
			Close_Review__c = true,
			Close_Review_Due_Date__c = system.now(),
			Can_Be_Reached__c = '2224443434',
			Outcome_Review__c = true,
			Follow_up__c = true,
			Order_Review__c = true,
			Order_Review_Due_Date__c = system.today(),
			Pre_Appointment_Review__c = true,
			Language_Preference__c = ''
		);

		insert cs;
		return cs;
	}

	public List<Case> createMultiCaseRec(Integer i, Contact c)
	{
		List<Case> caseList = new List<Case>();
		for(Integer j=0; j< i; j++)
		{
			caseList.add(
				new Case(
					ContactId = c.Id,
					Origin = 'Email',
					Priority = 'High',
					Status = 'Open',
					Type = 'Sign up for a new product/service',
					Reason_Code__c = 'Test1',
					Line_of_Business__c = 'FFH',
					Subject = 'Testing'+i,
					Close_Review__c = false,
					Bill_Cycle__c = 14,
					Close_Review_Due_Date__c = null,
					Can_Be_Reached__c = '2224443434',
					Outcome_Review__c = true,
					Follow_up__c = false,
					Order_Review__c = true,
					Order_Review_Due_Date__c = system.today(),
					Pre_Appointment_Review__c = true
				)
			);
		}
		return caseList;
	}

	public ETLLoads__c setCustomSettings()
	{
		ETLLoads__c CS = new ETLLoads__c();
		CS.runDataLoads__c = true;
		insert CS;
		return CS;
	}

	public API_Loads__c setAPILoadsCS()
	{
		API_Loads__c CS = new API_Loads__c();
		CS.Invoke_Contact_Trigger__c = true;
		CS.Activate_Conditional_Required_fields__c = true;
		insert CS;
		return CS;
	}

	//Deepak Malkani : Removed from test handler and Added shared methods into Utility Class.
	public List<Task> getAllTasks(Case cse) {
		return [SELECT id, Status, Subject FROM Task WHERE WhatId = :cse.Id];
	}

	public List<Task> getAllTasks(Case cse, String status) {
		return [SELECT id, Status, Subject FROM Task WHERE WhatId = :cse.Id and status = :status];
	}

	public List<Task> filterStatusSubject(List<Task> tasks, String status, String Subject) {
		List<Task> filtered = new List<Task>();
		for (Task t : tasks) {
			if (t.Status == status && t.Subject == subject)
				filtered.add(t);
		}
		return filtered;
	}

	//////////////////////////////////////////////////
	/// Private Methods
	//////////////////////////////////////////////////

	private Group mobilityENGroup() {
		return [SELECT id FROM Group WHERE Name = : 'Mobility - EN' LIMIT 1];
	}

}