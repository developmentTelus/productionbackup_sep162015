/*
	Author : Deepak Malkani.
	Created Date : 25/06/2015
	Purpose : Test class written to test call outs for SFDC Customer Account Contact Webservice.
*/
@isTest(SeeAllData=false)
private class SFDCCustomerAccntContCreateService_Test
{
	@isTest
	static void UnitTest_1_createCustAccnt_Error()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals('Could not Insert/Update Customer Account', response.respMessage);
		system.assertEquals(true, response.respError.contains('SourceRefID__c not specified'));
		system.assertEquals(null, response.SfdcId);
	}

	@isTest
	static void UnitTest_2_createCustAccnt_NoError()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '12345678';
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefId__c = 'C_12345678'].id, response.SfdcId);
	}

	@isTest
	static void UnitTest_3_createCustAccnt_wtContact()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		List<SFDCCustomerAccntContCreationService_v1.custContact> contList = new List<SFDCCustomerAccntContCreationService_v1.custContact>();
		SFDCCustomerAccntContCreationService_v1.custContact contRec = new SFDCCustomerAccntContCreationService_v1.custContact();
		SFDCCustomerAccntContCreationService_v1.Keys key = new SFDCCustomerAccntContCreationService_v1.Keys();
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '12345678';
		contRec.contFirstName = 'Test1';
		contRec.contLastName = 'TestLast1';
		contRec.contLanguagePref = 'EN';
		contRec.contRelnType = 'Owner';
		contRec.contEmailAddress = 'test1@test1.com';
		key.Id = '2232323';
		key.srcSystemName = 'CODS';
		contRec.contSrcKey = key;
		contList.add(contRec);
		custRec.CustContacts = contList;
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefId__c = 'C_12345678'].id, response.SfdcId);
	}

	@isTest
	static void UnitTest_4_createCustAccnt_wtContact_Error()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		List<SFDCCustomerAccntContCreationService_v1.custContact> contList = new List<SFDCCustomerAccntContCreationService_v1.custContact>();
		SFDCCustomerAccntContCreationService_v1.custContact contRec = new SFDCCustomerAccntContCreationService_v1.custContact();
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '12345678';
		contRec.contFirstName = 'Test1';
		contRec.contLastName = 'TestLast1';
		contRec.contLanguagePref = 'EN';
		contRec.contRelnType = 'Owner';
		contRec.contEmailAddress = 'test1@test1.com';
		contRec.contSrcKey = null; //no key is specified, so that contact upsert fails.
		contList.add(contRec);
		custRec.CustContacts = contList;
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(true, response.respError.contains('SourceRefID__c not specified'));
		system.assertEquals(true, response.respMessage.contains('Could not Insert'));
		system.assertEquals(null, response.SfdcId);
	}

	/*@isTest
	static void UnitTest_5_createCustAccnt_wtContact_Error()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		List<SFDCCustomerAccntContCreationService_v1.custContact> contList = new List<SFDCCustomerAccntContCreationService_v1.custContact>();
		SFDCCustomerAccntContCreationService_v1.Keys key = new SFDCCustomerAccntContCreationService_v1.Keys();
		SFDCCustomerAccntContCreationService_v1.custContact contRec = new SFDCCustomerAccntContCreationService_v1.custContact();
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '12345678';
		contRec.contFirstName = 'Test1';
		contRec.contLastName = null;//last name is deliberately set to null to throw DML Exception.
		contRec.contLanguagePref = 'EN';
		contRec.contRelnType = 'Owner';
		contRec.contEmailAddress = 'test1@test1.com';
		key.Id = '2232323';
		key.srcSystemName = 'CODS';
		contRec.contSrcKey = key;
		contList.add(contRec);
		custRec.CustContacts = contList;
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(true, response.respError.contains('Required fields are missing'));
		system.assertEquals(true, response.respMessage.contains('Could not Insert'));
		system.assertEquals(null, response.SfdcId);
	}*/

	@isTest
	static void UnitTest_6_createCustAccnt_wtnoContact_Error()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = null;
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		//Adding API Data Load Custom Setting.
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(true, response.respError.contains('SourceRefID__c not specified'));
		system.assertEquals(true, response.respMessage.contains('Could not Insert'));
		system.assertEquals(null, response.SfdcId);
	}

	@isTest
	static void UnitTest_7_createCustAccnt_wt_CivAddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.CivicAddress civAddr = new SFDCCustomerAccntContCreationService_v1.CivicAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		civAddr.CivicCountry = 'Canada';
		civAddr.CivicAdditonalInfo = 'Behind Main Street';
		civAddr.CivicMunicipalityName = 'VAN';
		civAddr.CivicNumber = '123';
		civAddr.CivicPostalCd = 'L9L9L9L';
		civAddr.CivicProvince = 'Ontario';
		civAddr.CivicNumberSuffix = 'A';
		civAddr.CivicStreetName = 'Younge Street';
		addr.civicAddr = civAddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		//Adding API Data Load Custom Setting.
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('123 A Younge Street Behind Main Street', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
	}

	@isTest
	static void UnitTest_8_createCustAccnt_wt_POAddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.PostBoxAddress POAddr = new SFDCCustomerAccntContCreationService_v1.PostBoxAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		POAddr.Country = 'Canada';
		POAddr.AdditonalInfo = 'Behind Main Street';
		POAddr.MunicipalityName = 'van';
		POAddr.POBoxNum = '1234';
		POAddr.careOf = 'Care Of : Test User';
		POAddr.PostalCd = 'l9l9l9l';
		POAddr.Province = 'Ontario';
		addr.POBOXAddr = POAddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		//Adding API Data Load Custom Setting.
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Care Of : Test User 1234 Behind Main Street', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals('van', [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals('Canada', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
	}

	@isTest
	static void UnitTest_9_createCustAccnt_wt_RRAddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.RuralRouteAddress RRAddr = new SFDCCustomerAccntContCreationService_v1.RuralRouteAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		RRAddr.Country = 'Canada';
		RRAddr.PostalCd = 'L9L9L9L';
		RRAddr.Province = 'Ontario';
		RRAddr.RuralAdditionalInfo = 'Behind Main Street';
		RRAddr.RuralRouteNum = '1234';
		RRAddr.RuralRouteType = 'RR';
		RRAddr.careOf = 'Ms Nans';
		RRAddr.stationAreaName = 'Station Area';
		RRAddr.stationAreaNum = '223';
		RRAddr.stationQualifier = 'A';
		RRAddr.RuralMuncipalityName = 'Van';
		addr.RRAddr = RRAddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		//Adding API Data Load Custom Setting.
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Ms Nans RR 1234 223 Station Area A Behind Main Street', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals('Van', [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals('Canada', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
		system.assertEquals('L9L9L9L', [SELECT id, BillingPostalCode FROM Account WHERE SourceRefID__c = 'C_123456'].BillingPostalCode);
		system.assertEquals('Ontario', [SELECT id, BillingState FROM Account WHERE SourceRefID__c = 'C_123456'].BillingState);
	}

	@isTest
	static void UnitTest_10_createCustAccnt_wt_GenDelAddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		//Adding API Data Load Custom Setting.
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.GenDeliveryAdddress GDAddr = new SFDCCustomerAccntContCreationService_v1.GenDeliveryAdddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		GDAddr.careOf = 'Nancy Drew';
		GDAddr.stationTypeCd = 'Stn';
		GDAddr.stationAreaName = 'Winston';
		GDAddr.stationQualifier = 'Qual A';
		GDAddr.genAdditionalInfo = 'Behind Main Street';
		GDAddr.GenDelMunicipalityName = 'Van';
		GDAddr.GenDelProvince = 'Ontario';
		GDAddr.GenDelPostalCd = 'L9L9L9L';
		GDAddr.GenDelCountry = 'Canada';
		addr.GenDelAddr = GDAddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Nancy Drew Winston Stn Qual A Behind Main Street', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals('Van', [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals('Canada', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
		system.assertEquals('L9L9L9L', [SELECT id, BillingPostalCode FROM Account WHERE SourceRefID__c = 'C_123456'].BillingPostalCode);
		system.assertEquals('Ontario', [SELECT id, BillingState FROM Account WHERE SourceRefID__c = 'C_123456'].BillingState);
	}

	@isTest
	static void UnitTest_11_createCustAccnt_wt_MilAddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		//Adding API Data Load Custom Setting.
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.MilitaryAddress MilAddr = new SFDCCustomerAccntContCreationService_v1.MilitaryAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		MilAddr.careOf = 'Nancy Drew';
		MilAddr.fleetMailOfficeName = 'NewPort Office';
		MilAddr.hmcsName = 'Camp A';
		MilAddr.stationAreaName = 'Area Milport';
		MilAddr.stationTypeCode = 'Stn';
		MilAddr.municipalityName = 'Van';
		MilAddr.provinceCode = 'Ontario';
		addr.MilitaryAddr = MilAddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Nancy Drew Camp A NewPort Office Area Milport Stn', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals('Van', [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals(null, [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
		system.assertEquals(null, [SELECT id, BillingPostalCode FROM Account WHERE SourceRefID__c = 'C_123456'].BillingPostalCode);
		system.assertEquals('Ontario', [SELECT id, BillingState FROM Account WHERE SourceRefID__c = 'C_123456'].BillingState);
	}

	@isTest
	static void UnitTest_12_createCustAccnt_wt_USAddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		//Adding API Data Load Custom Setting.
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.USAAddress USAddr = new SFDCCustomerAccntContCreationService_v1.USAAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		USAddr.careOf = 'Nancy Drew';
		USAddr.additionalAddressInformation = 'Next to Main Street EAST';
		USAddr.countryCode = 'USA';
		USAddr.municipalityName = 'Cincinnati';
		USAddr.stateCode = 'OH';
		USAddr.zipCode = '4040400';
		addr.USAddr = USAddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Nancy Drew Next to Main Street EAST', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals('Cincinnati', [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals('USA', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
		system.assertEquals('4040400', [SELECT id, BillingPostalCode FROM Account WHERE SourceRefID__c = 'C_123456'].BillingPostalCode);
		system.assertEquals('OH', [SELECT id, BillingState FROM Account WHERE SourceRefID__c = 'C_123456'].BillingState);
	}

	@isTest
	static void UnitTest_13_createCustAccnt_wt_IntlAddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		//Adding API Data Load Custom Setting.
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.IntlAddress Intlddr = new SFDCCustomerAccntContCreationService_v1.IntlAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		Intlddr.additionalAddressInformation = 'Kandivali E';
		Intlddr.careOf = 'Deepak Malkani';
		Intlddr.countryCode = 'Ind';
		Intlddr.stateCode = 'MH';
		addr.IntlAddr = Intlddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Deepak Malkani Kandivali E', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals(null, [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals('Ind', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
		system.assertEquals(null, [SELECT id, BillingPostalCode FROM Account WHERE SourceRefID__c = 'C_123456'].BillingPostalCode);
		system.assertEquals('MH', [SELECT id, BillingState FROM Account WHERE SourceRefID__c = 'C_123456'].BillingState);
	}

	@isTest
	static void UnitTest_14_createCustAccntCont_wt_Intddr()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		//Adding API Data Load Custom Setting.
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.IntlAddress Intlddr = new SFDCCustomerAccntContCreationService_v1.IntlAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		List<SFDCCustomerAccntContCreationService_v1.custContact> contList = new List<SFDCCustomerAccntContCreationService_v1.custContact>();
		SFDCCustomerAccntContCreationService_v1.custContact contRec = new SFDCCustomerAccntContCreationService_v1.custContact();
		SFDCCustomerAccntContCreationService_v1.Keys key = new SFDCCustomerAccntContCreationService_v1.Keys();
		
		Intlddr.additionalAddressInformation = 'Kandivali E';
		Intlddr.careOf = 'Deepak Malkani';
		Intlddr.countryCode = 'Ind';
		Intlddr.stateCode = 'MH';
		addr.IntlAddr = Intlddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		contRec.contFirstName = 'Test1';
		contRec.contLastName = 'TestLast1';
		contRec.contLanguagePref = 'EN';
		contRec.contRelnType = 'Owner';
		contRec.contEmailAddress = 'test1@test1.com';
		key.Id = '2232323';
		key.srcSystemName = 'CODS';
		contRec.contSrcKey = key;
		contList.add(contRec);
		custRec.CustContacts = contList;
		
		Test.startTest();
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		Test.stopTest();
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Deepak Malkani Kandivali E', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals(null, [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals('Ind', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
		system.assertEquals(null, [SELECT id, BillingPostalCode FROM Account WHERE SourceRefID__c = 'C_123456'].BillingPostalCode);
		system.assertEquals('MH', [SELECT id, BillingState FROM Account WHERE SourceRefID__c = 'C_123456'].BillingState);
		system.assertEquals('CI_2232323', [SELECT id, SourceRefID__c FROM Contact WHERE SourceRefID__c = 'CI_2232323'].SourceRefID__c);
		system.assertEquals('C_123456', [SELECT AccountRefId__c, Id, SourceRefID__c FROM AccountContacts__c WHERE ContactRefId__c = 'CI_2232323'].AccountRefId__c);
	}

	@isTest
	static void UnitTest_15_createCustAccntCont_wt_Intddr_Error()
	{
		SFDCCustomerAccntContCreationService_v1.customerRec custRec = new SFDCCustomerAccntContCreationService_v1.customerRec();
		//Adding API Data Load Custom Setting.
		UtlityTest testHandler = new UtlityTest();
		testHandler.setAPILoadsCS();
		SFDCCustomerAccntContCreationService_v1.IntlAddress Intlddr = new SFDCCustomerAccntContCreationService_v1.IntlAddress();
		SFDCCustomerAccntContCreationService_v1.Address addr = new SFDCCustomerAccntContCreationService_v1.Address();
		List<SFDCCustomerAccntContCreationService_v1.custContact> contList = new List<SFDCCustomerAccntContCreationService_v1.custContact>();
		SFDCCustomerAccntContCreationService_v1.custContact contRec = new SFDCCustomerAccntContCreationService_v1.custContact();
		SFDCCustomerAccntContCreationService_v1.Keys key = new SFDCCustomerAccntContCreationService_v1.Keys();
		List<AccountContacts__c> accContList = new List<AccountContacts__c>();

		Intlddr.additionalAddressInformation = 'Kandivali E';
		Intlddr.careOf = 'Deepak Malkani';
		Intlddr.countryCode = 'Ind';
		Intlddr.stateCode = 'MH';
		addr.IntlAddr = Intlddr;
		custRec.CustBillCycleCode = 24;
		custRec.CustBillCycleDay = 12;
		custRec.CustBrand = 'Telus';
		custRec.CustLangPref = 'EN';
		custRec.CustName = 'Test1';
		custRec.CustStatus = 'Active';
		custRec.CustType = 'Customer';
		custRec.CustId = '123456';
		custRec.CustAddress = addr;
		contRec.contFirstName = 'Test1';
		contRec.contLastName = 'TestLast1';
		contRec.contLanguagePref = 'EN';
		contRec.contRelnType = 'Owner';
		contRec.contEmailAddress = 'test1@test1.com';
		key.Id = '2232323';
		key.srcSystemName = 'CODS';
		contRec.contSrcKey = key;
		contList.add(contRec);
		custRec.CustContacts = contList;	
		//Make WebService Call out
		SFDCCustomerAccntContCreationService_v1.custContResponse response = SFDCCustomerAccntContCreationService_v1.createCustandContacts(custRec);
		system.assertEquals(null, response.respError);
		system.assertEquals(true, response.respMessage.contains('Success'));
		system.assertEquals([SELECT id FROM Account WHERE SourceRefID__c = 'C_123456'].id, response.SfdcId);
		system.assertEquals('Deepak Malkani Kandivali E', [SELECT id, BillingStreet FROM Account WHERE SourceRefID__c = 'C_123456'].BillingStreet);
		system.assertEquals(null, [SELECT id, BillingCity FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCity);
		system.assertEquals('Ind', [SELECT id, BillingCountry FROM Account WHERE SourceRefID__c = 'C_123456'].BillingCountry);
		system.assertEquals(null, [SELECT id, BillingPostalCode FROM Account WHERE SourceRefID__c = 'C_123456'].BillingPostalCode);
		system.assertEquals('MH', [SELECT id, BillingState FROM Account WHERE SourceRefID__c = 'C_123456'].BillingState);
		system.assertEquals('CI_2232323', [SELECT id, SourceRefID__c FROM Contact WHERE SourceRefID__c = 'CI_2232323'].SourceRefID__c);
		system.assertEquals('C_123456', [SELECT AccountRefId__c, Id, SourceRefID__c FROM AccountContacts__c WHERE ContactRefId__c = 'CI_2232323'].AccountRefId__c);

		//Performing DML Seperately to capture exceptions.
		Test.startTest();
		for(AccountContacts__c acObj : [SELECT AccountRefId__c, Id, SourceRefID__c,ContactRefId__c FROM AccountContacts__c WHERE ContactRefId__c = 'CI_2232323' LIMIT 1])
		{
			AccountContacts__c acContObj = new AccountContacts__c(ContactRefId__c = 'CI_2232323');
			acContObj.Id = acObj.Id;
			acContObj.AccountRefId__c = null;
			accContList.add(acContObj);
		}
		try {
		    update accContList;
		}
		catch (Exception e) {
		    system.assertEquals(true, e.getMessage().contains('AccountRefID is a required external key'));
		}
		finally {
		    accContList.clear();
		}
		Test.stopTest();
	}
}