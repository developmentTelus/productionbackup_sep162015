/*
	Author : Deepak Malkani.
	Created Date : July 22 2015
	Purpose : Batch job used to to a mass update on Source Ref Id key for Account Contacts. This is needed to ensure ETL now starts using 
	the new composite external key : AccountContactCompKey__c for upserting AccountContacts.
	Modified : Aneeq Hashmi
	Modified Date : 2 Aug 2015
*/
global class AccountContactExternalKeyUpdate_Batch implements Database.Batchable<sObject> {
	
	String query;
	
	global AccountContactExternalKeyUpdate_Batch() {
		query = 'SELECT id, SourceRefId__c, AccountContactCompKey__c, AccountId__r.SourceRefId__c, AccountId__r.Type, ContactId__r.SourceRefId__c FROM AccountContacts__c WHERE SourceRefId__c LIKE \'CA_%\' and AccountContactCompKey__c = \'\'';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
	
		List<AccountContacts__c> acntConList = new List<AccountContacts__c>();
		for(AccountContacts__c accont : (List<AccountContacts__c>) scope)
		{
			if(accont.AccountId__r.Type.toLowerCase() == 'customer')
			{
				AccountContacts__c ac = new AccountContacts__c(Id = accont.Id);
				ac.AccountContactCompKey__c = accont.AccountId__r.SourceRefId__c + '|' + accont.ContactId__r.SourceRefId__c;
				acntConList.add(ac);
			}
		}
		if(!acntConList.isEmpty())
			update acntConList;
	}
	
	global void finish(Database.BatchableContext BC) {
		
		//Finish Block will send an email
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
        		          FROM AsyncApexJob 
        		          WHERE Id =: BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'aneeq.hashmi@telus.com','greg.lebel@telus.com','vincent.ng@telus.com','shiva.sayahi@telus.com','bj.jancovic@telus.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('AccountContact Composite Key Update ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
}